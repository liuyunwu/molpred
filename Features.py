# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'


########################################################################################################################
# This module contains functions / classes for different types of molecular features (ECFP, graphs, etc.)

import numpy as np
import copy, os, glob
import MolPred as mp
import rdkit
import rdkit.Chem.AllChem
import time



########################################################################################################################
# Converts input molecules into "features" to be used for machine learning processes
def feature_encoding(mols, models):

    if models.feats_type == 'ecfp':
        X, Y, mols, exclude_mols = ecfp_encoding(mols, models)

    return( X, Y, mols, exclude_mols )


########################################################################################################################
def ecfp_encoding(mols, models):
    # TODO: determine if there is a useful way to exclude sparse ECFP molecules
    exclude_mols = []
    keep_mols = []
    n_mols = len(mols)

    if n_mols == 0:
        return([], [], mols, [])

    for i in range(n_mols):
        try:
            fp1 = rdkit.Chem.AllChem.GetMorganFingerprintAsBitVect(mols[i].rdmol, models.feats.fp_radius,
                                                                   models.feats.n_bits, useFeatures=models.feats.use_fcfp)
            # print('\tSuccessfully featurized molecule: %s' % (mols[i].mol_name))

            fp1 = np.array(fp1).reshape((1, models.feats.n_bits))

            mols[i].feats_vec = copy.deepcopy(fp1)
            keep_mols.append(copy.deepcopy(mols[i]))
        except:
            print('\t>>>>> ECFP featurization failed for molecule %s <<<<<\n\n' % (mols[i].mol_name))
            exclude_mols.append(copy.deepcopy(mols[i]))

    mols = copy.deepcopy(keep_mols)
    n_mols = len(mols)

    # n_feats = int(np.size(np.squeeze(mols[0].feats_vec)))
    n_feats = models.feats.n_bits

    X = np.zeros((n_mols, n_feats))
    Y = None

    #TODO: refactor the activity matrix Y loading to the function that calls this one
    has_acts = False
    if mols[0].acts is not None:
        if np.size(mols[0].acts) > 0:
            has_acts = True
            Y = np.zeros((n_mols, models.n_tasks))

    for i in range(n_mols):
        X[i,:] = np.squeeze(mols[i].feats_vec)
        if has_acts:
            Y[i,:] = np.squeeze(mols[i].acts)

    return(X, Y, mols, exclude_mols)


########################################################################################################################
def featurize_ecfp_molfiles(feats, mol_names, mol_root):
    # Load mol files with RDKit
    all_qmols = []

    for i in range(0, len(mol_names)):
        qmol = mp.MOL()
        qmol.mol_name = mol_names[i].lower()

        mol_file = os.path.join(mol_root, mol_names[i].upper() + '.mol')
        try:
            # Load molecule from file
            print('ECFP featurizing %s' % (mol_file))
            rdmol = rdkit.Chem.MolFromMolFile(str(mol_file))
            print('\tSuccessfully loaded mol file: %s' % (mol_file))

            # Perform 3D geometry optimization of molecules.  RDKit recommends to add Hs first
            # rdmol = rdkit.Chem.AddHs(rdmol)
            # rdkit.Chem.AllChem.EmbedMolecule(rdmol, rdkit.Chem.AllChem.ETKDG())

            fp1 = rdkit.Chem.AllChem.GetMorganFingerprintAsBitVect(rdmol, feats.fp_radius,
                                                                   feats.n_bits, useFeatures=feats.use_fcfp)
            print('\tSuccessfully featurized molecule: %s' % (mol_names[i]))

            fp1 = np.array(fp1).reshape((1, feats.n_bits))

            qmol.feats_vec = copy.deepcopy(fp1)
            qmol.smiles = rdkit.Chem.MolToSmiles(copy.deepcopy(rdmol))
            qmol.n_atoms = rdmol.GetNumAtoms()
            qmol.n_atom_types = np.unique(qmol.elem)
            qmol.x3d = np.zeros((3,qmol.n_atoms))
            elem = []
            for j in range(qmol.n_atoms):
                qmol.x3d[0, j] = rdmol.GetConformer().GetAtomPosition(j).x
                qmol.x3d[1, j] = rdmol.GetConformer().GetAtomPosition(j).y
                qmol.x3d[2, j] = rdmol.GetConformer().GetAtomPosition(j).z
                elem.append(rdmol.GetAtomWithIdx(j).GetSymbol())

            qmol.elem = copy.deepcopy(elem)
            all_qmols.append(copy.deepcopy(qmol))
        except:
            print('!!! Couldn''t load / featurize mol file: %s !!!' % (mol_file))
            continue

    return(all_qmols)


########################################################################################################################
def load_rdmols_from_molfiles(mol_names, mol_root):

    # Load mol files with RDKit
    all_qmols = []

    for i in range(0, len(mol_names)):
        qmol = mp.MOL()
        qmol.mol_name = mol_names[i].lower()

        mol_file = os.path.join(mol_root, mol_names[i].upper() + '.mol')
        try:
            # Load molecule from file
            rdmol = rdkit.Chem.MolFromMolFile(str(mol_file))
            qmol.rdmol = copy.deepcopy(rdmol)
            qmol.smiles = rdkit.Chem.MolToSmiles(copy.deepcopy(rdmol))
            qmol.n_atoms = rdmol.GetNumAtoms()
            qmol.n_atom_types = np.unique(qmol.elem)
            qmol.x3d = np.zeros((3, qmol.n_atoms))
            elem = []
            for j in range(qmol.n_atoms):
                qmol.x3d[0, j] = rdmol.GetConformer().GetAtomPosition(j).x
                qmol.x3d[1, j] = rdmol.GetConformer().GetAtomPosition(j).y
                qmol.x3d[2, j] = rdmol.GetConformer().GetAtomPosition(j).z
                elem.append(rdmol.GetAtomWithIdx(j).GetSymbol())

            qmol.elem = copy.deepcopy(elem)
            all_qmols.append(copy.deepcopy(qmol))
        except:
            print('!!! Couldn''t load / featurize mol file: %s !!!' % (mol_file))
            continue

    return(all_qmols)


########################################################################################################################
# Recalculates molecule ECFP featurization for various settings as part of the model stress test process
def ecfp_sensitivity_mst(all_model_save_files, base_models, model_labels, stset, data_int_base, data_test_base, config_file):

    nfeats = len(all_model_save_files)

    for i in range(0, len(stset.fp_radius)):
        nfeats += 1
        model_labels.append('Features' + str(nfeats))

        # Define the features for the current model
        # cur_model = copy.deepcopy(base_models)
        cur_model = mp.load_model(all_model_save_files[0])
        cur_model.feats.fp_radius = copy.deepcopy(stset.fp_radius[i])

        # Redo featurization and train model using the the fingerprint radius changes
        cur_model, curdata_int, curdata_test = mp.load_data_int_test(cur_model)
        cur_model = mp.perform_modeling_st(cur_model, curdata_int, curdata_test, model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.n_bits)):
        nfeats += 1
        model_labels.append('Features' + str(nfeats))
        # Define the features for the current model
        # cur_model = copy.deepcopy(base_models)
        cur_model = mp.load_model(all_model_save_files[0])
        cur_model.feats.n_bits = copy.deepcopy(stset.n_bits[i])

        # Redo featurization and train model using the n_bits changes
        cur_model, curdata_int, curdata_test = mp.load_data_int_test(cur_model)
        cur_model = mp.perform_modeling_st(cur_model, curdata_int, curdata_test, model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.use_fcfp)):
        nfeats += 1
        model_labels.append('Features' + str(nfeats))
        # Define the features for the current model
        # cur_model = copy.deepcopy(base_models)
        cur_model = mp.load_model(all_model_save_files[0])
        cur_model.feats.use_fcfp = copy.deepcopy(stset.use_fcfp[i])

        # Redo featurization and train model using the use_fcfp changes
        cur_model, curdata_int, curdata_test = mp.load_data_int_test(cur_model)
        cur_model = mp.perform_modeling_st(cur_model, curdata_int, curdata_test, model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    return(all_model_save_files, model_labels)


########################################################################################################################
class ECFP_FEATURES():
    def __init__(self):
        self.feats_type = 'ecfp'
        self.fp_radius = None
        self.n_bits = None
        self.use_fcfp = None

