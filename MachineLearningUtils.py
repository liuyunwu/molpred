# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

# A series of machine learning library functions and utilities

import numpy as np
import copy
from sklearn.cluster import KMeans
import itertools
import scipy
import PLS as pls
import KNN as knn
import ANN as ann
import sklearn
from xgboost import XGBClassifier, XGBRegressor


########################################################################################################################
# Applies an ANN that has been trained to generate a set of regression predictions.  Uses a more library independent
#   implementation of the prediction generation function
def apply_ann(ann_params, X):

    Np = np.size(X,0) # # of data samples to generate predictions for
    Nneur = np.size(ann_params.biases[0]) # number of neurons in hidden layer

    # Hidden layer terms
    Bmat_h = np.tile(ann_params.biases[0].reshape((1,Nneur)), (Np, 1) ) # bias terms for each of the Nneur neurons at each sample
    E = np.dot( X, ann_params.weights[0]) + Bmat_h # "Excitation" of hidden layer neurons

    # Apply the activation function to get the hidden layer output to the next layer
    if( ann_params.act_fn.lower() == 'sigmoid'):
        # standard sigmoid activation function
        H1 = 1.0 / (1.0 + np.exp(-E))
    elif( ann_params.act_fn.lower() == 'rectifier'):
        # ReLU: rectified linear unit
        H1 = np.maximum(np.zeros((np.size(E,0),np.size(E,1))), E)
    elif( ann_params.act_fn.lower() == 'tanh'):
        exp_exc = np.exp(E)
        exp_negexc = np.exp(-E)
        H1 = (exp_exc - exp_negexc) / (exp_exc + exp_negexc)
    else:
        # unknown activation type
        print('Unknown activation type.  Assuming identity activation')
        H1 = copy.deepcopy(E)

    # Output layer terms- no activation function here
    Bvec_out = np.tile(ann_params.biases[1], (Np,1) ) # vector of bias terms, one per prediction
    Y_pred = np.dot( H1, ann_params.weights[1] ) + Bvec_out

    return( Y_pred )



########################################################################################################################
# Applies a trained ANN classification model to predictors using "custom" functions (not the scikit functions)
def apply_ann_class(ann_params, X, binary_thresh=None):

    #TODO: make a more general summation function that handles multiple hidden layers
    Ns = np.size(X,0) # # of data samples to generate predictions for
    Nneur = np.size(ann_params.biases[0]) # number of neurons in hidden layer

    # Hidden layer terms
    Bmat_h = np.tile(ann_params.biases[0].reshape((1,Nneur)), (Ns, 1) ) # bias terms for each of the Nneur neurons at each sample
    E = np.dot( X, ann_params.weights[0]) + Bmat_h # "Excitation" of hidden layer neurons

    # Apply the activation function to get the hidden layer output to the next layer
    if( ann_params.act_fn.lower() == 'sigmoid'):
        # standard sigmoid activation function
        H1 = 1.0 / (1.0 + np.exp(-E))
    elif( ann_params.act_fn.lower() == 'rectifier'):
        # ReLU: rectified linear unit
        H1 = np.maximum(np.zeros((np.size(E,0),np.size(E,1))), E)
    elif( ann_params.act_fn.lower() == 'tanh'):
        exp_exc = np.exp(E)
        exp_negexc = np.exp(-E)
        H1 = (exp_exc - exp_negexc) / (exp_exc + exp_negexc)
    else:
        # unknown activation type
        print('Unknown activation type.  Assuming identity activation')
        H1 = copy.deepcopy(E)

    # Output layer terms- Softmax activation function to produce a probability
    Nclass = np.size(ann_params.biases[1])
    Bmat_out = np.tile(ann_params.biases[1].reshape((1,Nclass)), (Ns,1) )
    Z = np.dot(H1,ann_params.weights[1]) + Bmat_out
    expZ = np.exp(Z)
    sumExpZ = np.sum(expZ,1)

    class_pred = np.zeros((Ns,1))
    prob_pred = np.zeros((Ns,Nclass))
    for n in range(0, Ns):
        prob_pred[n,:] = np.squeeze(expZ[n,:] / sumExpZ[n])
        if Nclass == 2 and binary_thresh is not None:
            # Give a binary class prediction a label of Class 1 if probability > binary_thresh
            if prob_pred[n,1] >= binary_thresh:
                class_pred[n] = 1
            else:
                class_pred[n] = 0
        else:
            # Take the class prediction to be the highest class probability
            max_prob = np.unique(np.max(prob_pred[n, :]))
            class_pred[n] = np.min(np.where(prob_pred[n, :] == max_prob))


    return( prob_pred, class_pred )


########################################################################################################################
# Applies a multilayer ANN that has been trained to generate a set of regression predictions.  Uses a more library
#   independent implementation of the prediction generation function
def apply_ann_multi(ann_params, X):

    #TODO: make a more general summation function that handles multiple hidden layers
    Np = np.size(X,0) # # of data samples to generate predictions for
    Nh = len(ann_params.weights) - 1  # number of hidden layers in the ANN

    for h in range(0, Nh):
        Nneur = np.size(ann_params.biases[h]) # number of neurons in current hidden layer

        # Hidden layer terms
        Bmat_h = np.tile(ann_params.biases[h].reshape((1,Nneur)), (Np, 1) ) # bias terms for each of the Nneur neurons at each sample
        E = np.dot( X, ann_params.weights[h]) + Bmat_h # "Excitation" of hidden layer neurons

        # Apply the activation function to get the hidden layer output to the next layer
        if( ann_params.act_fn.lower() == 'sigmoid'):
            # standard sigmoid activation function
            H1 = 1.0 / (1.0 + np.exp(-E))
        elif( ann_params.act_fn.lower() == 'rectifier'):
            # ReLU: rectified linear unit
            H1 = np.maximum(np.zeros((np.size(E,0),np.size(E,1))), E)
        elif( ann_params.act_fn.lower() == 'tanh'):
            exp_exc = np.exp(E)
            exp_negexc = np.exp(-E)
            H1 = (exp_exc - exp_negexc) / (exp_exc + exp_negexc)
        else:
            # unknown activation type
            print('Unknown activation type.  Assuming identity activation')
            H1 = copy.deepcopy(E)

    # Output layer terms- no activation function here
    Bvec_out = np.tile(ann_params.biases[Nh], (Np,1) ) # vector of bias terms, one per prediction
    Y_pred = np.dot( H1, ann_params.weights[Nh] ) + Bvec_out

    return( Y_pred )



########################################################################################################################
def class_from_probs(prob_preds, bin_class_thresh = None):

    # if pred_probs is not a list:
    if prob_preds.ndim == 1:
        prob_preds = prob_preds.reshape((1,np.size(prob_preds)))
    ns = np.size(prob_preds,0)
    nclass = np.size(prob_preds,1)
    class_preds = np.zeros((ns,1),dtype=np.int32) * np.nan

    for i in range(0, ns):
        max_prob = np.unique(np.max(prob_preds[i, :]))

        if np.isnan(max_prob):
            continue

        if bin_class_thresh is not None and nclass == 2:
            # Binary class prediction- active class prediction if active prediction probability > threshold
            if prob_preds[i,1] > bin_class_thresh:
                class_preds[i] = 1
            else:
                class_preds[i] = 0
        else:
            # Class prediction determined by the highest probability
            if np.isnan(max_prob) == False:
                class_preds[i] = np.min(np.where(prob_preds[i, :] == max_prob))

    return( class_preds)


########################################################################################################################
# Converts a 1D input array to a one-hot encoding
def to_one_hot_encoding(Y, nclass=2, mask_value = None):

    if np.size(np.shape(Y)) == 1:
        ns = np.size(Y)
    else:
        ns = np.size(Y,0)
    Yonehot = np.zeros((ns, nclass))

    for i in range(ns):
        if mask_value is not None:
            if Y[i] == mask_value:
                Yonehot[i,:] = mask_value
                continue
        if np.isnan(Y[i]) or np.isinf(Y[i]) or np.isneginf(Y[i]):
            continue
        idx = int(Y[i])
        Yonehot[i,idx] = 1

    return( Yonehot)


########################################################################################################################
# Converts a one-hot encoded array to a 1D array
def from_one_hot_encoding(Yonehot):
    ns, nclass = np.shape(Yonehot)
    Y = np.zeros((ns,1))

    for i in range(ns):
        idx = np.where(Yonehot[i,:] > 0)[0]
        Y[i,0] = np.min(idx)

    return( Y )



########################################################################################################################
# Calculates performance statistics for a regression model, per-task
def error_stats(Y_pred, Y_obs, Y_pred_all = None, siglevel=0.95):

    n_tasks = Y_obs.shape[1]
    n_s = Y_obs.shape[0]

    if Y_pred_all is not None:
        n_models = Y_pred_all[0].shape[1]
    else:
        n_models = 1

    stats_all_tasks = []
    [stats_all_tasks.append(PERF_STATS(pred_type='regression', n_s=n_s, n_models=n_models, siglevel=siglevel)) for i in range(n_tasks)]
    # stats = PERF_STATS(pred_type='regression', n_tasks=n_tasks)
    # rsq = np.zeros(n_tasks)
    # e_rms = np.zeros(n_tasks)
    pred_errors = []
    for i in range(n_tasks):
        notnanidx_obs = np.where(np.isnan(Y_obs[:,i]) == False)[0]
        notnanidx_pred = np.where(np.isnan(Y_pred[i]) == False)[0]
        keepidx = np.intersect1d(notnanidx_obs, notnanidx_pred)

        pred = np.squeeze(Y_pred[i][keepidx])
        obs = Y_obs[keepidx,i]

        # obs_mean = np.mean(obs)
        # sumsq_total = np.sum((obs - obs_mean)**2.0) # total variance of the observations
        # pred_errors_task = pred - obs # errors in the predictive model used to generate predictions Y_pred
        # sumsq_resid = np.sum(pred_errors_task**2.0) # summed prediction errors squared
        # stats_all_tasks[i].rsq = 1.0 - sumsq_resid / sumsq_total # this is the fraction of variance explained by the model predictions

        stats_all_tasks[i].rsq, stats_all_tasks[i].e_rms, stats_all_tasks[i].e_maxabs, pred_errors_task = calc_rsq_stats(pred, obs)

        pred_errors.append(pred_errors_task)

        if Y_pred_all is not None:
            # stats_all_tasks[i] = ensemble_stats_regr(stats_all_tasks[i], Y_pred_all[i][keepidx,:], obs, siglevel=siglevel)
            stats_all_tasks[i] = ensemble_stats_regr(stats_all_tasks[i], Y_pred_all[i], Y_obs[:,i], siglevel=siglevel)

    return( stats_all_tasks, pred_errors )


########################################################################################################################
# Calculates ensemble prediction statistics for a task
def ensemble_stats_regr(stats, pred_all_models, obs, siglevel = 0.95):

    n_models = pred_all_models.shape[1]
    n_s = pred_all_models.shape[0]

    stats.confint_per_pred = np.zeros((n_s,2)) * np.nan

    # Calculate Rsq coeffs for each model, and confidence interval of Rsq figures of merit
    for i in range(n_models):
        stats.rsq_model[i], stats.e_rms_model[i], stats.e_maxabs_model[i], pred_errors = calc_rsq_stats(pred_all_models[:,i], obs)

    stats.confint_rsq = conf_int_bootstrap(stats.rsq_model, siglevel = siglevel)

    # Calculate confidence intervals for each prediction
    for i in range(n_s):
        curerrors = pred_all_models[i,:] - obs[i]
        notnanidx = np.where(np.isnan(curerrors) == False)[0]
        if np.size(notnanidx) < 2:
            continue
        stats.confint_per_pred[i, :] = conf_int_bootstrap(pred_all_models[i,:], siglevel=siglevel)

    return(stats)


########################################################################################################################
# Calculates rsq and related stats for all samples prediction
def calc_rsq_stats(pred, obs):
    notnanidx_pred = np.where(np.isnan(pred) == False)[0]
    notnanidx_obs = np.where(np.isnan(obs) == False)[0]
    notnanidx = np.intersect1d(notnanidx_obs, notnanidx_pred)

    pred = pred[notnanidx]
    obs = obs[notnanidx]

    obs_mean = np.mean(obs)
    sumsq_total = np.sum((obs - obs_mean) ** 2.0)  # total variance of the observations
    pred_errors = pred - obs  # errors in the predictive model used to generate predictions
    sumsq_resid = np.sum(pred_errors ** 2.0)  # summed prediction errors squared
    rsq = 1.0 - sumsq_resid / sumsq_total  # this is the fraction of variance explained by the model predictions

    e_rms = np.sqrt(np.mean(pred_errors**2.0))
    e_maxabs = np.max(np.abs(pred_errors))

    return(rsq, e_rms, e_maxabs, pred_errors)


########################################################################################################################
# Calculates confidence interval (non-parametrically) for a statistic using "bootstrapping"
def conf_int_bootstrap(x, siglevel = 0.95):
    notnanidx = np.where(np.isnan(x) == False)[0]
    if np.size(notnanidx) < 2:
        return(np.nan, np.nan)

    x = x[notnanidx]
    x_sort = np.sort(x)
    lower_frac = (1.0 - siglevel) / 2.0
    upper_frac = siglevel + lower_frac
    # ci_lower = np.percentile(x_sort, lower_frac * 100)
    # ci_upper = np.percentile(x_sort, upper_frac * 100)
    ci = np.percentile(x_sort, np.array([lower_frac*100, upper_frac*100]))
    ci = ci.reshape((1,2))
    return(ci)



########################################################################################################################
# Computes performance statistics for a set of classifier predictions
def multitask_perf_stats_class(Y_pred, Y_true, n_class, pred_probs = None):
    stats_all_tasks = []
    n_tasks = int(np.size(Y_true,1))
    n_s = int(np.size(Y_true,0))

    for t in range(n_tasks):
        # Only calculate stats where we have "true" values and where we have made predictions
        true_notnanidx = np.where(np.isnan(Y_true[:,t]) == False)[0]
        pred_notnanidx = np.where(np.isnan(Y_pred[t][:,0]) == False)[0]
        notnanidx = np.intersect1d(true_notnanidx, pred_notnanidx)

        n_used = int(np.size(notnanidx))
        Y_pred_used = Y_pred[t][notnanidx,:].reshape((n_used,1))
        Y_true_used = Y_true[notnanidx,t].reshape((n_used, 1))
        probs_used = None
        if pred_probs is not None:
            probs_used = pred_probs[t][notnanidx,:]

        #stats = perf_stats_class(Y_pred[t], Y_true[:,t].reshape((n_s,1)), n_class[t], pred_probs[t])
        stats = perf_stats_class(Y_pred_used, Y_true_used, n_class[t], probs_used)
        stats_all_tasks.append(copy.deepcopy(stats))

    return(stats_all_tasks)


########################################################################################################################
# Computes performance statistics for a set of classifier predictions.  This is single task only
def perf_stats_class(Y_pred, Y_true, n_class, pred_probs = None):
    stats = PERF_STATS(pred_type='classification')

    if np.size(Y_pred,0) == 0 or np.size(Y_pred,0) != np.size(Y_true,0):
        return( stats )

    # Exclude missing values / NaNs and re-count # of samples
    ns_total = np.size(Y_true,0)
    nanidx = np.where(np.isnan(Y_true[:,0]) == True)[0]
    keepidx = np.setdiff1d(np.arange(0, ns_total), nanidx)
    n_preds = int(np.size(keepidx))
    # n_obs = int(np.size(keepidx))
    if pred_probs is not None:
        pred_probs = pred_probs[keepidx,:]
    Y_pred = Y_pred[keepidx,0].reshape((n_preds,1))
    Y_true = Y_true[keepidx,0].reshape((n_preds,1))


    pred_error = Y_pred - Y_true
    correctidx = np.where( pred_error == 0)[0]
    n_corr = np.size(correctidx)
    stats.accuracy = float(n_corr) / float(n_preds)

    # Compute confusion matrix and Pcc matrix
    uclasses = np.arange(0,n_class)
    stats.CM = np.zeros((n_class,n_class))
    stats.Pmat = np.zeros((n_class, n_class))
    stats.precision = np.zeros(n_class)
    stats.recall = np.zeros(n_class)
    stats.ns_class = np.zeros(n_class, dtype=np.int32)
    Ntruepos_preds = np.zeros(n_class)
    Npredpos = np.zeros(n_class)
    Nfalsepos_preds = np.zeros(n_class)
    Ntrueneg_preds = np.zeros(n_class)
    Npredneg = np.zeros(n_class)
    Nfalseneg_preds = np.zeros(n_class)
    for m in range(0, n_class):
        curtrueposidx = np.where(Y_true == uclasses[m])[0] # indices of true positive instances of current class
        curtruenegidx = np.where(Y_true != uclasses[m])[0] # indices of true negative instances of current class
        curpredposidx = np.where(Y_pred == uclasses[m])[0] # indices of predicted positive instances of current class
        curprednegidx = np.where( Y_pred != uclasses[m])[0] # indices of predicted negative instances of current class

        Y_pred_ofcurtruecls = Y_pred[curtrueposidx] # predicted classes for true positive samples
        stats.ns_class[m] = np.size(Y_pred_ofcurtruecls) # num samples actually in current class
        for n in range(0, n_class):
            npred_incurcls = np.size( np.where(Y_pred_ofcurtruecls == uclasses[n])[0] )
            stats.CM[m,n] = npred_incurcls
            if stats.ns_class[m] > 0:
                stats.Pmat[m,n] = float(npred_incurcls) / float(stats.ns_class[m])
            else:
                stats.Pmat[m,n] = np.nan

        predposandtrueposidx = np.intersect1d(curpredposidx, curtrueposidx)
        predposandtruenegidx = np.intersect1d(curpredposidx, curtruenegidx)
        prednegandtrueposidx = np.intersect1d(curprednegidx, curtrueposidx)
        prednegandtruenegidx = np.intersect1d(curprednegidx, curtruenegidx)

        Ntruepos_preds[m] = np.size(predposandtrueposidx)
        Ntrueneg_preds[m] = np.size(prednegandtruenegidx)
        Nfalsepos_preds[m] = np.size(predposandtruenegidx)
        Nfalseneg_preds[m] = np.size(prednegandtrueposidx)

    Npospreds_total = Ntruepos_preds + Nfalsepos_preds
    Nnegpreds_total = Ntrueneg_preds + Nfalseneg_preds

    stats.precision = stats.PPV
    stats.recall = Ntruepos_preds / (Ntruepos_preds + Nfalseneg_preds)
    stats.sensitivity = stats.Pmat[1,1] # a.k.a. "true positive rate"
    stats.specificity = stats.Pmat[0,0] # a.k.a. "true negative rate"
    stats.balanced_accuracy = np.mean(np.diag(stats.Pmat))

    # Get negative, positive predictive values
    truenegidx  = np.intersect1d(np.where(Y_pred == uclasses[0])[0], np.where(Y_true == uclasses[0])[0])
    trueposidx  = np.intersect1d(np.where(Y_pred == uclasses[1])[0], np.where(Y_true == uclasses[1])[0])
    falsenegidx = np.intersect1d(np.where(Y_pred == uclasses[0])[0], np.where(Y_true == uclasses[1])[0])
    falseposidx = np.intersect1d(np.where(Y_pred == uclasses[1])[0], np.where(Y_true == uclasses[0])[0])

    TN = float(np.size(truenegidx))
    TP = float(np.size(trueposidx))
    FN = float(np.size(falsenegidx))
    FP = float(np.size(falseposidx))
    total_neg_preds = TN + FN
    total_pos_preds = TP + FP

    if total_neg_preds > 0:
        stats.NPV = TN / total_neg_preds
    else:
        stats.NPV = -999999.999

    if total_pos_preds > 0:
        stats.PPV = TP / total_pos_preds
    else:
        stats.PPV = -999999.999

    # Calculate AUC (area under ROC curve) if individual prediction probabilities are provided
    if pred_probs is not None and n_class == 2:
        try:
            stats.auc = sklearn.metrics.roc_auc_score(to_one_hot_encoding(Y_true), pred_probs)
        except:
            print('Error calculating AUC!!!')
            stats.auc = -1.0

    return( stats )



########################################################################################################################
# Balances out the # samples in a training set to give equal # samples in each class
def balance_training_set(X_train, X_test, Y_train, Y_test, test_set):
    uclasses = np.unique(Y_train,0)
    Nclasses = np.size(uclasses)
    num_in_class = np.zeros(Nclasses)
    for u in range(0, Nclasses):
        num_in_class[u] = np.int32(np.size(np.where(Y_train == uclasses[u])[0]))
    min_class_size = np.int32(np.min(num_in_class))
    min_class_idx = np.where(num_in_class == min_class_size)[0]
    min_class = uclasses[min_class_idx]

    allkeepidx = np.where(Y_train == min_class)[0]
    allexcludeidx = []
    not_min_classes = np.int32(np.setdiff1d(uclasses, min_class))
    Nnotmin = np.size(not_min_classes)
    for n in range(0, Nnotmin):
        # Randomly select min_class_size samples from the current larger class
        curidx = np.where(Y_train == uclasses[not_min_classes[n]])[0]
        randidx = np.random.permutation(min_class_size)
        keeptrainidx = curidx[randidx[0:min_class_size]]
        excludetrainidx = np.setdiff1d(curidx,keeptrainidx)

        allkeepidx = np.concatenate((allkeepidx,keeptrainidx))
        if len(allexcludeidx) == 0:
            allexcludeidx = copy.deepcopy(excludetrainidx)
        else:
            allexcludeidx = np.concatenate((allexcludeidx,excludetrainidx))

    X_train_new = X_train[allkeepidx,:]
    Y_train_new = Y_train[allkeepidx,:]
    X_train_exclude = X_train[allexcludeidx,:]
    Y_train_exclude = Y_train[allexcludeidx,:]
    X_train = copy.deepcopy(X_train_new)
    Y_train = copy.deepcopy(Y_train_new)

    # Append samples kicked out of the training set to the test set
    X_test = np.concatenate((X_test, X_train_exclude),axis=0)
    Y_test = np.concatenate((Y_test, Y_train_exclude),axis=0)
    test_set = np.concatenate((test_set, allexcludeidx))

    return(X_train, X_test, Y_train, Y_test, allkeepidx, test_set)


########################################################################################################################
# Splits a list of entities (molecules or possibly other objects) into training and test sets as per settings defined in
#   the model settings
#TODO: this must be modified to appropriately handle multitask learning scenarios
def partition_list(mols, models, test_ratio=0.25, pred_type='classification', test_set_file = None, exclude_extra_test = False):
    n_mols = len(mols)
    train_ratio = 1.0 - test_ratio
    n_tasks = int(np.size(mols[0].acts))
    Y = np.zeros((n_mols, n_tasks))
    for i in range(n_mols):
        Y[i,:] = np.squeeze(mols[i].acts)

    if pred_type.lower() == 'classification' and models.preds.downsample_balancing == True:
        # Downsample balancing
        uclasses, ucts = np.unique(Y, return_counts=True)
        nclass = np.size(uclasses)
        smallestct = int(np.unique(np.min(ucts)))
        ntoselect = int(np.round(smallestct*train_ratio))
        nselect_class = np.zeros(nclass,dtype=np.int32)
        extratestidx = np.array([])
        trainidx = np.array([])
        for i in range(0, nclass):
            curclassidx = np.where(Y == uclasses[i])[0]
            ncur = np.size(curclassidx)
            randomidx = np.random.permutation(ncur)
            curtrainidx = curclassidx[randomidx[0:ntoselect]]
            if ncur > smallestct:
                curextratestidx = curclassidx[randomidx[smallestct:]]
                extratestidx = np.concatenate((extratestidx, curextratestidx))
            nselect_class[i] = np.size(curtrainidx)
            trainidx = np.concatenate((trainidx, curtrainidx))

        trainidx = np.sort(trainidx)
        testidx = np.setdiff1d(np.arange(0, n_mols), trainidx)
        if exclude_extra_test:
            nextra_test = np.size(extratestidx)
            testidx = np.setdiff1d(testidx, extratestidx)
            print('Excluding %d extra test set samples in upsample balancing' % (nextra_test))

    elif pred_type == 'classification' and models.preds.upsample_balancing == True:
        exclude_extra_test = False
        # Upsample balancing
        uclasses, ucts = np.unique(Y, return_counts=True)
        nclass = np.size(uclasses)

        # First, exclude test_ratio of each class to be in the test set
        ntest_to_select = np.int32(np.round(test_ratio * ucts))
        testidx = np.array([])
        for i in range(nclass):
            curclassidx = np.where(Y == uclasses[i])[0]
            ncur = np.size(curclassidx)
            randomidx = np.random.permutation(ncur)
            curtestidx = curclassidx[randomidx[0:ntest_to_select[i]]]
            testidx = np.concatenate((testidx,curtestidx))
        testidx = np.int32(np.sort(testidx))
        trainidx = np.setdiff1d(np.arange(n_mols,dtype=np.int32), testidx)

        # Upsample the smaller training class to be the same size as the larger training class
        Y_train = Y[trainidx,:]
        utrainclasses, utraincts = np.unique(Y_train, return_counts=True)
        nclass_train = np.size(utrainclasses)
        if nclass_train != nclass:
            exit('Not all classes are contained in the training set!!!  Cannot train a skilled model')

        largestct = int(np.unique(np.max(utraincts)))
        for i in range(nclass_train):
            curclassidx = np.where( Y_train == utrainclasses[i])[0]
            ncur = np.size(curclassidx)
            if ncur == largestct:
                # Already have the largest class.  No need to upsample in this class
                continue
            nupsample = largestct - ncur # how many samples of current class to upsample
            for j in range(nupsample):
                randidx = np.random.permutation(ncur)
                newtrainidx = np.array([trainidx[curclassidx[randidx[0]]]])
                trainidx = np.concatenate((trainidx,newtrainidx))

        trainidx = np.sort(trainidx)

    elif pred_type == 'classification' and n_tasks == 1:
        exclude_extra_test = False
        uclasses, ucts = np.unique(Y, return_counts=True)
        nclass = np.size(uclasses)

        # First, exclude test_ratio of each class to be in the test set
        ntest_to_select = np.int32(np.round(test_ratio * ucts))
        testidx = np.array([])
        for i in range(nclass):
            curclassidx = np.where(Y == uclasses[i])[0]
            ncur = np.size(curclassidx)
            randomidx = np.random.permutation(ncur)
            curtestidx = curclassidx[randomidx[0:ntest_to_select[i]]]
            testidx = np.concatenate((testidx,curtestidx))
        testidx = np.int32(np.sort(testidx))
        trainidx = np.setdiff1d(np.arange(n_mols,dtype=np.int32), testidx)

    else:
        # Just randomly select samples.  Might want to try an algorithm to ensure that we get a
        #   representative sample that spans the range of activities for both training and test sets
        ntest = int(np.round(n_mols * test_ratio))
        randidx = np.random.permutation(n_mols)
        testidx = np.sort( randidx[0:ntest])
        trainidx = np.setdiff1d(np.arange(n_mols), testidx)

    train_mols = []
    trainidx = np.int32(trainidx)
    testidx = np.int32(testidx)
    ntrain = np.size(trainidx)
    ntest = np.size(testidx)
    for i in range(ntrain):
        train_mols.append(copy.deepcopy(mols[trainidx[i]]))
    test_mols = []
    for i in range(ntest):
        test_mols.append(copy.deepcopy(mols[testidx[i]]))
    extra_test_mols = []
    if exclude_extra_test:
        extratestidx = np.int32(extratestidx)
        for i in range(nextra_test):
            extra_test_mols.append(copy.deepcopy(mols[extratestidx[i]]))

    # Checks
    overlapidx = np.intersect1d(trainidx,testidx)
    if np.size(overlapidx) > 0:
        exit('\n\n\n\nOverlap between training and test sets!!!')

    return(train_mols, test_mols, extra_test_mols)



########################################################################################################################
# Partition data into internal training and external evaluation data
def partition_data(X, Y, test_ratio = 0.33, test_set = None, pred_type = 'classification'):
    n_s = int(np.size(X,0)) # total number of samples
    n_feats = int(np.size(X,1)) # number of features
    n_tasks = int(np.size(Y,1)) # num response variables, i.e. number of tasks

    # If test set isn't specified, randomly divide data into training (internal) / test (external) sets
    is_in_test = np.zeros(n_s) # indicator if a given sample is in the external test set or not
    if( np.any(test_set == None) or test_set == []):
        n_test = np.int32(np.round(test_ratio * n_s))

        if pred_type == 'classification' and n_tasks == 1:
            test_set = distributed_sampling_class(Y, n_test)
        else:
            test_set = np.random.permutation(np.arange(0, n_s))[0:n_test]
            test_set = np.sort(test_set)

        n_test = np.size(test_set)
    else:
        # Passing in an array of preestablished test set indices
        n_test = np.size(test_set)
    n_train = n_s - n_test

    is_in_test[test_set] = 1

    X_train = np.zeros((n_train,n_feats))
    X_test = np.zeros((n_test,n_feats))
    Y_train = np.zeros((n_train,n_tasks))
    Y_test = np.zeros((n_test,n_tasks))
    testct = 0
    trainct = 0
    for n in range(0, n_s ):
        if( is_in_test[n]):
            X_test[testct,:] = np.squeeze(X[n,:])
            Y_test[testct,:] = np.squeeze(Y[n,:])
            testct += 1
        else:
            X_train[trainct,:] = np.squeeze(X[n,:])
            Y_train[trainct,:] = np.squeeze(Y[n,:])
            trainct += 1

    return( X_train, X_test, Y_train, Y_test, test_set)



########################################################################################################################
# Partition data into "balanced" internal training and external evaluation data
def partition_data_balanced(X, Y, test_ratio = 0.33, test_set = None, pred_type = 'classification'):
    if pred_type == 'regression':
        exit('Need to implement balanced training sets for regression modeling...')

    Ns = np.size(Y,0)
    Ntest = int(np.round(test_ratio*Ns))
    Nint = Ns - Ntest
    test_set = distributed_sampling_class(Y, Ntest)
    int_set = np.setdiff1d(np.arange(0,Ns),test_set)
    Y_int = Y[int_set,:]

    # Balance the internal set
    uclasses = np.unique(Y_int)
    Nclasses = np.size(uclasses)
    num_in_class = np.zeros(Nclasses)
    inclassidx = []
    for u in range(0, Nclasses):
        curidx = np.where( Y_int[:,0] == uclasses[u])[0]
        num_in_class[u] = np.size(curidx)
        inclassidx.append(copy.deepcopy(curidx))

    min_cts_class = np.min(num_in_class)
    minidx = np.min(np.where(num_in_class == min_cts_class)[0])
    num_to_exclude = np.zeros(Nclasses, dtype=np.int32)
    excludeidx = []


    for u in range(0, Nclasses):
        if u == minidx:
            excludeidx.append([])
            continue
        num_to_exclude[u] = num_in_class[u] - min_cts_class

        # Do the exclusion
        curidx = copy.deepcopy(inclassidx[u])
        np.random.shuffle(curidx)
        excludeidx.append(copy.deepcopy(curidx[0:num_to_exclude[u]]))
        inclassidx[u] = copy.deepcopy(curidx[num_to_exclude[u]:])

    allretainidx = []
    for u in range(0, Nclasses):
        if len(allretainidx) == 0:
            allretainidx = copy.deepcopy(inclassidx[u])
        else:
            allretainidx = np.concatenate((allretainidx,inclassidx[u]))

    allretainidx = np.sort(allretainidx)

    new_int_set = int_set[allretainidx]
    Y_int = Y[new_int_set,:]
    X_int = X[new_int_set,:]

    # Define additional test set indices for later reporting / filtering
    test_set = np.setdiff1d(np.arange(0,Ns), new_int_set)
    Y_test = Y[test_set,:]
    X_test = X[test_set,:]


    return( X_int, X_test, Y_int, Y_test, test_set )


########################################################################################################################
# Partition data into "balanced" internal training and external evaluation data.  Balance via upsampling smaller classes
def partition_data_balanced_upsampling(X, Y, test_ratio = 0.33, test_set = None, pred_type = 'classification'):
    if pred_type == 'regression':
        exit('Need to implement balanced training sets for regression modeling...')

    Ns = np.size(Y,0)
    Ntest = int(np.round(test_ratio*Ns))


    uclasses = np.unique(Y)
    Nclass = np.size(uclasses)
    num_in_class = np.zeros(Nclass, dtype=np.int32)
    Nest_per_class = Ntest / Nclass
    # Upsample the test set to balance out the test set
    test_set = []
    for u in range(0, Nclass):
        curidx = np.where(Y == uclasses[u])[0]
        Ncurclass = np.size(curidx)
        randidx = np.random.permutation(Ncurclass)
        for j in range(0, Nest_per_class):
            test_set.append(copy.deepcopy(curidx[randidx[j]]))
    test_set = np.array(test_set)

    # Select test set proportional to the representation in the total data set
    int_set = np.setdiff1d(np.arange(0,Ns),test_set)
    Y_int = Y[int_set,:]
    Y_test = Y[test_set,:]
    X_int = X[int_set,:]
    X_test = X[test_set,:]
    int_set = int_set.tolist() # indices of internal set samples; unless perfectly balanced, some will get duplicated

    # Balance the internal set
    uclasses = np.unique(Y_int)
    Nclass = np.size(uclasses)
    num_in_class = np.zeros(Nclass, dtype=np.int32)
    for u in range(0, Nclass):
        curidx = np.where( Y_int[:,0] == uclasses[u])[0]
        num_in_class[u] = np.size(curidx)

    num_to_augment = np.int32(np.max(num_in_class) - num_in_class) # num samples to augment to each class

    # Do the augmentation for smaller classes
    for u in range(0, Nclass):
        if num_to_augment[u] == 0:
            continue

        # Select the "most diverse" samples- those with the largest distance from the mean feature vector
        curidx = np.where(Y_int[:, 0] == uclasses[u])[0]
        X_mean = np.mean(X_int[curidx,:],0)
        alldist = np.zeros(num_in_class[u])
        for i in range(0, num_in_class[u]):
            alldist[i] = vector_distance(X_int[curidx[i],:], X_mean)
        sortidx = np.argsort(alldist)[::-1] # descending sort- sample the most diverse molecules first
        augmentidx = []
        for i in range(0, num_to_augment[u]):
            useidx = i % num_in_class[u]
            augmentidx.append(copy.deepcopy(curidx[sortidx[useidx]]))
            int_set.append(copy.deepcopy(augmentidx[-1]))
        augmentidx = np.array(augmentidx)

        Xaugment_scrambled, scrambledidx = scramble_discrete_predictors(X_int[augmentidx,:], Npredvals=2, scramble_frac = 0.1)
        X_int = np.concatenate((X_int, Xaugment_scrambled), axis=0)
        Y_int = np.concatenate((Y_int, Y_int[augmentidx,:]), axis=0)

    int_set = np.array(int_set)

    return( X_int, X_test, Y_int, Y_test, test_set, int_set )


########################################################################################################################
# Partition data into "balanced" internal training and external evaluation data.  Balance via downsampling larger classes
def partition_data_balanced_downsampling(X, Y, test_ratio = 0.33, test_set = None, pred_type = 'classification', augment_test_set = True):
    if pred_type == 'regression':
        exit('Need to implement balanced training sets for regression modeling...')

    Ns = np.size(Y,0)
    Ntest = int(np.round(test_ratio*Ns))
    uclasses = np.unique(Y)
    Nclass = np.size(uclasses)
    Nest_per_class = Ntest / Nclass

    # Upsample the test set to balance out the test set
    test_set = []
    for u in range(0, Nclass):
        curidx = np.where(Y == uclasses[u])[0]
        Ncurclass = np.size(curidx)
        randidx = np.random.permutation(Ncurclass)
        for j in range(0, Nest_per_class):
            test_set.append(copy.deepcopy(curidx[randidx[j]]))
    test_set = np.array(test_set)

    int_set = np.setdiff1d(np.arange(0,Ns),test_set)
    Y_int = Y[int_set,:]

    # Balance the internal set
    uclasses = np.unique(Y_int)
    Nclasses = np.size(uclasses)
    num_in_class = np.zeros(Nclasses)
    inclassidx = []
    for u in range(0, Nclasses):
        curidx = np.where( Y_int[:,0] == uclasses[u])[0]
        num_in_class[u] = np.size(curidx)
        inclassidx.append(copy.deepcopy(curidx))

    min_cts_class = np.min(num_in_class)
    minidx = np.min(np.where(num_in_class == min_cts_class)[0])
    num_to_exclude = np.zeros(Nclasses, dtype=np.int32)
    excludeidx = []

    for u in range(0, Nclasses):
        if u == minidx:
            excludeidx.append([])
            continue
        num_to_exclude[u] = num_in_class[u] - min_cts_class

        # Do the exclusion
        curidx = copy.deepcopy(inclassidx[u])
        np.random.shuffle(curidx)
        excludeidx.append(copy.deepcopy(curidx[0:num_to_exclude[u]]))
        inclassidx[u] = copy.deepcopy(curidx[num_to_exclude[u]:])

    allretainidx = []
    for u in range(0, Nclasses):
        if len(allretainidx) == 0:
            allretainidx = copy.deepcopy(inclassidx[u])
        else:
            allretainidx = np.concatenate((allretainidx,inclassidx[u]))

    allretainidx = np.sort(allretainidx)

    new_int_set = int_set[allretainidx]
    Y_int = Y[new_int_set,:]
    X_int = X[new_int_set,:]

    # If desired, augment the test set to include the excluded samples from the larger class
    if augment_test_set == True:
        test_set = np.setdiff1d(np.arange(0,Ns), new_int_set)

    Y_test = Y[test_set,:]
    X_test = X[test_set,:]

    return( X_int, X_test, Y_int, Y_test, test_set )



########################################################################################################################
# Randomly select samples while ensuring that the sampling is evenly distributed through the range of activities
def distributed_sampling(Y, Ntest):
    Ns = np.size(Y,0)
    if Ntest > Ns:
        exit('!!! In distributed_sampling(), passed in more test samples (%d) than total available samples (%d)' % (Ntest,Ns) )

    # sortidx = np.argsort(Y,0)
    y_min = np.min(Y,0)
    y_max = np.max(Y,0)
    bin_edges = np.linspace(y_min, y_max, Ntest+1)
    Nbins = int(np.size(bin_edges))

    # Select samples randomly in each of the "bins"
    Ncts_bin = np.zeros(Nbins-1)
    Nunused_bin = np.zeros(Nbins-1)
    testidx = -np.ones(Ntest, dtype=np.int32)
    unusedsamplesidx = [] # list of all samples from all bins that weren't randomly selected for the test set
    for n in range(0, Nbins-1):
        greateridx = np.where(Y >= bin_edges[n])[0]
        lesseridx  = np.where(Y < bin_edges[n+1])[0]
        incurbinidx = np.intersect1d(greateridx,lesseridx) # indices of samples in current bin
        Ncts_bin[n] = np.size(incurbinidx)
        if Ncts_bin[n] <= 0:
            unusedsamplesidx.append([])
            continue
        # Draw a sample at random from the array of possible values in the current bin
        np.random.shuffle(incurbinidx)
        if np.size(incurbinidx) > 0:
            curunusedsamplesidx = incurbinidx[1:]
        else:
            curunusedsamplesidx = []
        testidx[n] = incurbinidx[0]
        unusedsamplesidx.append(copy.deepcopy(curunusedsamplesidx))
        Nunused_bin[n] = np.size(curunusedsamplesidx)

    # In the event that not all bins got filled, fill in bins that didn't get populated
    #   Sample preferentially from bins with higher counts.  This ensures that the test set is more representative of
    #   the data set as a whole
    nofillidx = np.where( testidx < 0)[0]
    if np.size(nofillidx) == 0:
        return( testidx )

    bin_sample_prob = Nunused_bin / float(np.sum(Nunused_bin))
    sumprobs = np.cumsum(bin_sample_prob)
    while( np.size(nofillidx) > 0):
        for n in range(0, np.size(nofillidx) ):
            # Determine the bin to draw a sample from.  Get a uniformly distributed random number
            urand = np.random.uniform(0.0,1.0)
            probbinidx = np.where( sumprobs > urand)[0]
            if np.size(probbinidx) == 0:
                continue
            probbinidx = np.min(probbinidx)
            cursamplesidx = unusedsamplesidx[probbinidx]
            if np.size( cursamplesidx) == 0:
                continue

            testidx[nofillidx[n]] = cursamplesidx[0]
            # Exclude the sample that was just selected
            if np.size(cursamplesidx) > 0:
                cursamplesidx = cursamplesidx[1:]
            else:
                cursamplesidx = []
            unusedsamplesidx[probbinidx] = copy.deepcopy(cursamplesidx)

        nofillidx = np.where(testidx < 0)[0]
    testidx = np.array(testidx)
    return( testidx )


########################################################################################################################
# Implements the distributed sampling algorithm for selecting test set samples in a representative manner
def distributed_sampling_class(class_labels, Ntest):

    ulabels = np.unique(class_labels)
    Nclass = np.size(ulabels)
    Ns_class = np.zeros(Nclass,dtype=np.float64)
    Ns_total = np.size(class_labels,0)
    for i in range(0,Nclass):
        curidx = np.where(class_labels == ulabels[i])[0]
        Ns_class[i] = np.size(curidx)

    frac_class = Ns_class / Ns_total

    # Determine # of samples to draw from each class
    Nselect_class = np.int32(np.round(np.ceil(frac_class * Ntest)))
    testidx = []
    for i in range(0, Nclass):
        curidx = np.where(class_labels == ulabels[i])[0]
        randidx = np.random.permutation(np.size(curidx))
        curidx = curidx[randidx[0:Nselect_class[i]]]

        if i == 0:
            testidx = copy.deepcopy(curidx)
        else:
            testidx = np.concatenate((testidx,curidx))

    return( testidx )




########################################################################################################################
# Partition data into internal training and external evaluation data while sampling the desired number of samples per
#   cluster (must be determined previously)
def partition_data_clusters(X, Y, Ntest, clust_labels, Ns_per_clust, idx_per_clust):
    Ns = np.size(X,0) # total number of samples, training + test
    Ntrain = Ns - Ntest # total number of training samples
    Npred = np.size(X,1) # num predictor variables
    Nresp = np.size(Y,1) # num response variables

    # If test set isn't specified, randomly divide data into training (internal) / test (external) sets
    is_in_test = np.zeros(Ns) # indicator if a given sample is in the external validation set or not

    Nclusts = len(idx_per_clust) # number of unique clusters in entire data set
    for c in range(0, Nclusts):
        # Randomly select "Ns_per_clust" for samples (without replacement) from the current cluster
        randidx = np.random.choice(idx_per_clust[c],Ns_per_clust[c],replace=False)
        is_in_test[randidx] = 1

    X_train = np.zeros((Ntrain,Npred))
    X_test = np.zeros((Ntest,Npred))
    Y_train = np.zeros((Ntrain,Nresp))
    Y_test = np.zeros((Ntest,Nresp))
    testct = 0
    trainct = 0
    for n in range(0, Ns ):
        if( is_in_test[n]):
            X_test[testct,:] = np.squeeze(X[n,:])
            Y_test[testct,:] = np.squeeze(Y[n,:])
            testct += 1
        else:
            X_train[trainct,:] = np.squeeze(X[n,:])
            Y_train[trainct,:] = np.squeeze(Y[n,:])
            trainct += 1
    test_set = np.where(is_in_test > 0)[0]

    return( X_train, X_test, Y_train, Y_test, test_set)


########################################################################################################################
# Clusters data into groups and returns the cluster label for each sample
#   This function also performs checks to ensure that there are enough samples in each cluster to perform a test / train
#       split, i.e. need > 1 sample per cluster.  If not the case, raises an error and terminates the program
#   This function also determines how many samples to select from each class to serve in the "test" set, given the number
#       of test samples
def cluster_data(X, Nclust, Ntest):

    # Do the clustering on the input data
    kmclust = KMeans(n_clusters=Nclust)
    kmclust.fit(X)
    clust_labels = copy.deepcopy(kmclust.labels_)

    # Determine if there are enough samples per cluster
    ulabels = np.unique(clust_labels)
    Nuniq = np.size(ulabels)
    num_per_label = np.zeros(Nuniq)
    idx_per_clust = []
    for n in range(0, Nuniq):
        curidx = np.int32(np.where( clust_labels == ulabels[n])[0])
        num_per_label[n] = np.size(curidx)
        idx_per_clust.append(copy.deepcopy(curidx))
    if np.any( num_per_label < 2 ):
        # Need at least two samples per cluster- otherwise that sample will ALWAYS get used for cross validation, never for training
        exit('\n\n!!! Not enough examples for each of the %d clusters in the input data to use for evaluation' % (Nuniq) )

    # Determine how many samples from each cluster to select when choosing a holdout "test" set
    if Ntest == 0:
        Ns_per_clust = copy.deepcopy(num_per_label)
    else:
        Ns = np.size(X,0) # total num samples present
        frac_per_label = num_per_label / float(Ns)
        Ns_per_clust = np.round(frac_per_label * Ntest)
        Ns_per_clust[Ns_per_clust == 0] = 1 # ensure that we choose at least 1 sample per cluster
        Ns_per_clust = np.int32(Ns_per_clust)
        Ntest = int(np.sum(Ns_per_clust)) # number in holdout test set might have gotten altered

    return( clust_labels, Ns_per_clust, idx_per_clust, Ntest )



########################################################################################################################
# Given an ensemble of trained models, generates a consensus prediction based on the median, mean prediction of all
#   models along with the uncertainty of each prediction
def ensemble_prediction(model, X, return_individual_preds = False):
    Nmod = len(model.all_models)
    Ns = np.size(X,0)

    Y_pred = np.zeros((Nmod,Ns))
    for n in range(0, Nmod):

        if model.ml_type.lower() == 'ann':
            Y_pred_cur = apply_ann(model.all_models[n], X)
        elif model.ml_type.lower() == 'pls':
            Y_pred_cur = pls.apply_pls_regr(model.all_models[n], X)
        elif model.ml_type.lower() == 'knn':
            Y_pred_cur = knn.apply_knn_regr(model.all_models[n], X)
        elif model.ml_type.lower() == 'xgboost':
            Y_pred_cur = model.all_models[n].predict(X)


        Y_pred[n,:] = np.squeeze(Y_pred_cur)

    Y_pred_mean = np.mean(Y_pred,0).reshape((Ns,1))
    Y_pred_med = np.median(Y_pred,0).reshape((Ns,1))
    Y_pred_std = np.std(Y_pred,0).reshape((Ns,1))

    if return_individual_preds == True:
        return (Y_pred_med, Y_pred_mean, Y_pred_std, Y_pred)

    return( Y_pred_med, Y_pred_mean, Y_pred_std )


########################################################################################################################
# Computes median consensus predictions for valid and training samples
def ensemble_prediction_valid(model, X, return_individual_preds = False):
    Ns = np.size(X,0) # total number of samples to evaluate
    Npred = np.size(X,1) # dimensionality of predictors
    Y_valid_med = np.zeros(Ns) + np.inf
    Y_valid_std = np.zeros(Ns) + np.inf
    Y_train_med = np.zeros(Ns) + np.inf
    Y_train_std = np.zeros(Ns) + np.inf
    valid_use_ct = np.zeros(Ns)

    for m in range(0, Ns):
        all_pred_acts = np.zeros(model.Nmodels)
        in_valid = np.zeros(model.n_models,dtype=bool)
        for n in range(0, model.n_models):
            # Generate prediction of the current model for the current molecule
            if model.ml_type == 'ann':
                all_pred_acts[n] = apply_ann(model.all_models[n], X[m,:].reshape((1,Npred)))
            elif model.ml_type == 'pls':
                all_pred_acts[n] = pls.apply_pls_regr(model.all_models[n], X[m,:])
            elif model.ml_type == 'knn':
                all_pred_acts[n] = knn.apply_knn_regr(model.all_models[n], X[m,:].reshape((1,Npred)) )
            elif model.ml_type == 'xgboost':
                all_pred_acts[n] = model.all_models[n].predict(X[m,:].reshape((1,Npred)))

            # Determine if current molecule is in training or test set for curret model
            intsct_valid = np.intersect1d(m, model.valid_sets[n])
            in_valid[n] = False
            if( np.size(intsct_valid) > 0 ):
                in_valid[n] = True
        # Compute consensus (median) predictions and std devs of activities of current molecule for training and CV sets
        valididx = np.where(in_valid == True)[0]
        trainidx = np.where(in_valid == False)[0]
        if( np.size(valididx) > 0):
            Y_valid_med[m] = np.median(all_pred_acts[valididx])
            Y_valid_std[m] = np.std(all_pred_acts[valididx])
            valid_use_ct[m] = np.size(valididx)
        if( np.size(trainidx) > 0):
            Y_train_med[m] = np.median(all_pred_acts[trainidx])
            Y_train_std[m] = np.std(all_pred_acts[trainidx])

    Y_valid_med = Y_valid_med.reshape((Ns, 1))
    Y_valid_std = Y_valid_std.reshape((Ns, 1))
    Y_train_med = Y_train_med.reshape((Ns, 1))
    Y_train_std = Y_train_std.reshape((Ns, 1))

    # Deal with +- infinity predictions
    infidx = np.where(Y_valid_med == np.inf)[0]
    Y_valid_med[infidx] = 0.0
    infidx = np.where(Y_valid_std == np.inf)[0]
    Y_valid_std[infidx] = 0.0
    infidx = np.where(Y_train_med == np.inf)[0]
    Y_train_med[infidx] = 0.0
    infidx = np.where(Y_train_std == np.inf)[0]
    Y_train_std[infidx] = 0.0

    if return_individual_preds == True:
        return (Y_valid_med, Y_valid_std, Y_train_med, Y_train_std, valid_use_ct, all_pred_acts[valididx], all_pred_acts[trainidx])

    return( Y_valid_med, Y_valid_std, Y_train_med, Y_train_std, valid_use_ct )


########################################################################################################################
# Generate consensus predictions for all samples from an ensemble of models
def ensemble_pred_class(models, X, return_individual_preds=False):


    for i in range(models.n_tasks):
        n_s = np.size(X,0) # total number of samples to evaluate
        Npred = np.size(X,1) # dimensionality of predictors
        # Nclass = np.size(unique_classes)
        Y_med = np.zeros((n_s,models.n_class[i])) # median predicted probability
        Y_std = np.zeros((n_s,models.n_class[i])) # standard deviation of predicted probabilities
        cons_class_preds = np.zeros((n_s, models.n_tasks), dtype=np.int32)


        for m in range(0, n_s):
            all_pred_classes = np.zeros(models.n_models)
            all_class_prob = np.zeros((models.n_models, models.n_class[i]))
            for n in range(0, models.n_models):
                if models.ml_type.lower() == 'ann':
                    all_class_prob[n, :], all_pred_classes[n] = apply_ann_class(models.all_models[n], X[m,:].reshape((1,Npred)), binary_thresh=models.preds.bin_class_thresh[i])
                elif models.ml_type.lower() == 'pls':
                    all_class_prob[n,:], all_pred_classes[n] = pls.apply_pls_class(models.all_models[n], X[m,:].reshape((1,Npred)), models.n_class[i], binary_thresh=models.preds.bin_class_thresh[i])
                elif models.ml_type.lower() == 'knn':
                    all_class_prob[n,:], all_pred_classes[n] = knn.apply_knn_class(models.all_models[n], X[m,:].reshape((1,Npred)), binary_thresh=models.preds.bin_class_thresh[i])
                elif models.ml_type.lower() == 'xgboost':
                    all_class_prob[n,:] = models.all_models[n].predict_proba(X[m,:].reshape((1,Npred)) )
                    all_pred_classes[n] = class_from_probs(all_class_prob[n,:].reshape((1,models.n_class[i])), bin_class_thresh=models.preds.bin_class_thresh[i])


            # Compute consensus (median) predictions and std devs of activities of current molecule for training and CV sets
            Y_med[m, :] = np.median(all_class_prob, 0)
            Y_std[m, :] = np.std(all_class_prob, 0)

            if models.n_class[i] == 2 and models.preds.bin_class_thresh[i] is not None:
                if Y_med[m,1] >= models.preds.bin_class_thresh[i]:
                    cons_class_preds[m] = 1
                else:
                    cons_class_preds[m] = 0
            else:
                cons_class_preds[m] = int(np.min(np.where(Y_med[m,:] == np.max(Y_med[m,:]))[0]))


    return( Y_med, Y_std, cons_class_preds )



########################################################################################################################
# Enumerates all possible testing / training set splits.  Useful for when total # samples is small
def enumerate_all_partitions(Ntrain, Ntest, max_iters = 200):
    Ns = Ntrain + Ntest
    train_sets = []
    test_sets = []

    Ncombs = np.int32(np.math.factorial(Ns) / (np.math.factorial(Ns - Ntest) * np.math.factorial(Ntest)))
    print('\nThere are %d total combinations of %d numbers tested %d at a time\n' % (Ncombs,Ns,Ntest))
    if Ncombs > max_iters:
        print('Too many test sets to evaluate all combinations!  Skipping exhaustive evaluation of all combinations!!!\n')
        return ( train_sets, test_sets )


    all_nums = np.arange(0,Ns)
    for curtest_set in itertools.combinations(all_nums, Ntest):
        curtrain_set = np.setdiff1d(all_nums, curtest_set)

        train_sets.append(curtrain_set)
        test_sets.append(curtest_set)

    return ( train_sets, test_sets )


########################################################################################################################
# Computes MOCC (Margin of Correct Classification) for a prediction
#   If a prediction is correct, MOCC tells how close (# std devs) that prediction is to being wrong
#   If a prediction is incorrect, MOCC is negative and tells how close (# std devs) that prediction is to being correct
def margin_correct_class(predcls, truecls, clsprobs, probstd):
    Ns = np.size(predcls,0)
    mocc = np.zeros((Ns,1))

    for n in range(0, Ns):
        curprobs = clsprobs[n,:]
        sortprobs = np.sort(curprobs)
        if predcls[n] == truecls[n]:
            # Correct prediction
            if probstd[n,0] == 0.0:
                mocc[n] = np.inf
            else:
                mocc[n] = (sortprobs[-1] - sortprobs[-2])/probstd[n,0]
        else:
            # Incorrect prediction
            if probstd[n,0] == 0.0:
                mocc[n] = -np.inf
            else:
                trueclsidx = int(round(truecls[n,0]))
                predclsidx = int(round(predcls[n,0]))
                mocc[n] = (curprobs[trueclsidx] - curprobs[predclsidx]) / probstd[n,0]

    return( mocc )


########################################################################################################################
# Generates simulated responses (activities) for
# Input:
# X,    Ns x Npred matrix of input data (# samples x # predictor variables)
# Output:
# Y,    Ns x 1 matrix of response data
def generate_responses_sim(X, simidx = 1):
    Ns = np.size(X,0) # number of samples to generate predictions for
    Npred = np.size(X,1) # number of predictor variables for each samples
    Y = np.zeros((Ns,1))

    if simidx == 1:
        # Some linear generative models
        coefs = np.zeros(Npred)
        coefs[0] = 1.0
        for n in range(1, Npred):
            sign = 1.0
            if n % 7 == 0:
                sign *= -1.0
            coefs[n] = 0.975*coefs[n-1]*sign

        for n in range(0, Ns):
            Y[n] = np.vdot(coefs, X[n,:]) #+ np.sum(X[n,:])
        Y = 10.0 * (Y -np.min(Y))/ (np.max(Y) - np.min(Y))

    elif simidx == 2:
        for n in range(0, Ns):
            Y[n] = (np.sum(X[n,:]))**2.0
        Y = 10.0 * (Y - np.min(Y)) / (np.max(Y) - np.min(Y))

    elif simidx == 3:
        indidx = 2*np.arange(1,Npred+1)
        for n in range(0, Ns):
            Y[n] = np.vdot(X[n,:], indidx)

    elif simidx == 4:
        # Nonlinear generative model
        for n in range(0, Ns):
            occidx = np.where(X[n,:] > 0)[0]
            if np.size(occidx) > 0:
                Y[n] = np.sum(np.exp(occidx))
    elif simidx == 5:
        # Activity is a sum of a linear and a nonlinear function
        for n in range(0, Ns):
            occidx = np.where(X[n,:] > 0)[0]
            if np.size(occidx) > 0:
                # Y[n] = np.sum(np.exp(occidx)) + np.vdot(coefs, X[n,:])
                Y[n] = np.sum(occidx**2.0)

    return( Y )


########################################################################################################################
# Round a floating point number to the nearest class label
def round_nearest_class(Y_float, Nclass):
    Ns = np.size(Y_float,0)
    Y_class = np.zeros((Ns,1))
    all_classes = np.arange(0, Nclass)
    # This loop could be vectorized for improved efficiency, but it's probably nowhere near a bottleneck
    for n in range(0, Ns):
        dist = np.abs( Y_float[n] - all_classes )
        mindist = np.min(dist)
        Y_class[n] = np.min(np.where(dist == mindist))

    return( Y_class)




########################################################################################################################
# Scales data as per a predefined method.
#   This function assumes that separate variables are in separate columns
#   Data is rescaled per variable (and not per sample)
def set_scaling(scale_params, Z):

    n_s = np.size(Z,0) # number of sampels to perform the rescaling on
    n_tasks = np.size(Z,1) # number of variables to rescale

    Zscaled = np.zeros((n_s,n_tasks)) * np.nan

    # Data should be rescaled on a per-task basis.  This is because different tasks can have incommensurate magnitudes
    # Determine where we don't have missing values (for each task)
    notnanidx = []
    for i in range(n_tasks):
        curnotnanidx = np.where(np.isnan(Z[:,i]) == False)[0]
        notnanidx.append(curnotnanidx)

    if scale_params.method == None:
        # Default scenario- no scaling applied
        Zscaled = copy.deepcopy(Z)
        scale_params.scaling_set = True
        scale_params.method = 'none'

    elif scale_params.method.lower() == 'none':
        # Default scenario- no scaling applied
        Zscaled = copy.deepcopy(Z)
        scale_params.scaling_set = True

    elif scale_params.method.lower() == 'standardize':
        # Convert data to a standard normal variate
        scale_params.meanvals = np.zeros(n_tasks)
        scale_params.stdvals = np.zeros(n_tasks)
        for i in range(n_tasks):
            scale_params.meanvals[i] = np.mean(Z[notnanidx[i],i])
            scale_params.stdvals[i] = np.std(Z[notnanidx[i],i])

        # Prevent rescaling to nan's for columns where all values are the same
        zeroidx = np.where( scale_params.stdvals == 0.0)[0]
        scale_params.stdvals[zeroidx] = 1.0 # setting nans to 1 doesn't change the bin values when applying / inverting scaling

        for i in range(n_tasks):
            Zscaled[notnanidx[i],i] = (Z[notnanidx[i],i] - scale_params.meanvals[i]) / scale_params.stdvals[i]

        scale_params.scaling_set = True

    elif scale_params.method.lower() == 'minmaxscale':
        # rescale data linearly between the min and max values for each task
        scale_params.minvals = np.zeros(n_tasks)
        scale_params.maxvals = np.zeros(n_tasks)
        for i in range(n_tasks):
            scale_params.minvals[i] = np.min(Z[notnanidx[i],i])
            scale_params.maxvals[i] = np.max(Z[notnanidx[i],i])

            # Rescaled values will be between [0,1]
            Zscaled[notnanidx[i],i] = (Z[notnanidx[i],i] - scale_params.minvals[i]) / (scale_params.maxvals[i] - scale_params.minvals[i])

        scale_params.scaling_set = True

    elif scale_params.method.lower() == 'robustscale':
        # This scaling method depends on order statistics and doesn't assume a particular distribution of data.  It is
        #   similar to the minmaxscale method but it uses order statistics instead of min and max values for the scale params
        #   The idea is to not have the data rescaling impacted by outlier samples at either extreme.  It is a
        #   generalization of the simple linear contrast stretch
        scale_params.medvals = np.zeros(n_tasks)
        scale_params.vals25pct = np.zeros(n_tasks)
        scale_params.vals75pct = np.zeros(n_tasks)
        for i in range(n_tasks):
            scale_params.medvals[i] = np.median(Z[notnanidx[i],i])
            sortvals = np.sort(Z[notnanidx[i],i])
            idx25 = int(float(n_s)/4.0)
            idx75 = int(3.0*float(n_s)/4.0)
            scale_params.vals25pct[i] = sortvals[idx25]
            scale_params.vals75pct[i] = sortvals[idx75]
            Zscaled[notnanidx[i],i] = np.squeeze((Z[notnanidx[i],i] - scale_params.medvals[i]) / (scale_params.vals75pct[i] - scale_params.vals25pct[i]))

        scale_params.scaling_set = True

    elif scale_params.method.lower() == 'log10':
        # Applies a base 10 log transform to data
        Zscaled = np.log10(Z)
        scale_params.scaling_set = True

    elif scale_params.method.lower() == 'exponential':
        # Applies a Z^10 scaling to the input data (per element)
        Zscaled = np.power(Z,10.0)
        # Zscaled = np.exp(Z)
        scale_params.scaling_set = True


    return( scale_params, Zscaled)


########################################################################################################################
# Applies previously established data scaling method to a new sample
#   This function assumes that separate variables are in separate columns
#   Data is rescaled per variable (and not per sample)
def apply_scaling(scale_params, Z):

    if scale_params.scaling_set == False:
        print('Data scaling parameters were never set!!!')
        exit()

    Zscaled = np.zeros((np.size(Z,0),np.size(Z,1)) ) * np.nan
    n_s = np.size(Z,0) # number of sampels to perform the rescaling on
    n_tasks = np.size(Z,1) # number of tasks to rescale

    # Determine where we don't have missing values (for each task)
    notnanidx = []
    for i in range(n_tasks):
        curnotnanidx = np.where(np.isnan(Z[:,i]) == False)[0]
        notnanidx.append(curnotnanidx)


    if scale_params.method == None:
        # Default scenario- no scaling applied
        Zscaled = copy.deepcopy(Z)

    elif scale_params.method.lower() == 'none':
        # Default scenario- no scaling applied
        Zscaled = copy.deepcopy(Z)

    elif scale_params.method.lower() == 'standardize':
        # Convert data to a standard normal variate
        for i in range(n_tasks):
            Zscaled[notnanidx[i],i] = (Z[notnanidx[i],i] - scale_params.meanvals[i]) / scale_params.stdvals[i]

    elif scale_params.method.lower() == 'minmaxscale':
        # rescale data linearly between the min and max values for each variable
        for i in range(n_tasks):
            Zscaled[notnanidx[i],i] = (Z[notnanidx[i],i] - scale_params.minvals[i]) / (scale_params.maxvals[i] - scale_params.minvals[i])

    elif scale_params.method.lower() == 'robustscale':
        # This scaling method depends on order statistics and doesn't assume a particular distribution of data.  It is
        #   similar to the minmaxscale method but it uses order statistics instead of min and max values for the scale params
        #   The idea is to not have the data rescaling impacted by outlier samples at either extreme.  It is a
        #   generalization of the simple linear contrast stretch
        for i in range(n_tasks):
            Zscaled[notnanidx[i],i] = (Z[notnanidx[i],i] - scale_params.medvals[i]) / (scale_params.vals75pct[i] - scale_params.vals25pct[i])

    elif scale_params.method.lower() == 'log10':
        # Applies a base 10 log transform to data
        Zscaled = copy.deepcopy(Z)
        if np.any(Zscaled) <= 0.0:
            exit('Negative values encountered when trying to log10 transform data.  Ensure positive values before trying')
        Zscaled = np.log10(Zscaled)

    elif scale_params.method.lower() == 'exponential':
        # Applies a Z^10 scaling to the input data
        Zscaled = np.power(Z,10.0)

    return( Zscaled)


########################################################################################################################
# "Unscales" previously scaled data to get back to "raw" input format
#   This function assumes that separate variables are in separate columns
#   Data scaling / inverse scaling is carried out per variable (and not per sample)
def invert_scaling(scale_params, Zscaled, mean_center = True):

    if scale_params.scaling_set == False:
        print('Data scaling parameters were never set.  Nothing to invert!')
        return(Zscaled)

    n_s = np.size(Zscaled,0) # number of samples to perform the rescaling on
    n_tasks = np.size(Zscaled,1) # number of variables to rescale
    Z = np.zeros((n_s, n_tasks))

    # Determine where we don't have missing values (for each task)
    notnanidx = []
    for i in range(n_tasks):
        curnotnanidx = np.where(np.isnan(Z[:,i]) == False)[0]
        notnanidx.append(curnotnanidx)

    if scale_params.method == None:
        # Default scenario- no scaling applied
        Z = copy.deepcopy(Zscaled)

    elif scale_params.method.lower() == 'none':
        # Default scenario- no scaling applied
        Z = copy.deepcopy(Zscaled)

    elif scale_params.method.lower() == 'standardize':
        # Convert data from standard normal variate back to original scaling
        for i in range(0, n_tasks):
            if mean_center == True:
                Z[notnanidx[i],i] = (Zscaled[notnanidx[i],i] * scale_params.stdvals[i]) + scale_params.meanvals[i]
            else:
                Z[notnanidx[i],i] = (Zscaled[notnanidx[i],i] * scale_params.stdvals[i])

    elif scale_params.method.lower() == 'minmaxscale':
        # Undo the [0,1] scaling
        for i in range(n_tasks):
            Z[notnanidx[i],i] = Zscaled[notnanidx[i],i]*(scale_params.maxvals[i] - scale_params.minvals[i]) + scale_params.minvals[i]

    elif scale_params.method.lower() == 'robustscale':
        # Undo the "robust" scaling
        for i in range(n_tasks):
            Z[notnanidx[i],i] = np.squeeze(Zscaled[notnanidx[i],i]*(scale_params.vals75pct[i] - scale_params.vals25pct[i]) + scale_params.medvals[i])

    elif scale_params.method.lower() == 'log10':
        # Applies a 10^Z transform to get back to original scale
        Z = np.power(10.0, Zscaled)

    elif scale_params.method.lower() == 'exponential':
        # Applies a log10 transform to get back to original scale
        Z = np.log10(Zscaled)

    return(Z)

########################################################################################################################
# Converts a list of 1d arrays to a single 2d array.  The inverse of mat_to_list()
def list_to_mat(X_list):
    n_cols = len(X_list)
    if n_cols == 0:
        return(X_list)

    n_rows = int(np.size(X_list[0]))
    X_mat = np.zeros((n_rows, n_cols))
    for i in range(n_cols):
        X_mat[:,i] = np.squeeze(X_list[i])

    return(X_mat)

########################################################################################################################
# Converts a 2d array into a list of 1d arrays (each matrix column a new list element).  The inverse of list_to_mat()
def mat_to_list(X_mat):
    n_cols = X_mat.shape[1]
    if n_cols == 0:
        return(X_mat)

    X_list = []
    for i in range(n_cols):
        X_list.append(X_mat[:,i])

    return(X_list)


########################################################################################################################
def applicability_metrics(modelvec, molvec):
    Nbinsused_model = np.size(modelvec)
    Nbinsused_mol = np.size(molvec)

    if Nbinsused_model != Nbinsused_mol:
        exit('Inconsistent # bins compared for model (%d) and current molecule (%d) !!!' % (Nbinsused_model, Nbinsused_mol))

    ad_stats = APPLICABILITY_DOMAIN()

    # Bin overlap stats
    ad_stats.binoccs_overlap = np.size(np.where(molvec != 0)[0])
    ad_stats.binoccs_overlap_frac = ad_stats.binoccs_overlap / float(Nbinsused_model)

    # Tanimoto coefficient stats
    proj_modelmol = np.vdot(modelvec, molvec)
    norm_model = np.linalg.norm(modelvec)
    norm_mol = np.linalg.norm(molvec)
    ad_stats.tanimoto_coef = proj_modelmol / (norm_model**2.0 + norm_mol**2.0 - proj_modelmol)

    # Hamming coefficient stats.  Hamming coefficient is a [0,1] normalized Hamming distance
    Ndiff = np.size(np.where( modelvec != molvec)[0])
    ad_stats.hamming_coef = float(Ndiff) / Nbinsused_model

    binary_modelvec = copy.deepcopy(modelvec)
    binary_modelvec[binary_modelvec != 0.0] = 1.0
    binary_molvec = copy.deepcopy(molvec)
    binary_molvec[binary_molvec != 0.0] = 1.0
    Ndiff = np.size(np.where(binary_modelvec != binary_molvec)[0])
    ad_stats.hamming_coef = float(Ndiff) / Nbinsused_model

    ad_stats.refmodelvec = copy.deepcopy(modelvec)

    ad_stats

    return( ad_stats )


########################################################################################################################
# Calculates the Tanimoto coefficient between two vectors.  Normalizes the distance if desired
def tanimoto_coefficient(vec0, vec1):

    proj01 = np.vdot(vec0, vec1)
    norm0 = np.linalg.norm(vec0)
    norm1 = np.linalg.norm(vec1)
    if norm0 == 0.0 and norm1 == 0.0:
        tcoef = 0.0
    else:
        tcoef = proj01 / (norm0**2.0 + norm1**2.0 - proj01)

    return( tcoef )


########################################################################################################################
def bayesian_prediction_class(all_probs, lhood_mats, decision_thresh=0.5, decision_class=1):
    Ns = np.size(all_probs,0)
    Nclass = np.size(all_probs,1)
    Nmodels = np.size(all_probs,2)
    cons_class_pred = np.zeros((Ns,1))

    # Before applying Bayesian inference rule, do some massaging of likelihood matrices.  This is to ensure that there
    #   are no "impossible" class predictions arising from 0 values in the likelihood matrices.  This can happen when
    #   a model works perfectly on its external test set and makes all predictions of a particular class for a given
    #   true class
    for i in range(0, Nmodels):
        for j in range(0, Nclass):
            zeroidx = np.where(lhood_mats[i][j,:] < 0.01)[0]
            lhood_mats[i][j,zeroidx] = 0.01
            lhood_mats[i][j,:] /= np.sum(lhood_mats[i][j,:]) # renrmalize to unit probability


    post_probs = np.zeros((Ns,Nclass))
    post_probs_model = np.zeros((Ns,Nclass,Nmodels))
    for i in range(0, Ns):
        for j in range(0, Nclass): # loop over true classes
            for k in range(0, Nmodels): # loop over all models
                for m in range(0, Nclass): # loop over all predicted classes
                    post_probs[i,j] += (lhood_mats[k][m,j]*all_probs[i,j,k])
                    post_probs_model[i,j,k] = lhood_mats[k][m,j]*all_probs[i,j,k]

        post_probs[i,:] /= np.sum(post_probs[i,:]) # force sum of probs = 1
        maxidx = np.min(np.where( post_probs[i,:] == np.max(post_probs[i,:]))[0])
        cons_class_pred[i] = maxidx

    # Compute the standard deviation of consensus predictions
    cons_sigma = np.std(post_probs_model,1)

    return( post_probs, cons_sigma, cons_class_pred )


########################################################################################################################
# Finds the correlations between input features and responses.  Returns list of arrays of indices which have have
#   significant correlations.  List is to denote different levels of correlations
def feature_response_correlations(X, Y, base_corr_thresh = 0.1, Nkeep_comb = 10, max_feat_combs = 4, use_abs_corrcoef = False, do_plots = True):

    # Determine what type of correlation to compute
    uvals = np.unique(Y)
    Nunique = np.size(uvals)
    is_binary_classifier = False
    if Nunique == 2:
        is_binary_classifier = True

    Nfeats = np.size(X,1)
    Ns = np.size(X,0)
    corr_thresh = np.zeros(max_feat_combs)
    corr_thresh[0] = base_corr_thresh
    for i in range(1, max_feat_combs):
        # corr_thresh[i] = (corr_thresh[i-1])**0.8
        if i == 1:
            corr_thresh[i] = base_corr_thresh
        else:
            corr_thresh[i] = base_corr_thresh

    # Find the correlations between each feature and the resultant activity
    corrs1 = np.zeros(Nfeats)
    hascorr1 = np.zeros(Nfeats,dtype=np.int32)
    for i in range(0, Nfeats):
        if is_binary_classifier:
            corrs1[i] = binomial_corr_coef(np.squeeze(Y), X[:,i])
        else:
            curcorrs = np.corrcoef(np.squeeze(Y), X[:,i])
            corrs1[i] = curcorrs[0,1]

    if use_abs_corrcoef == True:
        # Using absolute correlation
        binhascorridx = np.where(np.abs(corrs1) >= corr_thresh[0])[0]
        Nhascorr = np.size(binhascorridx)
        curcombsidx = copy.deepcopy(binhascorridx.reshape((Nhascorr,1)))
        all_best_combs = [copy.deepcopy(curcombsidx)]
        all_best_corrs = [copy.deepcopy(np.abs(corrs1[binhascorridx]))]
    else:
        # Using signed correlation
        binhascorridx = np.where(corrs1 >= corr_thresh[0])[0]
        Nhascorr = np.size(binhascorridx)
        curcombsidx = copy.deepcopy(binhascorridx.reshape((Nhascorr,1)))
        all_best_combs = [copy.deepcopy(curcombsidx)]
        all_best_corrs = [copy.deepcopy(corrs1[binhascorridx])]

    # Using the set of individual features which have some correlation to the activity, search for combinations of
    #   features that have the most correlation to the output activity
    for k in range(2, max_feat_combs+1):
        # Get all combinations of "k" features that individually have significant correlations to the activity
        Ncombs = int(scipy.misc.comb(Nhascorr, k))
        allcombs = itertools.combinations(binhascorridx, k)
        print('Searching for best %d-fold features (%d total combinations)' % (k,Ncombs))

        # Loop over all combinations of "k" features
        allmcc_kfold = np.zeros(Ncombs)
        curcombsidx = np.zeros((Ncombs, k), dtype=np.int32)

        for c in range(0, Ncombs):
            curidx = np.array(allcombs.next())
            curcombsidx[c,:] = np.squeeze(curidx)
            Xcur = X[:,curidx]
            allmcc_kfold[c] = multiple_corrcoef(Xcur, Y, corrs1[curidx], is_binary_classifier)

        # At the current # of bins compared, see which combinations of bins had the best correlation to the activity
        binhascorridx = np.where(binhascorridx>0)[0]
        Nhascorr = np.size(binhascorridx)

        keepidx = np.where(allmcc_kfold > corr_thresh[k-1])[0]
        all_best_combs.append(copy.deepcopy(curcombsidx[keepidx,:]))
        all_best_corrs.append(copy.deepcopy(allmcc_kfold[keepidx]))

    # Sort the combinations of bins in order of descending correlation to aid in feature selection
    sorted_best_combs = []
    sorted_best_corrs = []
    for i in range(0, len(all_best_combs)):
        cur_best_corrs_idx = np.argsort(all_best_corrs[i])[::-1]
        ncombs = np.size(all_best_corrs[i])
        cur_best_combs = []
        for j in range(0, ncombs):
            cur_best_combs.append(all_best_combs[i][cur_best_corrs_idx[j]])
        sorted_best_combs.append(copy.deepcopy(cur_best_combs))
        sorted_best_corrs.append(all_best_corrs[i][cur_best_corrs_idx])
    all_best_combs = copy.deepcopy(sorted_best_combs)
    all_best_corrs = copy.deepcopy(sorted_best_corrs)

    # Some visualization of the combinations of bins vs. multicorrelation coefficient
    if do_plots == True:
        import matplotlib.pyplot as plt
        plt.figure(1)
        plot_colors = ['r','g','b','c','m','k']
        for i in range(len(all_best_combs)-1,-1,-1):
            Ncombs = np.size(all_best_corrs[i])
            plt.scatter(np.arange(0,Ncombs), np.sort(all_best_corrs[i]), edgecolor=plot_colors[i],facecolor='None',marker='o',label=str(i+1)+' bin combs')
        plt.xlabel('Feature Combination #' )
        plt.ylabel('Multiple Corr Coef')
        plt.title('Multi Correlation Coefficient Comparison, Feature Combination # vs. Activity')
        plt.grid()
        plt.legend()
        plt.show(block=True)


    return(all_best_combs, all_best_corrs )


########################################################################################################################
# Computes the multiple correlation coefficient between columns of predictors X and responses Y
def multiple_corrcoef(X, Y, corr_y = None, is_binary_classifier = False, sig_level = 0.05):

    Npred = np.size(X,1)
    Ns = np.size(X,0)
    if corr_y is None:
        corr_y = np.zeros((Npred,1))
        for i in range(0, Npred):
            if is_binary_classifier:
                corr_y[i] = binomial_corr_coef(np.squeeze(Y), X[:,i])
            else:
                curcorr = np.corrcoef(np.squeeze(Y), X[:,i])
                corr_y[i] = curcorr[0,1]
    else:
        corr_y = corr_y.reshape((Npred,1))

    Rxx = np.corrcoef(X.T)
    Rxx_pinv = np.linalg.pinv(Rxx)

    MCCsq = np.dot( corr_y.T, np.dot(Rxx_pinv, corr_y) )

    # This is a biased estimator of the multiple corr coef squared.  Remove bias by a correction factor
    # For small correlations this is giving negative squared correlations.  Not robust
    cf = float(Ns - 1)/float(Ns - Npred - 1)
    MCCsq_adj = 1.0 - (1.0 - MCCsq) * cf
    MCCsq_adj[MCCsq_adj<0.0] = 0.0

    # Calculate F ratio to perform a test of statistical significance
    f_ratio = MCCsq_adj / (1.0 - MCCsq_adj) * (Ns - Npred - 1.0)/float(Npred)
    df1 = Npred - 1
    df2 = Ns - 1
    p_value = 1.0 - scipy.stats.f.cdf(f_ratio, df1, df2)
    if p_value < sig_level:
        MCC = np.sqrt(MCCsq_adj)
    else:
        MCC = 0.0

    return( MCC )


########################################################################################################################
# Calculates the "phi", i.e. binomial correlation coefficient, between two binary 1D arrays X and Y
def binomial_corr_coef(X, Y):
    X = np.squeeze(X)
    Y = np.squeeze(Y)
    N = np.size(X) # total number of observations

    xzeroidx = np.zeros(N,dtype=np.int32)
    xoneidx = np.zeros(N, dtype=np.int32)
    yzeroidx = np.zeros(N,dtype=np.int32)
    yoneidx = np.zeros(N, dtype=np.int32)

    xzeroidx[X==0] = 1
    xoneidx[X==1] = 1
    yzeroidx[Y==0] = 1
    yoneidx[Y==1] = 1

    n11idx = np.intersect1d(np.where(xzeroidx==1)[0], np.where(yzeroidx==1)[0])
    n10idx = np.intersect1d(np.where(xzeroidx==1)[0], np.where(yzeroidx==0)[0])
    n01idx = np.intersect1d(np.where(xzeroidx==0)[0], np.where(yzeroidx==1)[0])
    n00idx = np.intersect1d(np.where(xzeroidx==0)[0], np.where(yzeroidx==0)[0])

    n11 = np.size(n11idx)
    n10 = np.size(n10idx)
    n01 = np.size(n01idx)
    n00 = np.size(n00idx)

    nx1 = np.sum(xoneidx)
    nx0 = np.sum(xzeroidx)
    ny1 = np.sum(yoneidx)
    ny0 = np.sum(yzeroidx)

    bcc_numerator = n11*n00 - n10*n01
    bcc_denominator = nx1*nx0*ny0*ny1
    if bcc_denominator <= 0.0:
        bcc = 0.0
    else:
        bcc = float(bcc_numerator) / float(np.sqrt(bcc_denominator))

    return( bcc )



########################################################################################################################
def noise_addition_regression(Y, noise_frac = 0.2):

    n_tasks = Y.shape[1]
    n_s = Y.shape[0]

    if noise_frac < 0.0:
        noise_frac = 0.0

    # Add noise independently for each task, as they might not be commensurate
    Y_noise = copy.deepcopy(Y)
    activity_var = np.var(Y, 0)
    for i in range(n_tasks):

        if activity_var[i] < 0.0:
            activity_var[i] = 0.0

        if noise_frac >= 1:
            Y_noise = np.random.normal(0.0, np.sqrt(activity_var[i]), (n_s, 1))
        else:
            noise_var = noise_frac / (1.0 - noise_frac) * activity_var[i]
            noise_std = np.sqrt(noise_var)

            rand_noise = np.random.normal(0.0, noise_std, (n_s, 1))

            Y_noise[:,i] += np.squeeze(rand_noise)

    return(Y_noise)


########################################################################################################################
# Scrambles class labels for a predefined fraction of samples
def scramble_class_labels(Y, n_class, scramble_frac = 0.1):
    n_tasks = int(np.size(Y,1))

    for t in range(n_tasks):
        notnanidx = np.where(np.isnan(Y[:, t]) == False)[0]
        ns = np.size(notnanidx)
        n_scramble = int(np.round(scramble_frac * ns))

        # Randomly select indices of samples for which class labels will be scrambled
        randpermidx = np.random.permutation(ns)
        scrambleidx = notnanidx[randpermidx[0:n_scramble]]

        classes_curtask = np.arange(0, n_class[t], dtype=np.int32)
        n_wrongclasses = np.size(classes_curtask) - 1

        for i in range(0, n_scramble):
            correct_label = int(Y[scrambleidx[i],t]) # correct class label for current task
            wrong_labels = np.setdiff1d(classes_curtask, correct_label)


            # Randomly select one of the wrong classes to be the label for the current sample
            randwrongidx = np.random.permutation(n_wrongclasses)
            use_label = wrong_labels[randwrongidx[0]]

            Y[scrambleidx[i], t] = np.squeeze(use_label)

            # class_labels = np.unique(Y[notnanidx,t])
            #
            # # Randomly decide which of the incorrect classes to move the current sample into
            # urand = np.random.uniform(0.0, n_class[t]-1)
            # dist_class = np.abs(class_labels - urand)
            # dist_class[correct_class] = np.inf
            # wrong_class = np.where( dist_class == np.min(dist_class))[0]
            # Y[scrambleidx[i],t] = np.squeeze(wrong_class)

    return( Y, scrambleidx )


########################################################################################################################
# Scrambles a predefined fraction of predictors for every sample in X
def scramble_discrete_predictors(X, Npredvals = 2, scramble_frac = 0.1):
    Ns = np.size(X,0)
    Npred = np.size(X,1)
    Nscramble = int(np.round(scramble_frac * Npred))
    pred_values = np.arange(0, Npredvals)

    allscrambleidx = np.zeros((Ns,Nscramble))
    for i in range(0, Ns):
        # Randomly decide which predictors in current sample get incorrect values
        randidx = np.random.permutation(Npred)
        curscrambleidx = randidx[0:Nscramble]

        # Randomly choose incorrect values for the toggled predictors
        urand = np.random.uniform(0.0, Npredvals-1, (Nscramble) )
        for j in range(0, Nscramble):
            dist_pred = np.abs(pred_values - urand[j])
            dist_pred[int(X[i,curscrambleidx[j]])] = np.inf
            wrong_pred_value = np.where( dist_pred == np.min(dist_pred))[0]
            X[i,curscrambleidx[j]] = wrong_pred_value
        allscrambleidx[i,:] = curscrambleidx

    return( X, allscrambleidx )


########################################################################################################################
def vector_distance( V0, V1, distance_type = 'euclidean'):
    dvec = V1 - V0

    if distance_type == 'euclidean':
        distance = np.linalg.norm(dvec)
    elif distance_type == 'hamming':
        distance = np.size(np.where(dvec != 0.0)[0])
    else:
        exit('Unsupported distance metric: %s !!!' % (distance_type))

    return( distance )


########################################################################################################################
# Determines how frequently a feature is present in the given data.  If the model is a classification model and the true
#   class labels per sample are provided then returns frequency on a per-class basis
def feature_occs_frequency(featsidx, X, Y = None, pred_type='classification', class_labels = None):
    ns = np.size(X,0) # total number of samples to check
    nfeats = np.size(featsidx) # total num features to check for the presence of in the feature matrix X

    if pred_type == 'classification' and class_labels is not None:
        nclass = len(class_labels)

    # If we don't know dependent variable values or if we have a regression model then it doesn't make sense to divide
    #   stats up per class.  There is only one "class" in these scenarios- can't subdivide into actives vs. inactives
    if pred_type != 'classification' or Y is None:
        nclass = 1

    binfreqs_per_class = np.zeros((nfeats, nclass))
    noccs_per_class = np.zeros((nfeats, nclass))

    for i in range(nclass):
        # Find all samples in current class
        if nclass == 1:
            curclassidx = np.arange(0,ns, dtype=np.int32)
        else:
            curclassidx = np.int32(np.where( Y == class_labels[i])[0])
        ns_curclass = int(np.size(curclassidx))

        for j in range(ns_curclass):
            curoccidx = np.where(X[curclassidx[j],:] > 0)[0]
            intsctidx = np.intersect1d(featsidx, curoccidx)
            for k in range(np.size(intsctidx)):
                idx = np.where(featsidx == intsctidx[k])[0]
                noccs_per_class[idx,i] += 1
        binfreqs_per_class[:,i] = np.squeeze(noccs_per_class[:,i] / float(ns_curclass))

    return( binfreqs_per_class, noccs_per_class)


########################################################################################################################
# Generates "consensus" regression predictions given a predictive model (or a series of predictive models)
def models_predict(models, X, is_int_set=False, unscale_predictions = False):
    n_s = np.shape(X)[0]

    all_preds = []
    all_preds_std = []
    for i in range(models.n_tasks):
        all_preds.append(np.zeros((n_s, models.n_models)) )

    for i in range(models.n_models):
        cur_preds = model_predict(models.all_models[i], X, models.pred_type, models.ml_type, models.hp)

        if unscale_predictions:
            cur_preds = invert_scaling(models.scaling, list_to_mat(cur_preds))
            cur_preds = mat_to_list(cur_preds)

        for j in range(models.n_tasks):
            all_preds[j][:,i] = np.squeeze(cur_preds[j])

    # Sometimes we need to unscale predictions- for example, when we compare multiple model predictions
    # if unscale_predictions:
    #     unscaled_preds = copy.deepcopy(all_preds)
    #     for i in range
    #     unscaled_preds_test = invert_scaling(models.scaling, list_to_mat(preds_test))


    # Generate consensus predictions from the ensemble of models
    if is_int_set:
        # Need to keep separate sets of books for training, validation sets
        used_in_valid = np.zeros((n_s, models.n_models), dtype=np.bool)
        used_in_train = np.zeros((n_s, models.n_models), dtype=np.bool)
        for i in range(models.n_models):
            used_in_valid[models.valid_sets[i],i] = True
            used_in_train[models.train_sets[i],i] = True

        preds_train = []; preds_train_std = []
        preds_valid = []; preds_valid_std = []

        all_preds_train = copy.deepcopy(all_preds)
        all_preds_valid = copy.deepcopy(all_preds)

        for i in range(models.n_tasks):
            # Default for training / validation set exclusive predictions: predictions are NaNs unless proven otherwise
            cur_preds_train = np.zeros(n_s) * np.nan
            cur_preds_valid = np.zeros(n_s) * np.nan
            cur_preds_train_std = np.zeros(n_s) * np.nan
            cur_preds_valid_std = np.zeros(n_s) * np.nan

            all_preds_train[i] = all_preds[i] * np.nan
            all_preds_valid[i] = all_preds[i] * np.nan

            for j in range(n_s):
                curvalididx = np.where(used_in_valid[j,:] == True)[0]
                curtrainidx = np.where(used_in_train[j,:] == True)[0]

                if np.size(curtrainidx) > 0:
                    cur_preds_train[j] = np.mean(all_preds[i][j,curtrainidx])
                    cur_preds_train_std[j] = np.std(all_preds[i][j,curtrainidx])
                    all_preds_train[i][j,curtrainidx] = all_preds[i][j,curtrainidx]

                if np.size(curvalididx) > 0:
                    cur_preds_valid[j] = np.mean(all_preds[i][j,curvalididx])
                    cur_preds_valid_std[j] = np.std(all_preds[i][j,curvalididx])
                    all_preds_valid[i][j, curvalididx] = all_preds[i][j, curvalididx]

            preds_train.append(copy.deepcopy(cur_preds_train))
            preds_valid.append(copy.deepcopy(cur_preds_valid))
            preds_train_std.append(copy.deepcopy(cur_preds_train_std))
            preds_valid_std.append(copy.deepcopy(cur_preds_valid_std))

        return(preds_valid, preds_valid_std, all_preds_valid, preds_train, preds_train_std, all_preds_train)

    else:
        # Just average all predictions across all models- no need for separate books for training, validation sets
        preds_mean = []
        preds_std = []
        for j in range(models.n_tasks):
            cur_mean_preds = np.mean(all_preds[j], 1)
            cur_preds_std = np.std(all_preds[j], 1)
            preds_mean.append(cur_mean_preds)
            preds_std.append(cur_preds_std)


    return(preds_mean, preds_std, all_preds)



########################################################################################################################
# Generates "consensus" classification predictions given a predictive model (or a series of predictive models)
def models_predict_class(models, X, is_int_set=False):
    n_s = np.shape(X)[0]

    # Generate predictions for all samples in all classes in all tasks
    all_class_preds = []
    all_class_probs = []
    for t in range(models.n_tasks):
        all_class_preds.append(np.zeros((n_s,models.n_models)))
        all_class_probs.append(np.zeros((n_s, models.n_class[t], models.n_models)))

    # Perform the per-model, per-task predictions
    for i in range(models.n_models):
        cur_prob_pred, cur_class_pred = model_predict(models.all_models[i], X, models.pred_type, models.ml_type,
                                            models.hp, models.n_class, bin_class_thresh=models.preds.bin_class_thresh)
        for t in range(models.n_tasks):
            all_class_preds[t][:,i] = np.squeeze(cur_class_pred[t])
            all_class_probs[t][:,:,i] = np.squeeze(cur_prob_pred[t])

    # Generate consensus predictions from the ensemble of models
    if is_int_set:
        used_in_valid = np.zeros((n_s, models.n_models), dtype=np.bool)
        used_in_train = np.zeros((n_s, models.n_models), dtype=np.bool)
        for j in range(models.n_models):
            used_in_valid[models.valid_sets[j],j] = True
            used_in_train[models.train_sets[j],j] = True

        probs_train = []; probs_valid = []
        probs_std_train = []; probs_std_valid = []
        class_preds_train = []; class_preds_valid = []

        for t in range(models.n_tasks):
            cur_probs_train = np.zeros((n_s, models.n_class[t]))
            cur_probs_valid = np.zeros((n_s, models.n_class[t]))
            cur_probs_std_train = np.zeros((n_s, models.n_class[t]))
            cur_probs_std_valid = np.zeros((n_s, models.n_class[t]))

            for i in range(n_s):
                curvalididx = np.where(used_in_valid[i,:] == True)[0]
                curtrainidx = np.where(used_in_train[i,:] == True)[0]

                cur_probs_train[i, :] = np.mean(all_class_probs[t][i, :, curtrainidx], 0)
                cur_probs_train[i, :] /= np.sum(cur_probs_train[i,:])

                cur_probs_valid[i, :] = np.mean(all_class_probs[t][i, :, curvalididx], 0)
                cur_probs_valid[i, :] /= np.sum(cur_probs_valid[i,:])

                cur_probs_std_train[i,:] = np.std(all_class_probs[t][i, :, curtrainidx], 0)
                cur_probs_std_valid[i,:] = np.std(all_class_probs[t][i, :, curvalididx], 0)

            cur_class_pred_train = class_from_probs(cur_probs_train, bin_class_thresh=models.preds.bin_class_thresh[t])
            cur_class_pred_valid = class_from_probs(cur_probs_valid, bin_class_thresh=models.preds.bin_class_thresh[t])

            probs_train.append(copy.deepcopy(cur_probs_train))
            probs_valid.append(copy.deepcopy(cur_probs_valid))
            probs_std_train.append(copy.deepcopy(cur_probs_std_train))
            probs_std_valid.append(copy.deepcopy(cur_probs_std_valid))
            class_preds_train.append(copy.deepcopy(cur_class_pred_train))
            class_preds_valid.append(copy.deepcopy(cur_class_pred_valid))

        return(probs_valid, probs_std_valid, class_preds_valid, probs_train, probs_std_train, class_preds_train)

    else:
        probs = []
        class_preds = []
        probs_std = []
        for t in range(models.n_tasks):
            # med_class_probs = np.median(all_class_probs[t],2)
            mean_class_probs = np.mean(all_class_probs[t], 2)

            # Renormalize probabilities so they sum up to 1 across all classes
            sumprobs = np.sum(mean_class_probs,1)
            for i in range(n_s):
                mean_class_probs[i,:] /= sumprobs[i]
            probs_std.append(np.std(all_class_probs[t],2))

            cur_class_pred = class_from_probs(mean_class_probs, models.preds.bin_class_thresh[t])
            class_preds.append(copy.deepcopy(cur_class_pred))

            probs.append(mean_class_probs)

    return( probs, probs_std, class_preds )



########################################################################################################################
# Given a series of predictions from classification models, combines the predictions to generate ensemble predictions
def ensemble_predictions_class(all_class_preds, all_pred_probs, matchmat, ensemble_type='average'):

    n_mols = int(np.size(matchmat,0))
    n_models = int(np.size(matchmat,1))
    n_tasks = len(all_class_preds[0])

    alltasks_ens_pred_probs = []
    alltasks_ens_class_pred = []
    for t in range(n_tasks):
        n_class = int(np.size(all_pred_probs[0][t],1))
        ens_pred_probs = np.zeros((n_mols, n_class))
        ens_class_pred = np.zeros((n_mols,1), dtype=np.int32)

        for i in range(n_mols):
            curidx = np.where(matchmat[i,:] > -1)[0]
            n_cur = int(np.size(curidx))
            cur_pred_probs = np.zeros((n_cur, n_class))
            cur_class_preds = np.zeros(n_cur, dtype=np.int32)
            for j in range(n_cur):
                curmatchidx = matchmat[i, curidx[j]]
                cur_pred_probs[j,:] = np.squeeze(all_pred_probs[curidx[j]][t][curmatchidx,:])
                cur_class_preds[j] = all_class_preds[curidx[j]][t][curmatchidx,0]

            # Combine the predictions using the prescribed ensemble method
            # Calculate mean probabilities
            mean_probs = np.mean(cur_pred_probs, 0)
            mean_probs /= np.sum(mean_probs)
            max_mean_prob = np.max(mean_probs)
            maxmeanprobidx = np.where(mean_probs == max_mean_prob)[0]

            # Need to guard against NaN predictions in some models.
            if np.any(np.isnan(mean_probs)):
                ens_pred_probs[i, :] = np.nan
                ens_class_pred[i] = -9999999999 # can't do integer NaNs in numpy for some reason...
                print('NaN ensemble prediction for molecule %d, task %d' % (i, t))
                continue

            if ensemble_type == 'average':
                # Average the class probabilities and take ensemble prediction to be the class with the highest avg prob
                ens_class_pred[i] = int(np.min(maxmeanprobidx))
                ens_pred_probs[i, :] = np.squeeze(mean_probs)

            elif ensemble_type == 'voting':
                unique_class_preds, ucts = np.unique(cur_class_preds, return_counts=True)
                maxcts = np.unique(np.max(ucts))
                umaxidx = np.where(ucts == maxcts)[0]
                n_max_class = int(np.size(umaxidx))
                n_class_preds = int(np.size(unique_class_preds))

                if n_max_class == 1:
                    ens_class_pred[i] = unique_class_preds[umaxidx]

                elif n_max_class > 1:
                    # Tie-breaker: take the decision to be the class with the highest mean probability across all models
                    ens_class_pred[i] = int(np.min(maxmeanprobidx))

                maxidx = np.where(cur_class_preds == ens_class_pred[i])[0]
                ens_pred_probs[i,:] = np.squeeze(np.mean(cur_pred_probs[maxidx,:],0))

        alltasks_ens_pred_probs.append(copy.deepcopy(ens_pred_probs))
        alltasks_ens_class_pred.append(copy.deepcopy(ens_class_pred))

    return(alltasks_ens_class_pred, alltasks_ens_pred_probs)



########################################################################################################################
# Given a series of predictions from classification models, combines the predictions to generate ensemble predictions
def ensemble_predictions(all_preds, matchmat, ensemble_type='average'):

    n_s = matchmat.shape[0]
    n_models = matchmat.shape[1]
    n_tasks = len(all_preds[0])

    all_ens_preds = []
    for t in range(n_tasks):
        ens_preds = np.zeros(n_s) * np.nan

        for i in range(n_s):
            curidx = np.where(matchmat[i,:] > -1)[0]
            n_cur = int(np.size(curidx))
            cur_preds = np.zeros(n_cur)
            for j in range(n_cur):
                curmatchidx = matchmat[i, curidx[j]]
                cur_preds[j] = all_preds[curidx[j]][t][curmatchidx]

            # Generate ensemble predictions for current sample
            notnanidx = np.where(np.isnan(cur_preds) == False)[0]

            if np.size(notnanidx) == 0:
                continue

            if ensemble_type == 'average':
                ens_preds[i] = np.mean(cur_preds[notnanidx])

            elif ensemble_type == 'median':
                ens_preds[i] = np.median(cur_preds[notnanidx])

            #TODO: implement weighting based on confidence interval width

        all_ens_preds.append(copy.deepcopy(ens_preds))

    return(all_ens_preds)



########################################################################################################################
# Determine how complementary predictions are, given a list of predictions from separate models
def prediction_complementarity(preds, matchmat, probs=None):

    n_models = len(preds)

    #TODO: devise a way to perform ensemble predictions for models that overlapping tasks (but not necessarily identical ones)
    n_tasks = len(preds[0])

    is_classifier = False
    n_class = 0
    if probs is not None:
        is_classifier = True

    pc = []
    [pc.append(copy.deepcopy(PREDICTION_COMPLEMENTARITY() )) for t in range(n_tasks)]

    for t in range(n_tasks):
        if probs is not None:
            n_class = int(np.size(probs[0][t],1))

        corrcoef_probs = []
        pc[t].n_comps = int(n_models*(n_models-1)/2)
        corrcoef_preds = np.zeros(pc[t].n_comps)
        modelscompidx = np.zeros((pc[t].n_comps,2), dtype=np.int32) # indices of model predictions being compared
        compidx = 0
        print('Complementarity statistics for task # %d (of %d)' % (t+1,n_tasks))

        for i in range(n_models):
            idx_i = np.where(matchmat[:,i] > -1)[0]
            for j in range(i+1,n_models):
                idx_j = np.where(matchmat[:,j] > -1)[0]
                useidx = np.intersect1d(idx_i, idx_j)
                useidx_i = matchmat[useidx, i]
                useidx_j = matchmat[useidx, j]
                cc_preds = np.corrcoef(preds[i][t][useidx_i], preds[j][t][useidx_j] )

                corrcoef_preds[compidx] = cc_preds[0,1]
                modelscompidx[compidx, 0] = i
                modelscompidx[compidx, 1] = j
                compidx += 1

                print('\tModel %d vs. %d: prediction corr_coef = %.4f' % (i+1,j+1, cc_preds[0,1]))

                if probs is not None:

                    curcorrcoef_probs = np.zeros((n_class, n_class))
                    for m in range(n_class):
                        probs_i = probs[i][t][useidx_i, m]
                        for n in range(n_class):
                            probs_j = probs[j][t][useidx_j,n]
                            curcc = np.corrcoef(probs_i, probs_j )
                            curcorrcoef_probs[m,n] = copy.deepcopy(curcc[0,1])

                            print('\t\tClass %d vs. %d: probability corr_coef = %.4f' % (m,n, curcorrcoef_probs[m,n]))
                    corrcoef_probs.append(copy.deepcopy(curcorrcoef_probs))
        pc[t].corrcoef_preds = copy.deepcopy(corrcoef_preds)
        pc[t].modelscompidx = copy.deepcopy(modelscompidx)
        pc[t].corrcoef_probs = copy.deepcopy(corrcoef_probs)
    return(pc)


########################################################################################################################
# Trains a single model (no ensembling / bagging etc.)
def train_a_model(hp, pred_type, ml_type, X_train, Y_train, weights_train = None, task_names=None):

    n_tasks = int(np.size(Y_train,1))

    if pred_type == 'regression':
        # Train a regression model
        try:
            if ml_type == 'pls':
                model = pls.pls_train(pls.PLS_MODEL(), hp, X_train, Y_train)
            elif ml_type == 'ann':
                model = ann.ann_train_regr(hp, X_train, Y_train, weights=weights_train, task_names=task_names)
                # model.graph = tf.get_default_graph()
                # model.graph.finalize()

            elif ml_type == 'knn':
                model = knn.knn_train(knn.KNN_MODEL(), hp, X_train, Y_train)

            elif ml_type == 'xgboost':
                model = []
                for t in range(n_tasks):
                    cur_model = XGBRegressor(max_depth=hp.max_depth, learning_rate=hp.learning_rate,
                                         n_estimators=hp.n_estimators)
                    notnanidx = np.where(np.isnan(Y_train[:,t]) == False)[0]
                    if weights_train is not None:
                        cur_model.fit(X_train[notnanidx,:], np.squeeze(Y_train[notnanidx, t]), sample_weight=weights_train[notnanidx,t])
                    else:
                        cur_model.fit(X_train[notnanidx,:], np.squeeze(Y_train[notnanidx, t]) )
                    model.append(copy.deepcopy(cur_model))

        except:
            exit('Training diverged!!!')



    elif pred_type == 'classification':
        # Train a classification model
        try:
            if ml_type == 'pls':
                model = pls.pls_train(pls.PLS_MODEL(), hp, X_train, Y_train)
            elif ml_type == 'ann':
                model = ann.ann_train_class(hp, X_train, Y_train, weights=weights_train, task_names=task_names)

            elif ml_type == 'knn':
                model = knn.knn_train(knn.KNN_MODEL(), hp, X_train, Y_train)

            elif ml_type == 'xgboost':
                model = []
                for t in range(n_tasks):
                    cur_model = XGBClassifier(max_depth=hp.max_depth, learning_rate=hp.learning_rate,
                                              n_estimators=hp.n_estimators, objective=hp.objective)
                    notnanidx = np.where(np.isnan(Y_train[:,t]) == False)[0]
                    if weights_train is not None:
                        cur_model.fit(X_train[notnanidx,:], np.squeeze(Y_train[notnanidx, t]), sample_weight=weights_train[notnanidx,t])
                    else:
                        cur_model.fit(X_train[notnanidx,:], np.squeeze(Y_train[notnanidx, t]) )
                    model.append(copy.deepcopy(cur_model))
        except:
            import traceback
            tb = traceback.format_exc()
            exit('Training diverged!!!')

    return(model)


########################################################################################################################
# Applies a model to generate predictions for a given set of input features "X"
def model_predict(model, X, pred_type, ml_type, hp, n_class = 2, bin_class_thresh = 0.5):

    if pred_type == 'regression':
        if ml_type == 'ann':
            Y_pred = model.predict(X, batch_size=hp.batch_size)

            if not isinstance(Y_pred, list):
                # Single task models must have consistent format- store as a per-task list
                Y_pred = [Y_pred]

        elif ml_type == 'pls':
            Y_pred = pls.apply_pls_regr(model, X)

        elif ml_type == 'knn':
            Y_pred = knn.apply_knn_regr(model, X)

        elif ml_type == 'xgboost':
            Y_pred = []
            for t in range(len(model)):
                Y_pred.append(copy.deepcopy(model[t].predict(X)))

        else:
            exit('Unknown machine learning type (%s)' % (ml_type))

        return(Y_pred)


    elif pred_type == 'classification':

        if ml_type == 'ann':
            # prob_pred, class_pred = apply_ann_class(model, X, binary_thresh=bin_class_thresh)
            prob_pred = model.predict(X, batch_size = hp.batch_size)

            if not isinstance(prob_pred, list):
                # Single task models must have consistent format- store as a per-task list
                prob_pred = [prob_pred]

            class_pred = []
            for t in range(len(prob_pred)):
                cur_class_pred = class_from_probs(prob_pred[t], bin_class_thresh=bin_class_thresh[t])
                class_pred.append(copy.deepcopy(cur_class_pred))

        elif ml_type == 'pls':
            prob_pred, class_pred = pls.apply_pls_class(model, X, n_class, binary_thresh=bin_class_thresh)

        elif ml_type == 'knn':
            prob_pred, class_pred = knn.apply_knn_class(model, X, binary_thresh=bin_class_thresh)

        elif ml_type == 'xgboost':
            prob_pred = []
            class_pred = []
            for t in range(len(model)):
                cur_prob_pred = model[t].predict_proba(X)
                cur_class_pred = class_from_probs(cur_prob_pred, bin_class_thresh=bin_class_thresh[t])
                prob_pred.append(copy.deepcopy(cur_prob_pred))
                class_pred.append(copy.deepcopy(cur_class_pred))

        else:
            exit('Unknown machine learning type (%s)' % (ml_type))

        return(prob_pred, class_pred)

    return()

########################################################################################################################
# Returns the unique classes and counts of # unique classes per task (different tasks in different columns)
def unique_classes(Y, exclude_missing = True):
    n_tasks = int(np.size(Y,1))
    uclasses = []
    n_class = np.zeros(n_tasks, dtype=np.int32)
    for t in range(n_tasks):
        if exclude_missing is True:
            notnanidx = np.where(np.isnan(Y[:,t]) == False)[0]
            Y_curtask = Y[notnanidx,t]

        uclasses.append(np.unique(Y_curtask))
        n_class[t] = int(np.size(uclasses[-1]))

    return(uclasses, n_class)



########################################################################################################################
# Identifies the features in feature matrix X_feats with statistically significant correlations to endpoint values "Y".
#   Y can be passed in as an argument, or it can be generated from model predictions
def feature_significance_univariate(models, X_feats, n_sig = 150, pvalue_thresh = 0.05):

    sigfeatsidx_tasks = []
    correls_tasks = []
    pvalues_tasks = []
    fdr_tasks = []

    for i in range(models.n_tasks):
        # Determine the most significant bins via most frequent bin occurrences in the training data.  i.e. "rankScore" method
        pvalues = np.zeros((models.n_models,models.n_feats))
        correls = np.zeros((models.n_models,models.n_feats))

        for j in range(0, models.n_models):

            if models.pred_type == 'regression':
                Ypred_tasks = model_predict(models.all_models[j], X_feats, models.pred_type, models.ml_type, models.hp)
                Ypred_curtask = copy.deepcopy(Ypred_tasks[i])

            elif models.pred_type == 'classification':
                prob_pred_all, Ypred_tasks = model_predict(models.all_models[j], X_feats, models.pred_type, models.ml_type,
                                        models.hp, models.n_class, bin_class_thresh=models.preds.bin_class_thresh)
                Ypred_curtask = copy.deepcopy(Ypred_tasks[i])

            for k in range(0, models.n_feats):
                # tstats[j,k], pvalues_ttest[j,k] = scipy.stats.ttest_ind(X_feats[:,k],np.squeeze(Ypred_curtask),equal_var=False)
                #
                # ustats[j,k], pvalues_utest[j,k] = scipy.stats.mannwhitneyu(X_feats[:,k], np.squeeze(Ypred_curtask), alternative=None)
                #
                # r_pearson, pvalues_pearson = scipy.stats.pearsonr(X_feats[:,k], np.squeeze(Ypred_curtask))

                # Calculate the correlation between current feature and Y and p-value.  Use a non-parametric test
                #   as the data is not normally distributed at all
                if np.std(X_feats[:,k]) == 0.0:
                    continue

                correls[j, k], pvalues[j,k] = scipy.stats.spearmanr(X_feats[:,k], np.squeeze(Ypred_curtask))

                # if np.isnan(correls[j,k]):
                #     correls[j,k] = 0.0
                #     pvalues[j,k] = 0.0

                # Calculate fdr (false discovery rate)
                # fdr[j,k] =

                # curcorr = np.corrcoef(np.squeeze(Ypred_curtask), X_feats[:,k])
                # if np.any(np.isnan(curcorr)):
                #     correls[j,k] = 0.0
                # else:
                #     correls[j,k] = curcorr[0,1]


            # elif model.pred_type == 'classification' and model.Nclass == 2:
            #     Npos = np.sum(Ycur)
            #     for j in range(0, model.Npred):
            #         # Npos_x = np.sum(X_feats[:, j])
            #         # pr_pos = float(Npos_x)/float(Ns)
            #         # pvalues[i,j] = scipy.stats.binom_test(Npos, Ns, p=pr_pos, alternative='two-sided')
            #         correls[i,j] = ml.binomial_corr_coef(X_feats[:,j], Ycur)

        # Identify the "Nsig" most significant bins across all models- those with the highest probability of having a
        #   correlation between predictor and the resultant model output
        # pvalue_med = np.median(pvalues,0)
        pvalues_mean = np.mean(pvalues, 0)
        # pvalue_std = np.std(pvalues,0)
        # sortidx = np.argsort(pvalue_med)

        # keepidx = np.where(pvalues_mean < pvalue_thresh)[0]
        pvalue_excludeidx = np.where(pvalues_mean > pvalue_thresh)[0]


        #TODO: figure out a better sorting method for identifying most significant features
        corr_mean = np.mean(correls,0)
        # corr_std = np.std(correls,0)
        corr_mean[pvalue_excludeidx] = -np.inf
        sortidx = np.argsort(corr_mean)[::-1]
        # sortidx = np.setdiff1d(sortidx, excludeidx)

        if n_sig is None:
            n_sig = models.n_feats
        else:
            n_sig = min(n_sig,models.n_feats)

        if n_sig > np.size(sortidx):
            n_sig = np.size(sortidx)

        sigfeatsidx = sortidx[0:n_sig]

        sigfeatsidx_tasks.append(sigfeatsidx)
        correls_tasks.append(corr_mean[sigfeatsidx])
        pvalues_tasks.append(pvalues_mean[sigfeatsidx])
        # fdr_tasks.append(fdr_mean[sigfeatsidx])

    return(sigfeatsidx_tasks, correls_tasks, pvalues_tasks)


########################################################################################################################
# Calculate FDR (False Discovery Rate), where a "false discovery" occurs when a feature is "positive" and an activity
#   is "negative" (defined as "not positive")
def false_discovery_rate(X, Y):
    n_s = X.shape[0]
    n_feats = X.shape[1]
    n_tasks = Y.shape[1]
    fdr_tasks = np.zeros((n_tasks, n_feats))

    for i in range(n_tasks):
        negactsidx = np.where(Y[:,i] <= 0)[0]
        for j in range(n_feats):
            posfeatsidx = np.where(X[:,j] > 0)[0]

            falseposidx = np.intersect1d(negactsidx, posfeatsidx)
            n_falsepos = float(np.size(falseposidx))
            fdr_tasks[i,j] = n_falsepos / n_s

    return(fdr_tasks)



########################################################################################################################
########################################################################################################################
########################################################################################################################

class PREDICTION_COMPLEMENTARITY():
    def __init__(self):
        self.corrcoef_preds = None
        self.modelscompidx  =None
        self.corrcoef_probs = None
        self.n_comps = None # number of comparisons between models



class APPLICABILITY_DOMAIN():
    def __init__(self):
        self.binoccs_overlap_frac = None
        self.binoccs_overlap = None
        self.tanimoto_coef = None
        self.hamming_coef = None
        self.refmodelvec = None


########################################################################################################################
# Class for rescaling data.  A class might be overkill, but you need a good way of maintaining what scaling method was
#   used, what parameters were used to scale it, and the correct "unscaling" method.  You also need to know if scaling
#   has been set for a given regression model
class SCALE_PARAMS():
    def __init__(self):
        self.method = 'none'
        self.scaling_set = False # indicator variable of whether or not scaling has been set for the SCALE_PARAMS in question
        self.minvals = None
        self.maxvals = None
        self.medvals = None
        self.meanvals = None
        self.stdvals = None
        self.vals25pct = None
        self.vals75pct = None


########################################################################################################################
#  Class for defining XGBoost specific hyperparameters
class XGBOOST_HP():
    def __init__(self, pred_type='classification'):
        self.pred_type = pred_type # 'regression' or 'classification'
        self.max_depth = 4
        self.learning_rate = 0.1
        self.n_estimators = 100
        self.objective = 'binary:logistic'


########################################################################################################################
# Base class for storing modeling statistics.  Depends on the modeling type performed
# Note that statistics are calculated on a per-task basis
class PERF_STATS():
    def __init__(self, pred_type = 'classification', siglevel = 0.95, n_models=100, n_s = 1):
        self.pred_type = copy.deepcopy(pred_type)
        if pred_type == 'classification':
            # The specific size of stats matrices and even stats computed will depend on the number of classes
            #   For example, some statistics (sensitivity, specificity, etc.) are only calculated for binary classifiers
            self.Pmat = None
            self.CM = None
            self.recall = None
            self.precision = None
            self.ns_class = None  # num samples per class
            self.mocp = None  # margin of correct prediction, in terms of # std devs of consensus prediction
            self.accuracy = None
            self.balanced_accuracy = None
            self.sensitivity = None
            self.specificity = None
            self.PPV = None
            self.NPV = None
            self.auc = None  # area under ROC curve
        elif pred_type == 'regression':
            self.rsq = None # Rsq of each prediction for each task
            self.e_rms = None  # RMS prediction error
            self.e_maxabs = None  # maximum absolute prediction error

            # Ensemble statistics- calculated across all models
            self.rsq_model = np.zeros(n_models) * np.nan
            self.e_rms_model = np.zeros(n_models) * np.nan
            self.e_maxabs_model = np.zeros(n_models) * np.nan
            self.stddev_rsq = None
            self.confint_rsq = np.zeros((1,2)) * np.nan # confidence interval- lower and upper bounds
            self.siglevel = siglevel # significance level of confidence intervals
            self.confint_per_pred = np.zeros((n_s,2)) * np.nan # lower, upper prediction confidence interval bounds per sample

