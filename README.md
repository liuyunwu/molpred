# **MolPred**

## **Overview**

MolPred is a Python platform for applying supervised machine learning to SAR (Structure - Activity Relationship) modeling
for drug discovery.  It was designed for small molecule drug discovery / lead candidate selection, but it could be adapted for use 
in a variety of other applications.  MolPred was originally developed for scenarios where data is not very plentiful, 
though it can be used for scenarios where data is available in large quantities.  In an earlier incarnation, MolPred was
 used in a virtual screening process to select a shortlist of molecules to synthesize.  It successfully predict the 
 activities of ~2/3 of those shortlisted novel molecules that were synthesized and tested.  

MolPred has a number of unique features that help to validate or reject
 models.  One of the core differences with MolPred vs. many other modeling platforms: MolPred 
 is not intended to just get a high AUROC score or generate cool looking pictures 
 (though it can do both of those).   It is a tool that can be used by skilled pharmaceutical professionals
 to gauge risk and uncertainty.  Contrary to some other platforms which aim to replace
 medicinal chemists and pharmacologists, MolPred aims to *augment* the abilities of these skilled
 professionals.  
 

The main goals of MolPred are:

(1) To provide a robust modeling platform that facilitates risk-balanced decisions about 
molecular synthesis and selection candidates.  MolPred doesn't just allow you to 
train prediction mechanisms- it allows you to more readily gauge the reliability and consistency of
those prediction mechanisms.  The goal of MolPred isn't necessarily to train the latest and greatest modeling 
architecture that tops a leaderboard on popular data sets (though it can readily be modified to accommodate
such new model types).  

(2) To provide an easy to use modeling platform for highly skilled personnel who aren't software engineers
(chemists, biologists, pharmacologists), so that they can use their expertise to more readily experiment with 
molecule designs.    

If you run a training job, MolPred automatically saves the trained model.  It also saves an archive of all data used to 
train the model (for results reproducibility), performance statistics, and prediction reports.  

When you run a MolPred job, it automatically generates reports as shown below.  You get a prediction for every molecule, 
along with the composite performance statistics (if you provide class labels / regression activities).  
![Alt text](ReportScreenshot.png?raw=true "Report Screenshot")


If you train a regression model, MolPred generates easy to interpret plots like the following:
![Alt text](R2_plot.png?raw=true "Title")

## **Dependencies**
MolPred has been verified to function as intended with the following library versions:
* joblib >= 0.14.1
* matplotlib >= 3.1.3
* numpy >= 1.18.1
* scikit_learn >= 0.22.1
* scipy >= 1.4.1
* tensorflow == 2.0.0
* rdkit >= 2019.03.4
* xgboost >= 0.90

To run containerized MolPred jobs on the AWS cloud (optional), the following packages are required:
* boto3 >= 1.11.16
* yagmail >= 0.11.224


## **Some Unique Features of MolPred**
Why would you use MolPred vs. just training a model with TensorFlow, PyTorch, Scikit-Learn, etc.? 
MolPred builds off of such powerful frameworks to form a more comprehensive modeling
platform.  
### A Fundamentally Different Philosophy
MolPred is not just about training a model- it's geared towards validating models, and then using them to readily 
generate ensemble predictions for new molecules where you don't know class / activity information.  
### Ease of Use
You don't need to be a software engineer to use MolPred- you just need to be able to run a few basic command terminal 
commands and edit text files (which control the job settings).    

### Model Stress Testing
Biological / pharmaceutical modeling can be quite challenging.  Data is highly uncertain, 
it's often difficult to acquire, the most useful training sets are often small, and
the dimensionality of the design space is typically quite high.  For this reason, it is easy to fool yourself when training and 
validating predictive models.  To help reduce wasting time and money on synthesizing molecules, 
you need to thoroughly validate your predictive models.  

Most published research on SAR modeling shows some k-fold cross validation and related ablation studies.  In my view, these things
 are a good start but they are nowhere near rigorous enough for the drug discovery problem space.  The Model Stress Testing 
 module puts a trained model through a series of rigorous stress tests.  In the stress tests, model performance is tabulated 
 across a wide range of machine learning hyperparameters, molecular featurization settings, test sets used, and for varying 
 noise amplitudes injected into your data.  If your model shows wide variability for small steps in parameter space, this 
 probably means that your model has been overfit and will probably not generalize to new molecules outside of the training set.   
 
 If your model performs as desired during the stress tests, it is much more likely that your machine learning model has 
 "learned" a valid Structure-Activity Relationship that you're modeling.  It doesn't mean that your model is "right" or 
 that it will work for all molecules you test.  It just means that your model has validity in some region of the molecular
  design space.  

### Multi-Modal Machine Learning
MolPred allows you to seamlessly use many different machine learning methodologies.  This includes "modern" methods like 
deep neural networks and "classical" methods like boosting, kNN, and PLS.  Modern ML methods like deep learning are quite 
powerful.  However, they are often not suitable for many scenarios of interest in drug discovery, due to 
their need for large training sets.  For this reason, it can be beneficial to combine modern and classical approaches to generate 
multi-modal ensemble predictions.  

### Ensemble Modeling
MolPred allows you to seamlessly combine predictions from different models- models which
might be trained using completely different methodologies and training sets. In this way,
the biases and shortcomings of different individual methods can be better appreciated and possibly averaged out.   

### Multitask Learning
Often in molecular modeling it is the case that you want to predict multiple properties
of molecules.  This is where multitask learning can play a significant role, in both 
reducing overfitting and in "sharing" features between different tasks.  For some tasks it better to train models in a 
multitask configuration; some tasks are better modeled individually.  This is very much data dependent, and there is no
 one-size-its-all approach.  MolPred allows you to easily test different configurations.  

## **How to Use MolPred**
How do you use MolPred- how do you train, validate, and then use a model?  It depends on what type of job you're performing 
(training, stress testing, or inference).  

### Precursor: Data Set Preparation
MolPred is a Structure Activity Relationship modeling platform which models the relationship between molecules and their
 properties.  Thus, to use it one first needs a collection of molecules and their associated properties for the task
 being modeled.  This basic step- finding molecules and their associated properties of interest- is the key step 
 in modeling molecular properties.  For pharmaceutical modeling, acquiring such data can be difficult.  Some test data has
 AUROCs well below 1.0.  Models built on such data should not be trusted if their AUROC is greater than their associated
  experimental data AUROC.  

Molecule names and their associated biological effects should be written to a .csv "activity" file, formatted like so:
```
Molecule Name,Effect 1 Name,Effect 2 Name, ..., Effect N Name
molecule 1,label 1,label 2, ..., label N
molecule 2,label 1,label 2, ..., label N
...
molecule M,label 1,label2, ..., label N
```
The first row is a header row.  The first column must be the molecule name, and each subsequent column is the value of that 
effect for that molecule.  You don't need to know all effects for all molecules- there can be some missing values.  If a
 molecule name in the activity file is missing values for all effects, that molecule will be excluded from the modeling.  

MolPred assumes that input molecules are encoded in .mol file format.  Put all of your .mol files in the same directory.  


#### Classification or Regression Model?
MolPred supports classification and regression modeling.  To toggle between these two prediction types, change the **pred_type** 
field to *regression* or *classification* in the *Training_Control.xml* or the *StressTest_Control.xml* config files.  The type 
of model you train will largely be dictated by the task you are modeling and the training data you are able to find.  Sometimes 
it is beneficial to convert floating point regression data into a multi-class classification task, if the errors are in the 
training data are sufficiently large and uncertain.  This is a determination that the modeler must make, probably after some iteration.  

### Training a Model
Once we have assembled a data set of "predictors" (molecules in .mol file format) and their associated "effects" (the activities
associated with each molecule), we can then train a predictive model.  Training is controlled in the Training_Control.xml file.  
In this file, assign one of the supported settings (machine learning methodology, featurization type, 
hyperparameters, training / validation / test split, etc.).  Options and descriptions are contained in the Training_Control.xml file. 
Once you have settled on your settings (and organized your input data appropriately), you can run the traiing job from the 
command line by typing:

```
python  RunMolPredJob.py  --config_file  ./config/Training_Control.xml
```
The *Training_Control.xml* file can have any filename you'd like, it just has to have a job_type="training" attribute 
tag in the XML header to be recognized as a training job.  

#### Model Training Output
After a model is trained, a number of outputs are generated.  The first output is a saved model (stored in a .pkl file). 
 The other type outputs depends on the prediction type (classification or regression).  All jobs result in the generation 
 of reports that the modeler can review to make decisions.  If you achieve acceptable performance statistics, you should 
 then put your model through a Model Stress Test job.  

####  **What if My Model Doesn't Work Well?**
For modeling systems as complicated as biological ones, it is often the case that models don't have good predictive skill. 
This is where modeling becomes challenging, and there are no surefire fixes.  In my experience, modeling is best approached 
as an iterative process.  It is quite rare on the first pass to compile a data set, train a model, and have it robustly 
perform as expected.  

If your results are poor, it may be the case that there is no correlation between the molecules and the task(s) being modeled. 
In this scenario, training a model with predictive skill is a hopeless endeavor.  Before giving up on modeling your task, 
however, there are several basic steps to perform to see if a model with predictive skill can be trained 
for the given data.  What steps can you to to get a better model?  Here are some practical suggestions I have found when performing pharmaceutical 
modeling.  It is not exhaustive by any means.  

(1) Increase data quantity.  With more data, there are more opportunities to identify patterns in the molecular input data. The 
amount of data you need depends on the machine learning technique that you're using.  Data hungry techniques like deep learning 
face a significant disadvantage in drug discovery, in that relevant data is often not easy to collect.  Deep learning gets 
all the hype these days, but its need for large quantities of data means that we shouldn't set aside classical techniques 
that are more forgiving.  

(2) Increase data quality.  Not all data is equal.  Sometimes mixing data from disparate sets (in an effort to boost quantity) 
degrades the net quality such that a skilled model cannot be trained.  Double check the activity values for each effect 
associated with each molecule.  You may be surprised with how frequently effect values conflict for pharmaceutical data.  

(3) Increase model complexity.  If you have a sufficiently large training set, it may be beneficial to increase the amount of
variability that your model can describe.  This includes things like increase the number of layers and the number of neurons
 per layer (if using a deep neural network), the number of latent variables if using PLS, etc.  Increasing model complexity 
 can also be accomplished by switching to a machine learning technique that allows for greater variabiity to be modeled (such as deep learning
 methods).  
 
(4) Decrease model complexity.  If your data set is small, increasing model complexity might result in a highly underdetermined 
configuration.  It may be the case that a simpler model with less ability to describe variance might have better overall 
predictive skill.  This can also be accomplished by switching to a less complicated machine learning technique (like PLS or XGBoost).

(5) Scale regression data.  This is a pretty standard way to prevent outliers from receiving undue importance in the regression 
modeling process.  MolPred allows for several data scaling algorithms to be applied (see Training_Control.xml for acceptable values).  

(6) Convert regression data to multi-class classification labels.  It may be the case that data (especially regression data) 
is so uncertain that models with good performance statistic cannot be trained.  In such cases, it may be beneficial to "bin" 
regression values into broad classes.  For example, convert receptor binding affinity Ki values into "low", "medium", and "high" 
binding affinity classes.  

(7) Reduce the number of classes.  It may be the case that in a classification model, closely related classes get confused. 
In such a scenario, it may be beneficial to reduce the number of classes (sacrificing resolution) in favor of enhanced predictive
 skill.  
 
(8) Employ class balancing and sample weighting.  Often in pharmaceutical modeling, there are many more examples of molecules from one class 
than others.  For example, if modeling a toxicity it will often be the case that the number of benign molecules greatly 
exceeds the number of toxic molecules for a given effect.  This numerical imbalance can wreak havoc on modeling efforts. 
For this reason, MolPred allows for various balancing techniques to be employed (downsampling, upsampling), and various 
sample weighting techniques to be employed (higher penalties for misclassifying examples in numerically smaller classes). 

The key factor for drug discovery modeling (as in all machine learning) is your data.  Groundbreaking new algorithms and 
methodologies do not sidestep the new for reliable training data in significant quantities.  

### Stress Testing the Model (Model Validation)
So we've trained a model, and we like the initial results we've obtained.  Next, we want to validate this model through 
a series of "stress tests" that help us see how robust the model predictions are. These stress tests are optional- you can 
start using a model as soon as it is trained.  But you probably want to subject the model to more rigorous scrutiny before you 
trust it enough to use as a criteria for lead selection and virtual screening.  The basic philosophy of the model stress 
test job type is described above.  Basically, if you see large changes in model performance for small changes in model settings, 
that's usually a bad sign for the robustness of your model.  

To configure a stress testing job, modify the entries in the StressTest_Control.xml file.  The basic idea is the same as
 for training a model, except that you also define the deviations ("stresses") to subject your model to.  These are the
 MST_SETTINGS fields in the .xml file.  You can include as many permutations as you want for each test, separating each
 value by a comma.  More detailed instructions are in the .xml file.  To launch a stress test job, run the following from
 the command terminal:
```
python  RunMolPredJob.py  --config_file  ./config/StressTest_Control.xml
```
The *StressTest_Control.xml* file can have any filename you'd like, it just has to have a job_type="model_stress_test" attribute 
tag in the XML header to be recognized as a stress testing job.  **NOTE**: a model stress testing job can have a significant 
run-time duration, so you might want to not run too many stress tests.  This is something for the user to decide.  
 
#### Model Stress Testing Output
Many files are generated upon completion of the model stress test process.  There is a stress test summary report, which
 reports on the summary statistics for each individual stress test.  This allows you to see at a glance the robustness of
  your model performance.  The files and information needed to train a model for a particular stress test scenario are saved
   in separate folders within your results root directory.  This can be quite useful when a particular stress test scenario
   shows anomalous behavior and you want to do a root cause analysis of this anomaly.   

### Using the Model (Inference)
So we've trained a model and validated to our level of satisfaction that it has actually learned something valid, and not
 just fit noise.  Now, we can deploy the model to generate predictions on new molecules.  This is where we actually use 
 the model for virtual screening.  Configuring an inference job is much easier than configuring a training or a stress test 
 job.  In the Inference_Control.xml file, add the full filename of the models you want to use to generate predictions.  You
 can include as many models as you want to take advantage of model ensembling.  More detailed instructions are in the Inference_Control.xml 
 file. To run an inference job, run the following from the command terminal:
```
python  RunMolPredJob.py  --config_file  ./config/Inference_Control.xml
```
The *Inference_Control.xml* file can have any filename you'd like, it just has to have a job_type="inference" attribute 
tag in the XML header to be recognized as an inference job. 

#### Model Inference Output
After running an inference job on a previously trained model, you get a prediction report.  The prediction report lists the predictions 
for each model on each molecule, along with an ensemble prediction for all models (if using > 1 model).  The prediction report 
also contains a statistic called "prediction complementarity", which tells you how correlated the predictions are for each 
model type (if using > 1 model); these statistics are tabulated on a per-class basis if performing classification predictions. 
Prediction complementarity  tells you how similar the model predictions are, which can suggest ways to better ensemble models 
and (hopefully!) reduce prediction bias errors.  If the molecules_file has values for the effects being modeled, the prediction report also contains performance statistics.  

## **Supported Computing Environments**
MolPred can run in three different computing environments.
### Natively, on the Modeler's Computer
The user must install all dependencies and run MolPred from the command line.  

Where *config_filename* is the name of the .xml configuration file used to establishing the type of job, and how to 
run the job.  

### In a Docker Container Running on the Modeler's Computer
The MolPred repo contains a Dockerfile to facilitate running MolPred in a container.  After building the Docker image, MolPred
jobs can be run by specifying the name of the image in the *_Control.xml files.  Any job that runs natively can be run
 in the Docker container.   

### In a Cloud Based Docker Container
MolPred facilitates running jobs in the cloud (AWS in particular, though it could be adapted to other cloud services).  
The ingredients for a job are "packaged" and pushed to a containerized cloud computing environment.  When the job is 
completed, the user is emailed a notification of job completion along with an email link to download the model / prediction 
results. **Note that it is up to the user to establish the connections between the uploaded job payload and the cloud 
computing resources**.    

## Authors

* **Bob D'Agostino** - *Initial work and software architecture* - [bdagostino](https://gitlab.com/bdagostino)

## License
MolPred is licensed under the BSD License.  

## **Future Plans**
I have various extensions planned for Molpred.  There is no timeline for including any of these extensions.  
* Incorporation of newer deep learning based methods like MPNNs (Message Passing Neural Networks).  Some of these recent
 innovations are showing improved predictive skill on some popular benchmark data sets, so they probably  have a place 
 in the drug discovery toolkit 
* Incorporate hyperparameter optimization libraries
* Improved methods for determining the significance of molecular features
* Ways of determining model applicability domain, along with a prediction reliability score.  There are various ways of 
doing this, but I have not taken the time to judge which ones work best for drug discovery purposes
* Switching the deep learning framework from TensorFlow / Keras to PyTorch.  On other projects, PyTorch seems to throw 
 fewer weird runtime errors than TensorFlow, especially running in Docker containers.  At some convenient point I will make that switch.  
* Including additional molecular featurization types, other than ECFP / FCFP
* Using trained models to automatically design molecules.  This is a longer term project, but I am convinced that it will become
 a valuable complement of the drug discovery process 