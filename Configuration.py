# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

########################################################################################################################
# Utilities for input / output control

import os, glob, copy, datetime
import xml.etree.ElementTree as et
import shutil
import numpy as np
import MolPred as mp
import MachineLearningUtils as ml
import PLS as pls
import KNN as knn
import ANN as ann
import Features as features


########################################################################################################################
def parse_model_inference_file(config_file):
    infer_settings = INFERENCE_SETTINGS()

    print('Parsing model inference settings out of config file: %s' % (config_file))
    tree = et.parse(config_file)
    root = tree.getroot()

    models_root = root.find('MODEL_FILES')
    model_files = []
    for child in models_root:
        if child.text is not None:
            model_files.append(child.text)
    infer_settings.model_files = copy.deepcopy(model_files)

    child = root.find('ensemble_type')
    infer_settings.ensemble_type = copy.deepcopy(child.text)
    if child.text.lower() == 'none':
        infer_settings.ensemble_type = None

    child = root.find('report_name')
    infer_settings.report_name = copy.deepcopy(child.text)

    child = root.find('mol_root')
    infer_settings.mol_root = copy.deepcopy(child.text)

    child = root.find('save_root')
    infer_settings.save_root = copy.deepcopy(child.text)

    child = root.find('molecules_file')
    infer_settings.molecules_file = copy.deepcopy(child.text)

    child = root.find('excludes_file')
    infer_settings.excludes_file = copy.deepcopy(child.text)

    child = root.find('siglevel')
    infer_settings.siglevel = float(child.text)

    return(infer_settings)


########################################################################################################################
def parse_stress_test_settings_file(config_file):

    # Extract the base case settings
    settings = parse_model_training_file(config_file)
    models = mp.MODELS(settings)

    # Extract the stress test settings
    stset = mp.MODEL_STRESS_TEST_SETTINGS(models)

    tree = et.parse(config_file)
    root = tree.getroot()

    io_root = root.find('IO_PATHS')
    stset.test_set_files = None

    stset.activity_files = None

    mst_root = root.find('MST_SETTINGS')
    child = mst_root.find('n_test_sets')
    stset.n_test_sets = int(child.text)

    temp = mst_root.find('Xnoise_fracs').text
    tokens = []
    if temp is not None:
        tokens = temp.split(',')
    stset.Xnoise_fracs = list(map(float, tokens))

    temp = mst_root.find('Ynoise_fracs').text
    tokens = []
    if temp is not None:
        tokens = temp.split(',')
    stset.Ynoise_fracs = list(map(float, tokens))

    #######################################################
    # Parse out molecular featurization params to test
    if models.feats_type == 'ecfp':

        child = mst_root.find('MST_ECFP')

        temp = child.find('fp_radius').text
        tokens = temp.split(',')
        stset.fp_radius = list(map(int, tokens))

        temp = child.find('n_bits').text
        tokens = temp.split(',')
        stset.n_bits = list(map(int, tokens))

        temp = child.find('use_fcfp').text
        tokens = temp.split(',')
        temp2 = []
        [temp2.append(str_to_bool(tokens[i])) for i in range(len(tokens))]
        stset.use_fcfp = copy.deepcopy(temp2)

    #######################################################
    # Parse out ML hyperparameters to stress test
    if models.ml_type == 'pls':

        child = mst_root.find('MST_PLS')

        temp = child.find('n_comp').text
        tokens = temp.split(',')
        stset.n_comp = list(map(int, tokens))

        temp = child.find('n_iters').text
        tokens = temp.split(',')
        stset.n_iters = list(map(int, tokens))

    elif models.ml_type == 'ann':

        child = mst_root.find('MST_ANN')

        temp = child.find('n_neur').text
        tokens = temp.split(',')
        stset.n_neur = list(map(int, tokens))

        temp = child.find('n_hidden').text
        tokens = temp.split(',')
        stset.n_hidden = list(map(int, tokens))

        temp = child.find('n_epochs').text
        tokens = temp.split(',')
        stset.n_epochs = list(map(int, tokens))

        temp = child.find('act_fn').text
        stset.act_fn = temp.split(',')

        temp = child.find('learning_rate').text
        tokens = temp.split(',')
        stset.learning_rate = list(map(float, tokens))

        temp = child.find('learning_rule').text
        stset.learning_rule = temp.split(',')

        temp = child.find('learning_momentum').text
        tokens = temp.split(',')
        stset.learning_momentum = list(map(float, tokens))

        temp = child.find('n_iters').text
        tokens = temp.split(',')
        stset.n_iters = list(map(int, tokens))

        temp = child.find('dropout').text
        tokens = temp.split(',')
        stset.dropout = list(map(float, tokens))

        temp = child.find('batch_size').text
        tokens = temp.split(',')
        stset.batch_size = list(map(int, tokens))

    elif models.ml_type == 'knn':

        child = mst_root.find('MST_KNN')

        temp = child.find('k').text
        tokens = temp.split(',')
        stset.k = list(map(int, tokens))

        temp = child.find('distance_type').text
        stset.distance_type = temp.split(',')

        temp = child.find('weight_type').text
        stset.weight_type = temp.split(',')

    elif models.ml_type == 'xgboost':

        child = mst_root.find('MST_XGBOOST')

        temp = child.find('max_depth').text
        tokens = temp.split(',')
        stset.max_depth = list(map(int, tokens))

        temp = child.find('learning_rate').text
        tokens = temp.split(',')
        stset.learning_rate = list(map(float, tokens))

        temp = child.find('n_estimators').text
        tokens = temp.split(',')
        stset.n_estimators = list(map(int, tokens))

        temp = child.find('objective').text
        stset.objective = temp.split(',')

    return( models, stset )


########################################################################################################################
def parse_model_training_file(config_file):
    settings = SETTINGS()
    if os.path.exists(config_file) == False:
        print('\n\n\n!!! Model training file %s does not exist !!!' % (config_file))
        exit()

    try:
        print('Parsing model training settings out of config file: %s' % (config_file))
        tree = et.parse(config_file)
        root = tree.getroot()

        child = root.find('model_name')
        settings.model_name = child.text

        child = root.find('feats_type')
        settings.feats_type = child.text.lower()
        child = root.find('FEATURE_SETTINGS')
        settings.feats = parse_feature_settings(child, feats_type = settings.feats_type)

        child = root.find('pred_type')
        settings.pred_type = child.text.lower()
        child = root.find('PREDICTION_SETTINGS')
        settings.preds = parse_prediction_settings(child, settings.pred_type)

        child = root.find('ml_type')
        settings.ml_type = child.text.lower()
        child = root.find('ML_HYPERPARAMETERS')
        settings.hp = parse_ml_hyperparameters(child, ml_type=settings.ml_type)

        child = root.find('IO_PATHS')
        settings.io_paths = parse_io_paths(child)

        child = root.find('test_ratio')
        if child.text is not None:
            settings.test_ratio = float(child.text.lower())
        else:
            settings.test_ratio = 0.0

        child = root.find('valid_ratio')
        if child.text is not None:
            settings.valid_ratio = float(child.text.lower())
        else:
            settings.valid_ratio = 0.0

        child = root.find('n_models')
        settings.n_models = int(child.text.lower())

        child = root.find('ensemble_type')
        settings.ensemble_type = child.text.lower()

        child = root.find('exclude_sparse_mols')
        settings.exclude_sparse_mols = str_to_bool(child.text)

        child = root.find('sparse_frac')
        settings.sparse_frac = float(child.text)

        child = root.find('siglevel')
        settings.siglevel = float(child.text)

        child = root.find('docker_image_name')
        settings.docker_image_name = child.text

        child = root.find('run_aws_job')
        settings.run_aws_job = child.text

        child = root.find('random_seed')
        if child.text is None:
            settings.random_seed = None
        elif child.text.lower() == 'none':
            settings.random_seed = None
        else:
            settings.random_seed = int(child.text)

    except:
        exit('Error parsing configuration file: %s' % (config_file))


    return(settings)


########################################################################################################################
def parse_io_paths(root):
    io_paths = IO_PATHS()
    io_paths.activity_file = root.find('activity_file').text
    io_paths.save_root = root.find('save_root').text
    io_paths.mol_root = root.find('mol_root').text
    try:
        io_paths.test_set_file = root.find('test_set_file').text
    except:
        io_paths.test_set_file = None
    io_paths.excludes_file = root.find('excludes_file').text

    return(io_paths)



########################################################################################################################
def parse_prediction_settings(root,pred_type='classification'):
    preds = mp.PREDICTION_SETTINGS(pred_type=pred_type)

    if pred_type == 'classification':
        child = root.find('CLASSIFIER_SETTINGS')
        preds.bin_class_thresh = float(child.find('bin_class_thresh').text)
        preds.downsample_balancing = str_to_bool(child.find('downsample_balancing').text)
        preds.upsample_balancing = str_to_bool(child.find('upsample_balancing').text)
        preds.training_weight_type = child.find('training_weight_type').text
        preds.exclude_extra_test = str_to_bool(child.find('exclude_extra_test').text)
    elif pred_type == 'regression':
        preds.exclude_extra_test = False

        child = root.find('REGRESSION_SETTINGS')
        preds.training_weight_type = child.find('training_weight_type').text

        child = root.find('REGRESSION_SETTINGS').find('SCALING')
        preds.scaling.method = child.find('method').text


    return (preds)


########################################################################################################################
# Parse out the hyperparameters for each machine learning type
def parse_feature_settings(root, feats_type='ecfp'):
    feats = []
    feats_type = feats_type.lower()

    if feats_type == 'ecfp':

        child = root.find('ECFP_SETTINGS')
        feats = features.ECFP_FEATURES()
        feats.fp_radius = int(child.find('fp_radius').text)
        feats.n_bits = int(child.find('n_bits').text)
        feats.use_fcfp = str_to_bool(child.find('use_fcfp').text)

    return(feats)


########################################################################################################################
# Parse out the hyperparameters for each machine learning type
def parse_ml_hyperparameters(root, ml_type='ann'):
    ml_type = ml_type.lower()
    hp = []

    if ml_type == 'ann':
        #TODO: change where ANN is defined using Keras library
        hp = ann.ANN_HP()
        child = root.find('ANN_HP')
        hp.n_neur = int(child.find('n_neur').text)
        hp.act_fn = child.find('act_fn').text
        hp.learning_rate = float(child.find('learning_rate').text)
        hp.learning_rule = child.find('learning_rule').text
        hp.learning_momentum = float(child.find('learning_momentum').text)
        hp.n_iters = int(child.find('n_iters').text)
        hp.n_epochs = int(child.find('n_epochs').text)
        hp.n_hidden = int(child.find('n_hidden').text)
        hp.dropout = float(child.find('dropout').text)
        hp.batch_size = int(child.find('batch_size').text)

    elif ml_type == 'pls':
        hp = pls.PLS_HP()
        child = root.find('PLS_HP')
        hp.n_comp = int(child.find('n_comp').text)
        hp.algo = child.find('algo').text.lower()
        hp.n_iters = int(child.find('n_iters').text)
        hp.norm_y_weights = str_to_bool(child.find('norm_y_weights').text)
        hp.deflation_mode = child.find('deflation_mode').text
        hp.scale_data = str_to_bool(child.find('scale_data').text)
        hp.tol = float(child.find('tol').text)

    elif ml_type == 'knn':
        hp = knn.KNN_HP()
        child = root.find('KNN_HP')
        hp.k = int(child.find('k').text)
        hp.n_s = int(child.find('n_s').text)
        hp.n_feats = int(child.find('n_feats').text)
        hp.distance_type = child.find('distance_type').text
        hp.weight_type = child.find('weight_type').text

    elif ml_type == 'xgboost':
        hp = ml.XGBOOST_HP()
        child = root.find('XGBOOST_HP')
        hp.max_depth = int(child.find('max_depth').text)
        hp.learning_rate = float(child.find('learning_rate').text)
        hp.n_estimators = int(child.find('n_estimators').text)
        hp.objective = child.find('objective').text

    return(hp)


########################################################################################################################
def str_to_bool(str):
    value = np.nan

    if str.lower() == 'true':
        value = True
    elif str.lower() == 'false':
        value = False

    return(value)



# ########################################################################################################################
# Packages data for running a job in a Docker container
def build_docker_payload(config_file, job_date_str, job_time_str):

    tree = et.parse(config_file)
    root = tree.getroot()
    job_type = root.attrib['job_type']

    if job_type == 'training':
        settings = parse_model_training_file(config_file)
        io_paths = settings.io_paths
    elif job_type == 'model_stress_test':
        models, st_settings = parse_stress_test_settings_file(config_file)
        io_paths = models.io_paths
    elif job_type == 'prediction':
        settings = parse_prediction_settings(config_file)
        io_paths = settings.io_paths
    else:
        exit('\n!!! Unknown job type:  %s !!!' % (job_type))

    # Get the molecule names, including for the external test set (if used)
    try:
        f = open(io_paths.activity_file,'r')
        lines = f.readlines()
        f.close()
    except:
        exit('Could not read sample names from activity file: %s' % (io_paths.activity_file))

    mol_names = []
    for curline in lines[1:]:
        tokens = curline.rstrip('\r\n').split(',')
        if len(tokens[0]) > 0:
            mol_names.append(tokens[0])

    if io_paths.test_set_file is not None:
        try:
            f_test = open(io_paths.test_set_file,'r')
            lines = f_test.readlines()
            f_test.close()
            for curline in lines[1:]:
                tokens = curline.rstrip('\r\n').split(',')
                if len(tokens[0]) > 0:
                    mol_names.append(tokens[0])
        except:
            exit('Could not read sample names from test set file: %s' % (io_paths.test_set_file))


    # Make the payload folder
    payload_folder = os.path.join(io_paths.save_root,'docker_payload_' + job_date_str + '_' + job_time_str)
    if os.path.exists(payload_folder):
        shutil.rmtree(payload_folder, ignore_errors=True)
    print('Creating job payload folder: %s' % (payload_folder))
    try:
        os.mkdir(payload_folder)
    except:
        exit('Failed to create payload folder:  %s' % (payload_folder))

    # Given the needed molecule names, move them to the folder within the payload root
    n_mols = len(mol_names)

    mol_folder = os.path.join(payload_folder,'mol')
    try:
        os.mkdir(mol_folder)
    except:
        exit('Could not create folder:  %s' % (mol_folder))

    missing_mol_files = []

    for i in range(n_mols):
        base_mol = mol_names[i].upper() + '.mol'
        orig_mol_file = os.path.join(io_paths.mol_root, base_mol)
        moved_mol_file = os.path.join(mol_folder,base_mol)
        if os.path.exists(orig_mol_file):
            shutil.copy(orig_mol_file, moved_mol_file)
        else:
            missing_mol_files.append(base_mol)

    # Move activity (optionally, test_set_file) to payload folder
    activity_folder = os.path.join(payload_folder,'activity')
    activity_root, base_activity_file = os.path.split(io_paths.activity_file)
    try:
        os.mkdir(activity_folder)
    except:
        exit('Could not create folder:  %s' % (activity_folder))

    try:
        shutil.copy(io_paths.activity_file, os.path.join(activity_folder, base_activity_file))
    except:
        exit('Could not move activity file:  %s' % (io_paths.activity_file))

    if io_paths.test_set_file is not None:
        try:
            test_set_root, base_test_set_file = os.path.split(io_paths.test_set_file)
            shutil.copy(io_paths.test_set_file, os.path.join(activity_folder, base_test_set_file))
        except:
            exit('Could not move test set file:  %s' % (io_paths.test_set_file))


    # Write missing molecule names to file
    missing_mols_file = os.path.join(payload_folder, 'missing_mol_files.csv')
    try:
        f = open(missing_mols_file,'w')
        for i in range(len(missing_mol_files)):
            f.write('%s\r\n' % (missing_mol_files[i]))
        f.close()
    except:
        exit('Failed writing file:  %s' % (missing_mols_file))

    # Copy the job .xml file to the payload folder, and then edit the file to run locally (while in the Docker container)
    config_root, base_config_file = os.path.split(config_file)
    payload_config_file = os.path.join(payload_folder, base_config_file)
    try:
        shutil.copy(config_file, payload_config_file)
    except:
        exit('Could not move config file %s from %s  to  %s' % (base_config_file, config_root, payload_folder))

    #  Also edit it to run with the paths defined above for results
    element_tree = et.parse(payload_config_file)
    root = element_tree.getroot()
    docker_mol_root = '/MolPred/mol'
    docker_save_root = '/MolPred/'
    docker_activity_file = os.path.join('/MolPred/activity', base_activity_file)
    if io_paths.test_set_file is not None:
        docker_test_set_file = os.path.join('/MolPred/activity', base_test_set_file)
        root.find('IO_PATHS').find('test_set_file').text = docker_test_set_file
    else:
        docker_test_set_file = ''

    root.find('IO_PATHS').find('mol_root').text = docker_mol_root
    root.find('IO_PATHS').find('save_root').text = docker_save_root
    root.find('IO_PATHS').find('activity_file').text = docker_activity_file

    # In the Docker config file, eliminate the Docker image to avoid infinite container recursion!
    root.find('docker_image_name').text = ''

    # Write the xml changes to file
    element_tree.write(payload_config_file)

    # Zip the folder
    payload_name = payload_folder + '.zip'
    try:
        shutil.make_archive(payload_folder, 'zip', payload_folder)
    except:
        exit('Failed compressing Docker payload folder:  %s' % (payload_name))

    return(payload_name, io_paths)




# ########################################################################################################################
# Filenames and paths where data is retrieved from / written to
class IO_PATHS():
    def __init__(self):
        self.activity_file = None # full path of the activity file.  Can be multitask.  Can have missing values for some tasks
        self.save_root = None # number of classes to generate predictions, for each task
        self.mol_root = None # where to find mol files (if needed)
        self.excludes_file = None # optional.  full filename of excludes file- what molecules to NOT use
        self.model_save_file = None # optional.  full filename of trained, saved model (including path)
        self.test_set_file = None  # optional; file containing molecule names / activities to use in external test set


########################################################################################################################
# This class contains settings for a model inference job
class INFERENCE_SETTINGS():
    def __init__(self):
        self.pred_label = None
        self.report_name = None
        self.model_files = None
        self.mol_root = None
        self.save_root = None
        self.ensemble_type = None
        self.molecules_file = None
        self.excludes_file = None
        self.siglevel = None



########################################################################################################################
# This is the container for setting / storing modeling settings (both for training models and applying previously trained models)
#   These are things that will not change during the training / apply models process
class SETTINGS():
    def __init__(self):
        self.model_name = 'default' # what name to call the model?  It will get appended in various files and folders
        self.pred_type = 'classification' # 'classification' or 'regression' model
        self.preds = None # classification / regression specific settings

        self.feats_type = 'ecfp' # which type of features to use- ecfp, user_defined, ...
        self.ml_type = 'ann' # machine learning type- 'ann', 'pls', 'knn', 'xgboost', ...
        self.test_ratio = 0.2 # fraction of all samples to use for test set
        self.valid_ratio = 0.25 # fraction of 'internal' set to use for validation set
        self.n_models = 100 # number of trained models int he ensemble
        self.ensemble_type = None
        self.do_multitasking = False
        self.exclude_sparse_mols = False # whether or not to exclude sparse molecules
        self.sparse_frac = 0.001 # threshold for declaring a feature occupancy vector to be sparse or not
        self.siglevel = 0.95  # statistical signifcance level for confidence interval calculations

        self.io_paths = IO_PATHS() # paths to load from, save to

        self.hp = None # machine learning hyperparameters to use for model training / applying
        self.feats = None # settings for featurization type
        self.random_seed = None

        # Settings for how to run MolPred job
        self.docker_image_name = None  # name of Docker container in which to run the MolPred job.  If None then run locally
        self.aws_bucket_name = None  # name of AWS bucket to which job gets uploaded