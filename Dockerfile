# This image runs various MolPred processing jobs
#   The container runs the RunMolPredDockerJob.py script.  The script contains command line arguments for the name of the
#   zipped docker payload (which contains all information needed to run the job)

FROM continuumio/anaconda3
RUN apt-get update -y
RUN apt-get install apt-utils -y
RUN apt-get install build-essential -y
RUN apt-get install  gfortran -y
RUN apt-get install wget -y
RUN apt-get install g++ -y
RUN apt-get install liblapack-dev -y
RUN apt-get install libopenblas-dev -y
RUN apt-get install gcc -y

RUN conda install python=3.7


# Set the working directory for running binaries in the Docker image
WORKDIR ~/MolPred

# Need to install RDKit separately from default conda install
COPY requirements.txt ./

# Install Python packages
#RUN conda install --yes --file requirements.txt  # installating from pigar generated requirements.txt fails for some reason...
RUN conda install keras
RUN conda install tensorflow
RUN conda install tensorflow-gpu
RUN conda install joblib
RUN conda install matplotlib
RUN conda install numpy
RUN conda install scikit-learn
RUN conda install scipy

RUN pip install boto3
RUN pip install yagmail

# Install RDKit and XGBoost separately as the default conda installs fail for a Ubuntu Linux container
RUN conda install -c conda-forge rdkit
RUN conda install -c conda-forge xgboost

COPY . .
RUN gcc --version  # check that gcc / g++ got installed correctly

# This is what gets executed when you launch a container
#   Python runs and calls the script RunMolPredDockerJob.py
#   We need to supply arguments to this script at run-time (Docker payload)
CMD [ "python", "./RunMolPredDockerJob.py"]
