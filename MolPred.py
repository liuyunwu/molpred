# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

# These are the core MolPred predictive modeling functions
import numpy as np
import MachineLearningUtils as ml
import Reporting as rep
import Features as feats
import Configuration as cfg
import os, glob, copy, datetime, sys
from sys import platform
import matplotlib
if platform == "darwin":
    # Need to set the graphics back-end to something suitable for MacOS X
    matplotlib.use('agg')
    # matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import pickle
import time
import xml.etree.ElementTree as et
import shutil
import tensorflow.keras.models
from joblib import Parallel, delayed
import functools


########################################################################################################################
# Run the job.  Job type is determined from the job_type tag in the XML file
def run_job(init_config_file):
    success = False

    # Backup and rename the config file so that multiple jobs can be run in parallel
    file_root, file_stem = os.path.split(init_config_file)
    tokens = file_stem.split('.')
    base_file = tokens[0]
    job_time = time.strftime('%H-%M-%S')
    job_date = time.strftime('%m%d%Y')
    base_config_file = base_file + '+' + job_date + '_' + job_time + '+.xml'
    config_file = os.path.join(os.curdir, base_config_file)
    shutil.copy(init_config_file, config_file)

    # Determine what job type is to be run
    tree = et.parse(config_file)
    root = tree.getroot()
    job_type = root.attrib['job_type']

    # Run the job based on job type
    results_root = None
    if job_type == 'training':
        models, results_root = run_model_training(config_file)
    elif job_type == 'model_stress_test':
        results_root = run_model_stress_test(config_file)
    elif job_type == 'inference':
        results_root = run_model_inference(config_file)

    # Delete renamed config_file if not deleted already
    renamed_config_file = os.path.join(file_root, config_file.lstrip('./'))
    if os.path.exists(renamed_config_file):
        os.remove(renamed_config_file)

    return(results_root)


########################################################################################################################
# Load the model configuration (definition) from the config file
def run_model_inference(config_file):
    success = False

    infer_settings = cfg.parse_model_inference_file(config_file)

    job_time = time.strftime('%H-%M-%S')
    job_date = time.strftime('%m%d%Y')
    infer_settings.save_root = os.path.join(infer_settings.save_root, 'InferenceResults_' + job_date + '_' + job_time)
    os.makedirs(infer_settings.save_root)

    # Load the model(s)
    n_model_files = len(infer_settings.model_files)
    n_tasks_models = np.zeros(n_model_files, dtype=np.int32)
    all_models = []
    for i in range(n_model_files):
        try:
            all_models.append(load_model(infer_settings.model_files[i]))
            n_tasks_models[i] = all_models[-1].n_tasks
            print('Loaded model: %s' % (infer_settings.model_files[i]))
        except:
            exit('Could not load model %s!!!' % (infer_settings.model_files[i]))
    n_models = len(all_models)

    #TODO: create a unique task identification scheme which enables models with different numbers of tasks to be used in ensemble predictions for overlapping tasks
    # Check to ensure that all models have the same number of tasks
    unique_numtasks = np.unique(n_tasks_models)
    n_unique_numtasks = int(np.size(unique_numtasks))
    if n_unique_numtasks > 1:
        exit('Ensemble models with different numbers of tasks is not currently supported.')

    print('\nLoading and featurizing data for %d models:' % (n_models))
    all_data_test = load_data_for_inference(all_models, infer_settings)

    # Generate predictions for all molecules and all models.  Perform ensemble prediction if desired
    all_pred_probs = []
    all_class_preds = []
    all_regr_preds = []
    all_regr_mol_preds = []
    for i in range(n_models):
        if all_models[i].pred_type == 'classification':
            # pred_probs, prob_std, class_preds = ml.consensus_pred_class(all_models[i], all_data_test[i].X)
            pred_probs, prob_std, class_preds = ml.models_predict_class(all_models[i], all_data_test[i].X)
            all_pred_probs.append(copy.deepcopy(pred_probs))
            all_class_preds.append(copy.deepcopy(class_preds))

        elif all_models[i].pred_type == 'regression':
            preds_test, preds_test_std, all_preds_test = ml.models_predict(all_models[i], all_data_test[i].X, unscale_predictions=True)

            all_regr_preds.append(copy.deepcopy(preds_test))
            all_regr_mol_preds.append(copy.deepcopy(all_preds_test))


    # Write the inference report
    if all_models[i].pred_type == 'classification':
        rep.classification_inference_report(infer_settings, all_models, all_data_test, all_class_preds, all_pred_probs)
    elif all_models[i].pred_type == 'regression':
        rep.regression_inference_report(infer_settings, all_models, all_data_test, all_regr_preds, all_regr_mol_preds)

    # Back up the model configuration file, renaming it if * delimited naming conventions have been adhered to
    cfg_root, cfg_stem = os.path.split(config_file)
    tokens = cfg_stem.split('+')
    if len(tokens) == 3:
        new_stem = tokens[0] + '_' + tokens[1] + tokens[2]
        renamed_config_file = os.path.join(infer_settings.save_root, new_stem)
        shutil.copy(config_file, renamed_config_file)
    else:
        shutil.copy(config_file, infer_settings.save_root)

    # shutil.copy(config_file, pred_settings.save_root)
    os.remove(config_file)

    return(infer_settings.save_root)



########################################################################################################################
# Run model training using settings from the config file
def run_model_training(config_file):
    t_start = time.time()
    trained_models = False

    settings = cfg.parse_model_training_file(config_file)
    models = MODELS(settings)
    np.random.seed(models.random_seed)


    # Load the data
    ####################################################################################################################
    # Internal (training/validation) external data set split
    # If desired, use a predefined holdout test set

    models, data_int, data_test = load_data_int_test(models)

    t_start_train = time.time()

    models = train_bagging(models, data_int, process_type='serial')

    # TODO: The parallel training works, but it crashes (with ANNs) when you try to use models trained in a
    #   multithreaded (parallel) manner after training finishes.  Known problem that Keras / Tensorflow doesn't seem to
    #   want to fix...  The workaround is really ugly so I'm not implementing it for now
    # models = train_bagging_parallel(models, data_int)

    t_stop_train = time.time()
    print('Took %.3f seconds to train models' % (t_stop_train-t_start_train))

    ##################################################################
    # Write summary report
    # Compute the model performance statistics and generate the summary report
    print('\n\nModeling wrap-up (writing report, saving models)')
    models, wrote_report, model_save_folder = modeling_wrapup(models, data_int, data_test)


    ###################################################################
    # Calculate the most significant features of the model with various methods
    # print('\n\nCalculating feature significance:')
    # feature_significance(models, data_int)

    ########################################################################################################################

    archive_folder = os.path.join(model_save_folder,'ModelArchive')
    os.mkdir(archive_folder)
    print('\n\nCreating model archive:')
    archived_files = rep.modeling_file_archive(archive_folder, models, config_file, data_int.mols, data_test.mols, archive_config=True)


    t_done = time.time()
    print('\033[1;46mModeling complete.  Took %.3f  seconds to complete\033[1;m' % (t_done - t_start))

    return(models, model_save_folder)



########################################################################################################################
# Identify the most significant features of a trained model.  Methods used depend on feature and ML type of model
def feature_significance(models, data):


    all_msf = [] # container for all most significant features

    # Do the generic feature significance algorithms- ones shared by all methods
    sigfeatsidx_tasks = ml.feature_significance_univariate(models, data.X, n_sig=100)


    if len(all_msf) == 0:
        all_msf = None

    return(all_msf)


########################################################################################################################
# Load "internal" and test data as part of a model training / model stress test job
def load_data_int_test(models):
    # models.feats = feats.initialize_features(models.feats)
    extra_mols = []
    io = models.io_paths

    int_mols, models.task_names, models.n_tasks = load_data_multiacts(models, activity_file = io.activity_file,
                                                                      exclude_nans=False)
    if models.pred_type == 'classification':
        models.n_class, models.ns_class, models.n_missing = nclass_from_mols(int_mols)

        if models.n_tasks > 1:
            models.preds.exclude_extra_test = False
            if models.preds.downsample_balancing is True:
                models.preds.downsample_balancing = False
                print('\n\nWARNING: Performing multitask learning.  Ignoring downsample flag setting\n\n')
            if models.preds.upsample_balancing is True:
                models.preds.downsample_balancing = False
                print('\n\nWARNING: Performing multitask learning.  Ignoring upsample flag setting\n\n')


    # Test and train/validation partition.  Then do featurization
    if io.test_set_file is not None:
        # Draw test set from the provided predefined test set file
        test_mols, task_names, n_tasks = load_data_multiacts(models, activity_file=io.test_set_file)
        int_mols, test_mols = deduplicate_mols(int_mols, test_mols, eliminate_duplicates=True)

        # models, int_mols = feats.feature_selection(int_mols, models)
        X_int, Y_int, int_mols, int_exclude_mols = feats.feature_encoding(int_mols, models)
        X_test, Y_test, test_mols, test_exclude_mols = feats.feature_encoding(test_mols, models)
        models.test_ratio = float(X_test.shape[0]) / float(X_int.shape[0])

    else:
        # Draw test set randomly as per the balancing option selected (if any)
        int_mols, test_mols, extra_mols = ml.partition_list(int_mols, models, test_ratio=models.test_ratio,
                                                                 pred_type=models.pred_type, exclude_extra_test=models.preds.exclude_extra_test)
        # models, int_mols = feats.feature_selection(int_mols, models)
        X_int, Y_int, int_mols, int_exclude_mols = feats.feature_encoding(int_mols, models)
        X_test, Y_test, test_mols, test_exclude_mols = feats.feature_encoding(test_mols, models)


    # Rescale regression data if so desired
    if models.pred_type == 'regression':
        models.scaling, Y_int = ml.set_scaling(models.preds.scaling, Y_int)
        if np.size(Y_test) > 0:
            Y_test = ml.apply_scaling(models.preds.scaling, Y_test)

    models = initialize_sample_counts(models, X_int, X_test, Y_int, Y_test)

    # Store molecular and activity data in the appropriate containers
    rawdata_int = RAW_DATA()
    rawdata_int.mols = copy.deepcopy(int_mols)
    rawdata_int.extra_mols = copy.deepcopy(extra_mols)
    rawdata_int.exclude_mols = copy.deepcopy(int_exclude_mols)
    rawdata_int.X = copy.deepcopy(X_int)
    rawdata_int.Y = copy.deepcopy(Y_int)

    rawdata_test = RAW_DATA()
    rawdata_test.mols = copy.deepcopy(test_mols)
    rawdata_test.extra_mols = copy.deepcopy(extra_mols)
    rawdata_test.exclude_mols = copy.deepcopy(test_exclude_mols)
    rawdata_test.X = copy.deepcopy(X_test)
    rawdata_test.Y = copy.deepcopy(Y_test)


    return( models, rawdata_int, rawdata_test )



########################################################################################################################
# Load test data as part of an inference job.  Assumes a list of models
#   This procedure stores X and Y data separately for each model.  There are several reasons for doing this, despite
#   duplication: (1) different featurization algorithms (ECFP vs. user defined, etc.) will featurize the same molecule in
#   completely different ways; (2) different models using the same featurization algorithm typically use different
#   featurization settings, resulting in different X matrices; (3) Different featurization algorithms fail on different
#   molecules, resulting in different sets of molecules for which we can even generate predictions; (4) Different
#   regression models typically use different activity scalers, so we need to maintain different scalings for different
#   models.  Probably more... best to keep things separate
def load_data_for_inference(all_models, infer_settings, use_common_mols = True):

    extra_mols = []
    n_models = len(all_models)
    all_data_test = []

    # If so desired, first determine which molecules are in common
    if use_common_mols is True:
        all_mol_names = []
        all_test_mols = []
        for i in range(n_models):
            print('\tLoading and featurizing data for %s' % (all_models[i].model_name))
            all_models[i].io_paths.mol_root = infer_settings.mol_root
            all_models[i].io_paths.excludes_file = infer_settings.excludes_file
            cur_test_mols, task_names, n_tasks = load_data_multiacts(all_models[i], activity_file=infer_settings.molecules_file)
            cur_mol_names = []
            for mol in cur_test_mols:
                cur_mol_names.append(mol.mol_name)
            all_mol_names.append(cur_mol_names)
            all_test_mols.append(cur_test_mols)

        # Find the intersection- all mol names in common across all models
        common_mol_names = list(functools.reduce(set.intersection, [set(item) for item in all_mol_names]))
        n_common = len(common_mol_names)
        for i in range(n_models):
            cur_test_mols = []
            for j in range(n_common):
                curidx = all_mol_names[i].index(common_mol_names[j])
                cur_test_mols.append(all_test_mols[i][curidx])

        all_test_mols.append(copy.deepcopy(cur_test_mols))

    for i in range(n_models):
        if use_common_mols is not True:
            print('\tLoading data for %s' % (all_models[i].model_name))
            all_models[i].io_paths.mol_root = infer_settings.mol_root
            all_models[i].io_paths.excludes_file = infer_settings.excludes_file
            test_mols, task_names, n_tasks = load_data_multiacts(all_models[i], activity_file=infer_settings.molecules_file)
        else:
            test_mols = all_test_mols[i]
        print('\tFeaturizing data for %s' % (all_models[i].model_name))
        X_test, Y_test, test_mols, test_exclude_mols = feats.feature_encoding(test_mols, all_models[i])


        # Store molecular and activity data in the appropriate containers
        rawdata_test = RAW_DATA()
        rawdata_test.mols = copy.deepcopy(test_mols)
        rawdata_test.extra_mols = copy.deepcopy(extra_mols)
        rawdata_test.exclude_mols = copy.deepcopy(test_exclude_mols)
        rawdata_test.X = copy.deepcopy(X_test)
        rawdata_test.Y = copy.deepcopy(Y_test)

        all_data_test.append(copy.deepcopy(rawdata_test))

    return( all_data_test )


########################################################################################################################
# Runs the model stress test using the settings found in the StressTest_Control.xml file
def run_model_stress_test(config_file):

    models, stset = cfg.parse_stress_test_settings_file(config_file)

    np.random.seed(models.random_seed)

    cur_time = time.strftime('%H-%M-%S')
    cur_date = time.strftime('%m%d%Y')
    date_time_str = cur_date + '_' + cur_time
    base_save_folder = models.model_name + '_' + models.feats_type + '_' + models.ml_type + '_' + date_time_str
    models.io_paths.save_root = os.path.join(models.io_paths.save_root, base_save_folder)
    if os.path.exists(models.io_paths.save_root) == False:
        os.mkdir(models.io_paths.save_root)

        # Backup the config file
        cfg_root, cfg_stem = os.path.split(config_file)
        tokens = cfg_stem.split('*')
        if len(tokens) == 3:
            new_stem = tokens[0] + tokens[2]
            renamed_config_file = os.path.join(models.io_paths.save_root, new_stem)
            shutil.copy(config_file, renamed_config_file)
        else:
            shutil.copy(config_file, models.io_paths.save_root)

    wrote_st_report = model_stress_test(models, stset, config_file, report_ext=date_time_str)

    # Delete the renamed config file, if it exists and if it appears to have been renamed
    if os.path.isfile(config_file):
        cfg_path, cfg_stem = os.path.split(config_file)
        tokens = cfg_stem.split('*')
        if len(tokens) > 2:
            os.remove(config_file)

    return(models.io_paths.save_root)


########################################################################################################################
# Tally up counts per task
def initialize_sample_counts(models, X_int, X_test, Y_int, Y_test):

    #models.valid_ratio = models.valid_ratio * np.ones(models.n_tasks)
    if models.pred_type == 'classification':
        models.preds.bin_class_thresh = models.preds.bin_class_thresh * np.ones(models.n_tasks)

    models.n_int = int(np.size(Y_int,0))
    models.n_test = int(np.size(Y_test,0))
    models.n_valid = int(np.round(models.valid_ratio * models.n_int))
    models.n_train = models.n_int - models.n_valid
    models.n_feats = int(np.size(X_int,1))
    models.n_grandtotal = models.n_int + models.n_test
    models.n_totalintask = np.zeros(models.n_tasks, dtype=np.int32)
    models.n_missing = np.zeros(models.n_tasks, dtype=np.int32)
    for i in range(models.n_tasks):
        notnan_int = np.where(np.isnan(Y_int[:,i]) == False)[0]
        nan_int = np.where(np.isnan(Y_int[:, i]) == True)[0]

        if models.n_test > 0:
            notnan_test= np.where(np.isnan(Y_test[:,i]) == False)[0]
            nan_test = np.where(np.isnan(Y_test[:, i]) == True)[0]
        else:
            notnan_test = np.array([])
            nan_test = np.array([])

        models.n_totalintask[i] = int(np.size(notnan_int)) + int(np.size(notnan_test))
        models.n_missing[i] = int(np.size(nan_int)) + int(np.size(nan_test))

    #TODO: filter out molecules that can't be used.  This can be a molecule with activities missing from all tasks, or
    #   (depending on the ml_type used) a molecule with just 1 missing activities

    return(models)



########################################################################################################################
# Matches molecules with predictor and response data
# Returns:
# matched_mols, Molecules with predictors and corresponding responses
# noresp_mols,  Molecules with predictors but no responses.  Can be used for prediction after doing model building
def predictor_response_match(all_acts, mol_names_acts, all_mols):
    matched_mols = []
    noact_mols = []
    Npred = len(all_mols) # num predictor molecules
    Nresp = np.size(all_acts,0) # num responses to try to match to

    matched_pred = np.zeros((Npred,1),dtype=bool)
    matched_resp = np.zeros((Nresp,1),dtype=bool)
    for p in range(0, Npred ):
        if( matched_pred[p]):
            continue
        for r in range(0, Nresp):
            if( matched_resp[r]):
                continue
            if( all_mols[p].mol_name.lower() == mol_names_acts[r].lower()):
                # Found a match
                all_mols[p].acts = copy.deepcopy(all_acts[r])
                matched_pred[p] = True
                matched_resp[r] = True
                break

    # Populate lists of matched and no response molecules
    for p in range(0, Npred):
        if( matched_pred[p]):
            matched_mols.append(copy.deepcopy(all_mols[p]))
        else:
            noact_mols.append(copy.deepcopy(all_mols[p]))

    # Find molecules that have activities but no predictor information
    nopred_acts = []
    nopred_acts_names = []
    for r in range(0, Nresp):
        if matched_resp[r] == True:
            continue
        nopred_acts.append(copy.deepcopy(all_acts[r]))
        nopred_acts_names.append(copy.deepcopy(mol_names_acts[r].lower()))

    print('Found ' + str(len(matched_mols)) + ' molecules with matched predictor, response information')
    # print('Found ' + str(len(noact_mols)) + ' molecules with predictors but no response information')
    print('Found ' + str(len(nopred_acts)) + ' molecules with responses but no predictor information')


    # Show matched, unmatched data help with data reduction
    print('%d molecules with activities but without chemical shifts' % (len(nopred_acts)))

    return( matched_mols, noact_mols, nopred_acts, nopred_acts_names)



########################################################################################################################
# Parses activity data from a .csv file for each molecule.  Activity assumed to be in second column.  All other columns ignored
def read_acts_file(acts_file, model_type = 'classification', excludes_file = None):
    print('Reading activity data from: %s' % (acts_file))
    acts = []
    mol_names = []

    f = open(acts_file)
    lines = f.readlines()
    f.close()

    ct = 0
    for line in lines:
        if( ct == 0):
            ct += 1 # assumes top row is a header row
            continue
        newline = line.rstrip('\r\n')
        tokens = newline.split(',')

        if len(tokens) < 2:
            continue
        if len(tokens[0]) < 1 or len(tokens[1]) < 1:
            continue

        mol_names.append(tokens[0].lower())
        if model_type.lower() == 'regression':
            acts.append(float(tokens[1]))
        else:
            acts.append(float(tokens[1]))
        # print(mol_names[-1] + ',  ' + str(acts[-1]))


    # Check for duplicate molecule names in the activity file.  Show duplicates (if any) to user- the user must handle
    #   this situation
    core_names = []
    for mol_name in mol_names:
        curbasename = mol_name.split('-')
        core_names.append(copy.deepcopy(curbasename[0]))

    nacts = len(core_names)

    # unique_names, unique_counts = np.unique(core_names, return_counts=True)
    # nunique = len(unique_names)
    # if nunique < nacts:
    #
    #     dupidx = np.where( unique_counts > 1)[0]
    #     ndupes = np.size(dupidx)
    #     print('\n\n\n\033[1;41m!!! %d duplicate core structures detected in activity file: %s !!!' % (ndupes, acts_file))
    #     for i in range(0, ndupes):
    #         print('\t%s,  listed %d times in activity file' % (unique_names[dupidx[i]], unique_counts[dupidx[i]]))
    #
    #     print('\033[1;m\n\n')

    # Get the list of the excluded molecules from the excluded file, if one is supplied
    if excludes_file is not None:
        got_excludes = False
        try:
            exclude_mol_names = read_excludes_file(excludes_file)

            excludeidx = []
            for i in range(0, nacts):
                if mol_names[i] in exclude_mol_names:
                    excludeidx.append(copy.deepcopy(i))

            excludeidx = np.array(excludeidx)
            keepidx = np.setdiff1d(np.arange(0,nacts), excludeidx)

            keep_mol_names = []
            keep_acts = []
            for i in range(0,np.size(keepidx)):
                keep_mol_names.append(copy.deepcopy(mol_names[keepidx[i]]))
                keep_acts.append(copy.deepcopy(acts[keepidx[i]]))

            if np.size(excludeidx) > 0:
                print('\n\n%d molecule(s) from activity file on excludes list:' % (np.size(excludeidx)))
                for i in range(0, np.size(excludeidx)):
                    print('\tExcluding molecule: %s' % (mol_names[excludeidx[i]]))
                print('\n\n')
            mol_names = copy.deepcopy(keep_mol_names)
            acts = copy.deepcopy(keep_acts)

        except:
            print('Could not read molecules from excludes file: %s' % (excludes_file))

    # convert activities to a numpy array
    if len(acts) > 0:
        acts = np.array(acts).reshape((len(acts),1))

    return( acts, mol_names)


########################################################################################################################
# Parses a .csv file with multiple activities / tasks per molecule.  This function assumes:
# (1) the first row is a header row- contains the names of the activities.  If there is no header name, that column
#       won't be taken to be an activity
# (2) the number of tasks is taken to be the number of consecutive task names in the header row
# (3) there can be missing values for some tasks in molecules, but every molecule must have at least one activity or it
#       will be ignored
# (4) the first column is the molecule name
def read_multiacts_file(multiacts_file, model_type = 'classification', excludes_file = None):
    print('Reading activity data from: %s' % (multiacts_file))

    exclude_mol_names = []
    if excludes_file is not None:
        exclude_mol_names = read_excludes_file(excludes_file)

    f = open(multiacts_file)
    lines = f.readlines()
    f.close()

    # Header row is used to determine # of activities and name of activities
    tokens = lines[0].rstrip('\r\n').split(',')
    max_n_tasks = len(tokens) - 1
    task_names = []
    for i in range(1,len(tokens)):
        # If there is no column header, this denotes that there are no additional tasks to include for the modeling
        if len(tokens[i]) == 0:
            break
        task_names.append(tokens[i].lower())
    n_tasks = len(task_names)

    n_lines = len(lines)
    nmols = n_lines - 1 # assumes that the number of molecules equals # rows - 1 header row.  Corrected later if needed
    multiacts = np.zeros((nmols,n_tasks))
    mol_names = []
    actexcludeidx = []

    for i in range(1,n_lines):
        newline = lines[i].rstrip('\r\n')
        tokens = newline.split(',')

        # Check if there's no molecule name- nothing to match
        if len(tokens[0]) == 0:
            actexcludeidx.append(copy.deepcopy(i-1))
            continue

        # Check if the current molecule name is in the excludes list
        if tokens[0].lower() in exclude_mol_names:
            print('Molecule %s excluded from job (it is in the excludes file)' % (tokens[0].lower()))
            actexcludeidx.append(copy.deepcopy(i-1))
            continue

        mol_names.append(tokens[0].lower())

        for j in range(0,n_tasks):
            if len(tokens[j+1]) == 0:
                multiacts[i-1,j] = np.nan
            else:
                multiacts[i-1,j] = float(tokens[j+1])

    actexcludeidx = np.array(actexcludeidx)
    keepidx = np.setdiff1d(np.arange(0,nmols),actexcludeidx)
    multiacts = multiacts[keepidx,:]

    #TODO: include a check to exclude tasks that have no remaining activities.  Needs to compatible with prediction jobs
    #  and other scenarios where activities are not known or otherwise not provided

    # Check for duplicate molecule names in the activity file.  Show duplicates (if any) to user- the user must handle
    #   this situation
    # core_names = []
    # for mol_name in mol_names:
    #     curbasename = mol_name.split('-')
    #     core_names.append(copy.deepcopy(curbasename[0]))

    nmols = len(mol_names)
    unique_names, unique_counts = np.unique(mol_names, return_counts=True)
    nunique = len(unique_names)

    if nunique < nmols:
        dupidx = np.where( unique_counts > 1)[0]
        ndupes = np.size(dupidx)
        print('\n\n\n\033[1;41m!!! %d duplicate molecule names detected in activity file: %s !!!' % (ndupes, multiacts_file))
        for i in range(0, ndupes):
            print('\t%s,  listed %d times in activity file' % (unique_names[dupidx[i]], unique_counts[dupidx[i]]))
        print('\033[1;m\n\n')

    return( multiacts, mol_names, task_names)




########################################################################################################################
# Parses the excluded molecules file and returns a list of molecule names to excluded
def read_excludes_file(excludes_file):
    excluded_mol_names = []

    try:
        f = open(excludes_file)
        lines = f.readlines()
        f.close()
        nlines = len(lines)

        for i in range(1, nlines):
            tokens = lines[i].split(',')
            if len(tokens[0]) > 0:
                excluded_mol_names.append(tokens[0].rstrip('\r\n').lower())

    except:
        print('\n\nError encountered parsing the excludes file!!!  No molecules will be excluded...\n\n')
        excluded_mol_names = []

    return( excluded_mol_names )




########################################################################################################################
# Merges two lists of MOL objects to avoid duplicates.  This function puts data from different sources on an equal
#   footing.  In the event that both lists have the same molecule, pick the one that has more relevant isotopes
def merge_mol_lists(mols0, mols1 = []):
    merged_mols = []
    N0 = len(mols0)
    N1 = len(mols1)

    merged0 = np.zeros(N0, dtype=bool)
    merged1 = np.zeros(N1, dtype=bool)

    for i in range(0, N0):
        for j in range(0, N1):
            if merged1[j] == True:
                continue

            if( mols0[i].mol_name == mols1[j].mol_name ):
                # Merge these two molecules; can't just combine all chemical shifts or there will be duplicate shifts
                if( np.size(mols0[i].elem) >= np.size(mols1[j].elem)):
                    merged_mols.append( copy.deepcopy(mols0[i]))
                else:
                    merged_mols.append( copy.deepcopy(mols1[j]))
                merged0[i] = True
                merged1[j] = True

                break
        # If ith molecule in mols0 list hasn't been merged then add it to the merged list
        if merged0[i] == False:
            merged_mols.append(copy.deepcopy(mols0[i]))

    # Check for unmerged molecules in mols1 list
    for j in range(0, N1):
        if merged1[j] == True:
            continue
        merged_mols.append( copy.deepcopy(mols1[j]) )
        merged1[j] = True

    return( merged_mols)


########################################################################################################################
# Divides a list of molecules into a test vs. internal set.  Need to specity either test or internal set (but not both)
def partition_mols(mols, test_set = None, internal_set = None):
    intmols = []
    testmols = []

    if np.any(test_set == None) and np.all(internal_set != None):
        use_int = True
        use_test = False
    elif np.any(test_set != None) and np.all(internal_set == None):
        use_int = False
        use_test = True

    if np.any(test_set == None) and np.any(internal_set == None):
        return( intmols, testmols)

    for n in range(0, len(mols)):
        if use_int == True:
            matchidx = np.intersect1d(n, internal_set)
            if len(matchidx > 0):
                intmols.append(copy.deepcopy(mols[n]))
            else:
                testmols.append(copy.deepcopy(mols[n]))
        else:
            matchidx = np.intersect1d(n, test_set)
            if len(matchidx > 0):
                testmols.append(copy.deepcopy(mols[n]))
            else:
                intmols.append(copy.deepcopy(mols[n]))

    return( intmols, testmols)



########################################################################################################################
# Converts floating point activities to class labels as per predefined thresholds
def convert_floatacts_to_classlabels(Yacts, class_thresh):
    class_thresh = np.sort(class_thresh)
    Nclass = np.size(class_thresh) + 1
    Ns = np.size(Yacts,0)
    Ne = np.size(Yacts,1) # number of endpoints
    Ylabels = np.zeros((Ns, Ne), dtype=np.int32)

    for n in range(0, Ns):

        greateridx = np.where( Yacts[n] > class_thresh)[0]
        if len(greateridx) == 0:
            Ylabels[n] = 0
        else:
            Ylabels[n] = int(np.max(greateridx) + 1)

    return(Ylabels)


########################################################################################################################
# Computes figure of merit for a hyperparameter optimization process
def compute_figure_of_merit(valid_stats, train_stats = None, pred_type = 'classification', preferred_class = None):

    if pred_type == 'classification':
        if preferred_class == None:
            # Simple method: just average the diagonals of the conditional probability matrix
            # This method gives equal weight to Pcc for each class (independent of # samples)
            valid_diag = np.diag( valid_stats.Pmat )
            Nclass = np.size(valid_diag)
            penalty_accuracy = Nclass - np.sum(valid_diag)

        else:
            preferred_class = np.array(preferred_class)
            Npref = np.size(preferred_class)
            valid_diag = np.diag( valid_stats.Pmat )
            valid_diag = valid_diag[preferred_class]
            penalty_accuracy = Npref - np.sum(valid_diag)

        # Penalize imbalanced performance across all classes
        penalty_imbalance = np.std(valid_diag)

        # Optional: penalize overfitting
        penalty_overfit = 0.0
        if train_stats != None:
            train_diag = np.diag( train_stats.Pmat )
            overfit = np.maximum(train_diag - valid_diag, np.zeros(Nclass))
            penalty_overfit = np.sum(overfit)

        fom = -(penalty_accuracy + penalty_imbalance + penalty_overfit)

    elif pred_type == 'regression':
        # Accuracy penalty must take into account that R2 values can be negative.  More negative values --> larger penalty
        # Also take into account training and validation / test set accuracy separately
        # penalty_accuracy = (1.0 - valid_stats) + (1.0 - train_stats)
        penalty_overfit = np.max(train_stats - valid_stats,0)

        fom = train_stats + 2.0*valid_stats - penalty_overfit
        # fom = valid_stats - penalty_overfit
        # fom = valid_stats

    return( fom )


########################################################################################################################
# Assign weights to every individual training sample.  Depends on prediction type and user selections for how to weight samples
def assign_training_weights(training_weight_type, pred_type, Y_train):
    n_train = int(np.size(Y_train,0))
    n_tasks = int(np.size(Y_train,1))

    weights_train = np.zeros((n_train, n_tasks))

    for t in range(n_tasks):
        missingidx = np.where(np.isnan(Y_train[:, t]))[0]
        sample_weights = np.ones(n_train)
        sample_weights[missingidx] = 0

        usableidx = np.setdiff1d(np.arange(0, n_train), missingidx)
        n_missing = int(np.size(missingidx))
        n_usable = n_train - n_missing
        uclasses = np.unique(Y_train[usableidx, t])
        n_class = int(np.size(uclasses))

        if pred_type == 'regression' or (pred_type == 'classification' and training_weight_type == 'equal'):
            # All samples get equal weight, except for missing values
            weights_train[:,t] = np.squeeze(sample_weights)
            continue

        wt_per_class = float(n_usable) / float(n_class)  # All classes have equal weights
        for c in range(0, n_class):
            curidx = np.where(Y_train[:, t] == c)[0]
            inv_class_frac = wt_per_class / float(np.size(curidx))
            sample_weights[curidx] = inv_class_frac

        # This is a proportional weighting
        sample_weights /= np.unique(np.max(sample_weights[sample_weights > 0]))

        if training_weight_type.lower() == 'exponential':
            sample_weights = np.exp(weights_train)
        elif training_weight_type.lower() == 'squared':
            sample_weights = sample_weights ** 2
        elif training_weight_type.lower() == 'cubic':
            sample_weights = sample_weights ** 3.0
        elif training_weight_type.lower() == 'quartic':
            sample_weights = sample_weights ** 4.0

        weights_train[:, t] = np.squeeze(sample_weights)

    return( weights_train )




########################################################################################################################
# This function trains a series of models in a "bagging" process, where the training / validation sets are randomly
#   shuffled.  This is for concurrent multitask learning.
# You can train bagging models serially or in parallel
def train_bagging(models, data, process_type = None):

    if process_type is None:
        # The default is to train models in parallel.  Except for ANNs, there is a problem with applying Tensorflow
        #   models trained in different threads from main execution thread
        if models.ml_type != 'ann':
            models = train_bagging_parallel(models, data)
        else:
            models = train_bagging_serial(models, data)

    elif process_type == 'parallel':
        models = train_bagging_parallel(models, data)

    elif process_type == 'serial':
        models = train_bagging_serial(models, data)

    else:
        exit('unknown bagging training type %s!' % (process_type))

    return( models )


########################################################################################################################
# This function trains a series of models in a parallel "bagging" process, where the training / validation sets are randomly
#   shuffled
def train_bagging_parallel(models, data):

    valid_ratio_target = copy.deepcopy(models.valid_ratio)

    par_models, par_valid_sets, par_train_sets, par_valid_names, par_train_names = \
        zip(*Parallel(n_jobs=-1)(delayed(train_eval_model)(models, i, data, valid_ratio_target)
            for i in range(models.n_models)))

    models.all_models = list(par_models)
    models.valid_sets = list(par_valid_sets)
    models.train_sets = list(par_train_sets)
    models.valid_names = list(par_valid_names)
    models.train_names = list(par_train_names)
    models.valid_ratio = np.float32(models.n_valid) / (np.float32(models.n_train) + np.float32(models.n_valid))

    return( models )



########################################################################################################################
# This function serially trains a series of models in a "bagging" process, where the training / validation sets are randomly
#   shuffled
def train_bagging_serial(models, data):

    valid_ratio_target = copy.deepcopy(models.valid_ratio)

    ################################################################
    # Perform bagging trials.  In each trial the internal data is partitioned into training and validation sets
    all_models = []

    perf_stats = []
    for i in range(models.n_models):
        perf_stats.append(ml.PERF_STATS(pred_type = models.pred_type, n_models=models.n_models))

    all_valid_sets = []
    all_train_sets = []
    all_valid_names = []
    all_train_names = []
    modelct = 0

    while modelct < models.n_models:
        model, valid_set, train_set, valid_names, train_names = train_eval_model(models, modelct, data, valid_ratio_target)
        all_models.append(model)
        all_valid_sets.append(valid_set)
        all_train_sets.append(train_set)
        all_valid_names.append(valid_names)
        all_train_names.append(train_names)
        modelct += 1

    models.all_models = all_models
    models.valid_sets = copy.deepcopy(all_valid_sets)
    models.train_sets = copy.deepcopy(all_train_sets)
    models.valid_names = copy.deepcopy(all_valid_names)
    models.train_names = copy.deepcopy(all_train_names)
    models.valid_ratio = np.float32(models.n_valid) / (np.float32(models.n_train) + np.float32(models.n_valid))

    return( models )


########################################################################################################################
def train_eval_model(models, modelct, data_int, valid_ratio_target):

    print('\033[1;46mBuilding %s model %d of %d:\033[1;m' % (models.ml_type,modelct+1,models.n_models) )

    X_train, X_valid, Y_train, Y_valid, cur_valid_set = ml.partition_data(data_int.X, data_int.Y, valid_ratio_target, pred_type=models.pred_type)
    models.n_valid = np.size(X_valid,0)
    models.n_train = np.size(X_train,0)
    models.valid_ratio = np.float64(models.n_valid) / np.float64(models.n_int)

    weights_train = assign_training_weights(models.preds.training_weight_type, models.pred_type, Y_train)

    if models.pred_type == 'regression':

        model = ml.train_a_model(models.hp, models.pred_type, models.ml_type, X_train, Y_train,
                                     weights_train=weights_train, task_names=models.task_names)

        Y_pred_train = ml.model_predict(model, X_train, models.pred_type, models.ml_type, models.hp)
        Y_pred_valid = ml.model_predict(model, X_valid, models.pred_type, models.ml_type, models.hp)

        # Get performance statistics
        train_stats, train_errors = ml.error_stats(Y_pred_train, Y_train)

        if models.n_valid > 2:
            valid_stats, valid_errors = ml.error_stats(Y_pred_valid, Y_valid)

        print('R2_valid = ' + str([stats.rsq for stats in valid_stats]))
        print('R2_train = ' + str([stats.rsq for stats in train_stats]))


    elif models.pred_type == 'classification':
        ############################################################################################################
        print('training a model...')
        model = ml.train_a_model(models.hp, models.pred_type, models.ml_type, X_train, Y_train,
                                 weights_train=weights_train, task_names=models.task_names)

        prob_train, Y_pred_train = ml.model_predict(model, X_train, models.pred_type, models.ml_type, models.hp, models.n_class, models.preds.bin_class_thresh)
        prob_valid, Y_pred_valid = ml.model_predict(model, X_valid, models.pred_type, models.ml_type, models.hp, models.n_class, models.preds.bin_class_thresh)

        for t in range(models.n_tasks):
            cur_train_cs = ml.perf_stats_class(Y_pred_train[t], Y_train[:,t].reshape((models.n_train,1)), models.n_class[t], pred_probs=prob_train[t])
            cur_valid_cs = ml.perf_stats_class(Y_pred_valid[t], Y_valid[:,t].reshape((models.n_valid,1)), models.n_class[t], pred_probs=prob_valid[t])

            # print('\tPcc (train) = %.3f;  Pcc (valid) = %.3f' % (cur_train_cs.accuracy, cur_valid_cs.accuracy))
            print('Performance statistics for bagging model # %d (of %d)' % (modelct+1, models.n_models))
            print('Performance statistics for Task %s (#%d of %d)' % (models.task_names[t], t+1, models.n_tasks))
            print('***** Training Set Statistics *****')
            print('Accuracy (train) = %.3f' % (cur_train_cs.accuracy))
            print('Confusion Matrix:')
            print(cur_train_cs.CM)
            print('Conditional probability matrix:')
            print(cur_train_cs.Pmat)
            print('# samples per class:')
            print(cur_train_cs.ns_class)
            print('total # samples = %d' % (np.sum(cur_train_cs.ns_class)))
            print('\n***** Validation Set Statistics *****')
            print('Accuracy (valid) = %.3f' % (cur_valid_cs.accuracy))
            print('Confusion Matrix:')
            print(cur_valid_cs.CM)
            print('Conditional probability matrix:')
            print(cur_valid_cs.Pmat)
            print('# samples per class:')
            print(cur_valid_cs.ns_class)
            print('total # samples = %d' % (np.sum(cur_valid_cs.ns_class)))

    # Logging for post-processing and analysis
    cur_train_set = np.setdiff1d(np.arange(0,models.n_int), cur_valid_set)
    cur_train_names = []
    for p in range(0, models.n_train):
        cur_train_names.append(copy.deepcopy(data_int.mols[cur_train_set[p]].mol_name))
    cur_valid_names = []
    for p in range(0, models.n_valid):
        cur_valid_names.append(copy.deepcopy(data_int.mols[cur_valid_set[p]].mol_name))

    cur_valid_set = cur_valid_set.reshape((1, models.n_valid))
    cur_train_set = cur_train_set.reshape((1, models.n_train))

    return(model, cur_valid_set, cur_train_set, cur_valid_names, cur_train_names)


########################################################################################################################
def save_model(models,filename):

    if models.ml_type == 'ann':
        # Can't directly save an ensemble of Keras models.  Need to save each individual model
        file_root, file_stem = os.path.split(filename)
        base_file = file_stem[0:file_stem.rindex('.')]
        all_models = models.all_models

        # Save the individual models in HDF5 files.  These files store the weight and network architectures
        model_filenames = []
        full_filenames = []
        for i in range(len(all_models)):
            model_filenames.append(base_file + '_Model' + str(i) + '.h5')
            full_filename = os.path.join(file_root,model_filenames[-1])
            tensorflow.keras.models.save_model(all_models[i], full_filename)
            print('Saving submodel (%d of %d): %s' % (i+1, len(all_models), model_filenames[-1]))

        # Save the model "metadata" in a .pkl file
        models.model_filenames = copy.deepcopy(model_filenames)
        models.all_models = None # blow away the individual ANNs to save the .pkl file
        with open(filename, 'wb') as output:
            pickle.dump(models, output, pickle.HIGHEST_PROTOCOL)

        # Restore the models into RAM for additional post-processing (if so desired)
        models.all_models = all_models

    else:
        with open(filename,'wb') as output:
            pickle.dump(models, output, pickle.HIGHEST_PROTOCOL)

    return( True )


########################################################################################################################
def load_model(filename):
    print('Loading model from file:  %s' % (filename))

    with open(filename,'rb') as input:
        models = pickle.load(open(filename, 'rb'))

    if models.ml_type == 'ann':
        # Load individual submodels.  Assumed to be in the same directory as the .pkl file
        all_models = []
        for i in range(models.n_models):
            file_path, file_stem = os.path.split(filename)
            cur_model_file = os.path.join(file_path, models.model_filenames[i])

            curmodel = tensorflow.keras.models.load_model(cur_model_file)
            # curmodel.graph
            all_models.append(curmodel)

            # # GCN with custom layers
            # curmodel = keras.models.load_model(cur_model_file, custom_objects={'NeuralGraphHidden': NeuralGraphHidden,
            #                                                                     'NeuralGraphOutput': NeuralGraphOutput})
        models.all_models = all_models

    return( models )


########################################################################################################################
def exclude_warning_mols(all_mols,warning_mol_names):

    Nmols = len(all_mols)
    Nwarn = len(warning_mol_names)
    keep_mols = []

    for n in range(0,Nmols):
        exclude_curmol = False
        for w in range(0, Nwarn):
            if all_mols[n].mol_name.lower() == warning_mol_names[w].lower():
                exclude_curmol = True
                break

        if exclude_curmol == False:
            keep_mols.append(copy.deepcopy(all_mols[n]))

    return( keep_mols )


########################################################################################################################
# Wraps up a model building process: computes net performance stats, generates a detailed report, saves the model
def modeling_wrapup(models, data_int, data_test, report_prefix = None):

    cur_time = time.strftime('%H-%M-%S')
    cur_date = time.strftime('%m%d%Y')

    if report_prefix is not None:
        file_base = report_prefix + '_' + models.model_name + '_' + models.ml_type + '_' + models.feats_type + '_' + cur_date + '_' + cur_time
    else:
        file_base = models.model_name + '_' + models.ml_type + '_' + models.feats_type + '_' + cur_date + '_' + cur_time
    save_folder = os.path.join(models.io_paths.save_root, 'Results_' + file_base)
    os.mkdir(save_folder)
    report_name = os.path.join(save_folder, 'Report_' + file_base + '.csv')
    models.io_paths.model_save_file = os.path.join(save_folder, 'Model_' + file_base + '.pkl')

    X_int = copy.deepcopy(data_int.X)
    Y_int = copy.deepcopy(data_int.Y)
    X_test = copy.deepcopy(data_test.X)
    Y_test = copy.deepcopy(data_test.Y)


    # Write out the train/validation and test set feature matrices to file
    intmol_names = []
    [intmol_names.append(data_int.mols[i].mol_name) for i in range(len(data_int.mols))]
    int_features_file = os.path.join(save_folder,'FeaturesMat_TrainValid_' + file_base + '.csv')
    rep.write_feature_matrix_to_file(X_int, int_features_file, row_names=intmol_names)

    has_test_set = False
    if np.size(data_test.X, 0) > 0:
        has_test_set = True

    testmol_names = []
    if has_test_set:
        [testmol_names.append(data_test.mols[i].mol_name) for i in range(len(data_test.mols))]
        test_features_file = os.path.join(save_folder,'FeaturesMat_Test_' + file_base + '.csv')
        rep.write_feature_matrix_to_file(X_test, test_features_file, row_names=testmol_names)


    if models.pred_type == 'regression':

        # Generate ensemble prediction using the ensemble of all trained models
        preds_valid, preds_valid_std, all_preds_valid, preds_train, preds_train_std, all_preds_train = ml.models_predict(models, X_int, is_int_set=True)
        if has_test_set:
            preds_test, preds_test_std, all_preds_test = ml.models_predict(models, data_test.X)

        # Generate consensus predictions separately for training, validation, and test sets for each molecule
        models.valid_stats, valid_errors = ml.error_stats(preds_valid, Y_int, Y_pred_all=all_preds_valid, siglevel=models.siglevel)
        models.train_stats, train_errors = ml.error_stats(preds_train, Y_int, Y_pred_all=all_preds_train, siglevel=models.siglevel)

        if has_test_set:
            models.test_stats, test_errors = ml.error_stats(preds_test, Y_test, Y_pred_all=all_preds_test, siglevel=models.siglevel)


        print('\nSummary stats for %s model, %s endpoint across %d model building trials:' % (models.ml_type, models.model_name, models.n_models))
        print('Used %d total mols: %d for training models; %d used for validation; validation ratio = %.2f' % (models.n_valid + models.n_train, models.n_train, models.n_valid, models.valid_ratio))
        print('\tRsq of consensus prediction for training sets = %s' % (str([stats.rsq for stats in models.train_stats])))
        print('\tRsq of consensus prediction for validation sets = %s' % (str([stats.rsq for stats in models.valid_stats])))
        if has_test_set:
            print('\tRsq of consensus prediction for test set = %s' % (str([stats.rsq for stats in models.test_stats])))

        # Generate a performance summary report across all bagging trials
        try:
            rep.regression_training_report(models, report_name, data_int.mols, data_test.mols, X_int, Y_int, X_test, Y_test)
            wrote_report = True
        except RuntimeError:
            print('!!! Error writing detailed summary report to file: %s!!!' % (report_name))
            wrote_report = False

        # For data analysis purposes, put activities back to original scaling.  But keep a copy of scaled activities
        if models.scaling.scaling_set:
            # Scaled activity values and predictions
            scaled_int = copy.deepcopy(Y_int)
            scaled_preds_valid = copy.deepcopy(preds_valid)
            scaled_preds_valid_std = copy.deepcopy(preds_valid_std)
            scaled_preds_train = copy.deepcopy(preds_train)
            scaled_preds_train_std = copy.deepcopy(preds_train_std)

            if has_test_set:
                scaled_preds_test = copy.deepcopy(preds_test)
                scaled_preds_test_std = copy.deepcopy(preds_test_std)
                scaled_test = copy.deepcopy(Y_test)

            # Unscaled activity values and predictions
            unscaled_preds_valid = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_valid))
            unscaled_preds_valid_std = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_valid_std), mean_center=False)
            unscaled_preds_train = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_train))
            unscaled_preds_train_std = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_train_std), mean_center=False)
            unscaled_int = ml.invert_scaling(models.scaling, Y_int)

            if has_test_set:
                unscaled_preds_test = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_test))
                unscaled_preds_test_std = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_test_std), mean_center=False)
                unscaled_test = ml.invert_scaling(models.scaling, Y_test)

        # Generate and save some plots
        path_head, path_tail = os.path.split(report_name)
        base_file = path_tail.split('.')

        # Generate prediction vs. measurement plot- raw values
        for i in range(models.n_tasks):
            print('Generating plots for %s (task #%d of %d) ...' % (models.task_names[i], i+1, models.n_tasks))
            try:
                plt.figure(1)
                plt.clf()
                plt.scatter(unscaled_int[:,i], unscaled_preds_train[:,i], edgecolor='r', facecolor='None',label='Training Set')
                plt.errorbar(unscaled_int[:,i], unscaled_preds_train[:,i],yerr=unscaled_preds_train_std[:,i], c='r',fmt='None',ecolor='r',elinewidth=1, capsize=3,capthick=1)
                plt.scatter(unscaled_int[:,i], unscaled_preds_valid[:,i], edgecolor='g', facecolor='None', label='Validation Set')
                plt.errorbar(unscaled_int[:,i], unscaled_preds_valid[:,i], yerr=unscaled_preds_valid_std[:,i], c='g',fmt='None',ecolor='g',elinewidth=1, capsize=3,capthick=1)
                if has_test_set:
                    plt.scatter(unscaled_test[:,i], unscaled_preds_test[:,i], edgecolor='b', facecolor='None', label='Test Set')
                    plt.errorbar(unscaled_test[:,i], unscaled_preds_test[:,i], yerr=unscaled_preds_test_std[:,i], c='b',fmt='None', ecolor='b',elinewidth=1, capsize=3,capthick=1)

                if has_test_set:
                    min_act = np.min(np.array([np.min(unscaled_int[:,i]),np.min(unscaled_test[:,i])]))
                    max_act = np.max(np.array([np.max(unscaled_int[:,i]),np.max(unscaled_test[:,i])]))
                else:
                    min_act = np.min(unscaled_int[:,i])
                    max_act = np.max(unscaled_int[:,i])
                delta_act = max_act - min_act
                min_act -= 0.05*delta_act
                max_act += 0.05*delta_act
                ideal_line = np.linspace(min_act, max_act, 10)
                plt.plot(ideal_line,ideal_line,c='k')
                plt.xlabel('Raw Measured Activity')
                plt.ylabel('Raw Predicted Activity')
                plt.grid()
                plt.legend()
                if has_test_set:
                    plt.title('RAW Pred vs. Meas Acts in Task %s\nRsq (train, valid, test) = (%.3f, %.3f, %.3f)' % (models.task_names[i], models.train_stats[i].rsq,models.valid_stats[i].rsq,models.test_stats[i].rsq))
                else:
                    plt.title('RAW Pred vs. Meas Acts in Task %s\nRsq (train, valid) = (%.3f, %.3f)' % (models.task_names[i], models.train_stats[i].rsq,models.valid_stats[i].rsq))
                plt.show(block=False)
                plt.savefig(os.path.join(path_head,'Unscaled_PredVsMeasActs_Task'+ models.task_names[i] + '_' + base_file[0] + '.png'), dpi=480 )

                # Generate prediction vs. measurement plot- scaled values
                if models.scaling.scaling_set == True and models.scaling.method.lower() != 'none':
                    plt.figure(2)
                    plt.clf()
                    plt.scatter(scaled_int[:,i], scaled_preds_train[i], edgecolor='r', facecolor='None',label='Training Set')
                    plt.errorbar(scaled_int[:,i], scaled_preds_train[i],yerr=scaled_preds_train_std[i], c='r',fmt='None',ecolor='r',elinewidth=1, capsize=3,capthick=1)
                    plt.scatter(scaled_int[:,i], scaled_preds_valid[i], edgecolor='g', facecolor='None', label='Validation Set')
                    plt.errorbar(scaled_int[:,i], scaled_preds_valid[i], yerr=scaled_preds_valid_std[i], c='g',fmt='None',ecolor='g',elinewidth=1, capsize=3,capthick=1)
                    if has_test_set:
                        plt.scatter(scaled_test[:,i], scaled_preds_test[i], edgecolor='b', facecolor='None', label='Test Set')
                        plt.errorbar(scaled_test[:,i], scaled_preds_test[i], yerr=scaled_preds_test_std[i], c='b',fmt='None', ecolor='b',elinewidth=1, capsize=3,capthick=1)

                    if has_test_set:
                        min_act = np.min(np.array([np.min(scaled_int[:,i]),np.min(scaled_test[:,i])]))
                        max_act = np.max(np.array([np.max(scaled_int[:,i]),np.max(scaled_test[:,i])]))
                    else:
                        min_act = np.min(scaled_int[:,i])
                        max_act = np.max(scaled_int[:,i])
                    delta_act = max_act - min_act
                    min_act -= 0.05*delta_act
                    max_act += 0.05*delta_act
                    ideal_line = np.linspace(min_act, max_act, 10)
                    plt.plot(ideal_line,ideal_line,c='k')
                    plt.xlabel('%s Scaled Measured Activity' % (models.scaling.method))
                    plt.ylabel('%s Scaled Predicted Activity' % (models.scaling.method))
                    plt.grid()
                    plt.legend()
                    if has_test_set:
                        plt.title('%s Scaled Pred vs. Meas Acts in Task %s\nRsq (train, valid, test) = (%.3f, %.3f, %.3f)' % (models.scaling.method, models.task_names[i], models.train_stats[i].rsq,models.valid_stats[i].rsq,models.test_stats[i].rsq))
                    else:
                        plt.title('%s Scaled Pred vs. Meas Acts in Task %s\nRsq (train, valid) = (%.3f, %.3f)' % (models.scaling.method, models.task_names[i], models.train_stats[i].rsq,models.valid_stats[i].rsq))
                    plt.show(block=False)
                    plt.savefig(os.path.join(path_head,'Scaled_PredVsMeasActs_Task'+ models.task_names[i] + '_' + base_file[0] + '.png'), dpi=480 )


                # Generate plot that facilitates the identification of outlier predictions
                plt.figure(3)
                plt.clf()
                notnanidx_errors = np.where(np.isnan(train_errors[i]) == False)[0]
                notnanidx_std = np.where(np.isnan(preds_train_std[i]) == False)[0]
                train_notnanidx = np.intersect1d(notnanidx_errors, notnanidx_std)

                notnanidx_errors = np.where(np.isnan(valid_errors[i]) == False)[0]
                notnanidx_std = np.where(np.isnan(preds_valid_std[i]) == False)[0]
                valid_notnanidx = np.intersect1d(notnanidx_errors, notnanidx_std)

                notnanidx_errors = np.where(np.isnan(test_errors[i]) == False)[0]
                notnanidx_std = np.where(np.isnan(preds_test_std[i]) == False)[0]
                test_notnanidx = np.intersect1d(notnanidx_errors, notnanidx_std)

                plt.scatter(np.abs(train_errors[i][train_notnanidx]), preds_train_std[i][train_notnanidx],edgecolor='r',facecolor='None',label='Training Set')
                plt.scatter(np.abs(valid_errors[i][valid_notnanidx]), preds_valid_std[i][valid_notnanidx], edgecolor='g', facecolor='None', label='Validation Set')
                if has_test_set:
                    plt.scatter(np.abs(test_errors[i][test_notnanidx]), preds_test_std[i][test_notnanidx], edgecolor='b', facecolor='None', label='Test Set')
                    for j in range(np.size(test_notnanidx)):
                        plt.text(np.abs(test_errors[i][test_notnanidx[j]]), preds_test_std[i][test_notnanidx[j]], str(test_notnanidx[j]), color='b')

                for j in range(np.size(train_notnanidx)):
                    plt.text(np.abs(train_errors[i][train_notnanidx[j]]), preds_train_std[i][train_notnanidx[j]],str(train_notnanidx[j]),color='r')
                for j in range(np.size(valid_notnanidx)):
                    plt.text(np.abs(valid_errors[i][valid_notnanidx[j]]), preds_valid_std[i][valid_notnanidx[j]], str(valid_notnanidx[j]), color='g')

                plt.xlabel('Median Absolute Pred Error')
                plt.ylabel('Std Dev of Predictions')
                plt.grid()
                plt.legend()
                if has_test_set:
                    plt.title('AD Stats in Task %s\nRsq (train, valid, test) = (%.3f, %.3f, %.3f)' % (models.task_names[i], models.train_stats[i].rsq,models.valid_stats[i].rsq,models.test_stats[i].rsq))
                else:
                    plt.title('AD Stats in Task %s\nRsq (train, valid) = (%.3f, %.3f)' % (models.task_names[i], models.train_stats[i].rsq,models.valid_stats[i].rsq))
                plt.show(block=False)
                plt.savefig(os.path.join(path_head, 'ADPlot_Task'+ models.task_names[i] + '_' + base_file[0] + '.png'), dpi=480)

            except:
                print('Error generating regression plots for task %d of %d (%s)!!!' % (i+1,models.n_tasks, models.task_names[i]))



    elif models.pred_type == 'classification':
        # if has_test_set > 0:
        #     Y_all = np.concatenate((Y_int,Y_test),axis=0)
        # else:
        #     Y_all = copy.deepcopy(Y_int)

        if has_test_set:
            test_probs, test_prob_std, test_class_preds = ml.models_predict_class(models, X_test)
            models.test_stats = ml.multitask_perf_stats_class(test_class_preds, Y_test, models.n_class, pred_probs=test_probs)

        valid_probs, valid_prob_std, valid_class_preds, train_probs, train_prob_std, train_class_preds = \
            ml.models_predict_class(models, X_int, is_int_set=True)

        models.train_stats = ml.multitask_perf_stats_class(train_class_preds, Y_int, models.n_class,pred_probs=train_probs)
        models.valid_stats = ml.multitask_perf_stats_class(valid_class_preds, Y_int, models.n_class,pred_probs=valid_probs)

        try:
            rep.classification_training_report(models, report_name, data_int.mols, data_test.mols, X_int, Y_int, X_test, Y_test)
            wrote_report = True
        except RuntimeError:
            print('!!! Error writing detailed summary report to file: %s!!!' % (report_name))
            wrote_report = False


    # Calculate FDR (false discovery rate)
    #TODO: figure out what (if anything) we want to do with this statistic
    # fdr_tasks = ml.false_discovery_rate(X_int, Y_int)

    # Calculate feature significance, and write to file(s)
    # sigfeatsidx_tasks = ml.feature_significance_univariate(models, X_int, n_sig=100)
    # print('\nCalculating most significant features...')
    # n_files_written = rep.write_sig_features_files(report_name, models, X_int, Y_int, X_test, Y_test)


    ####################################################################################################################
    # Save predictive models and summary stats for use in a later session

    try:
        print('\n\nSaving model to %s' % (models.io_paths.model_save_file))
        save_model(models, models.io_paths.model_save_file)
    except RuntimeError:
        print('Error writing model to file!!!')

    print('\n# molecules total = %d; # internal = %d;  # train = %d;  # valid = %d;  # test = %d' % (
    models.n_grandtotal, models.n_int, models.n_train, models.n_valid, models.n_test))

    # Move all files used to build this model to the current results folder
    # replay_dir = os.path.join(save_folder,'')

    return( models, wrote_report, save_folder )


########################################################################################################################
# Performs the model stress tests as specified in the StressTest_Control.xml file
def model_stress_test(models, stset, config_file, report_ext=None):

    save_root = models.io_paths.save_root
    all_model_save_files = []

    models, data_int_base, data_test_base = load_data_int_test(models)
    base_model = copy.deepcopy(models)

    # Run the "base case" model, generate performance stats, and write reports
    model_labels = ['BaseCase']
    base_model = perform_modeling_st(base_model, data_int_base, data_test_base, model_labels[0], config_file)
    dflt_model = base_model
    all_model_save_files.append(base_model.io_paths.model_save_file)
    print('\n')
    for i in range(3):
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    print('Finished BaseCase')
    for i in range(3):
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    print('\n')

    # ***************************** Model Stress Part I: feature sensitivity ***********************************
    if dflt_model.feats.feats_type == 'ecfp':
        all_model_save_files, model_labels = feats.ecfp_sensitivity_mst(all_model_save_files, dflt_model, model_labels, stset, data_int_base, data_test_base, config_file)


    # ********************  Model Stress Part II: ML hyperparameter sensitivity  *********************
    nhp = 1
    if dflt_model.ml_type == 'pls':
        all_model_save_files, model_labels = pls_sensitivity_mst(all_model_save_files, dflt_model, model_labels, stset, data_int_base, data_test_base, config_file)
    elif dflt_model.ml_type == 'ann':
        all_model_save_files, model_labels = ann_sensitivity_mst(all_model_save_files, dflt_model, model_labels, stset, data_int_base, data_test_base, config_file)
    elif dflt_model.ml_type == 'knn':
        all_model_save_files, model_labels = knn_sensitivity_mst(all_model_save_files, dflt_model, model_labels, stset, data_int_base, data_test_base, config_file)
    elif dflt_model.ml_type == 'xgboost':
        all_model_save_files, model_labels = xgboost_sensitivity_mst(all_model_save_files, dflt_model, model_labels, stset, data_int_base, data_test_base, config_file)


    # ******************************* Model Stress Test Part III: test set sensitivity ******************************
    n_test_sets = 1
    startidx = 0
    if stset.test_set_files is not None:
        startidx = 1

    for i in range(startidx, stset.n_test_sets):
        n_test_sets += 1
        model_labels.append('TestSet' + str(n_test_sets) )
        # cur_model = copy.deepcopy(dflt_model)
        cur_model = load_model(all_model_save_files[0])
        if stset.test_set_files is not None:
            cur_model.io_paths.test_set_file = stset.test_set_files[i]
            cur_model.io_paths.activity_file = stset.activity_files[i]

        cur_model, curdata_int, curdata_test = load_data_int_test(cur_model)
        cur_model = perform_modeling_st(cur_model, curdata_int, curdata_test, model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    # ******************** Model Stress Test Part IV: predictor / response noise sensitivity ************************
    nnoise = 1
    # Only do X scrambling if predictors are binary.
    n_unique_feats = np.unique(data_int_base.X)
    binary_features = False
    if np.size(n_unique_feats) == 2:
        binary_features = True
    if binary_features:
        for i in range(0, len(stset.Xnoise_fracs)):
            nnoise += 1
            model_labels.append('Xnoise' + str(stset.Xnoise_fracs[i]))
            # cur_model = copy.deepcopy(dflt_model)
            cur_model = load_model(all_model_save_files[0])
            X_int_noise, scrambleidx = ml.scramble_discrete_predictors(copy.deepcopy(data_int_base.X), Npredvals=2, scramble_frac=stset.Xnoise_fracs[i])
            curdata_int = copy.deepcopy(data_int_base)
            curdata_int.X = copy.deepcopy(X_int_noise)

            cur_model = perform_modeling_st(cur_model, curdata_int, data_test_base, model_labels[-1], config_file)
            all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    # Adding noise to activities- need to handle differently for classification vs. regression models
    for i in range(0, len(stset.Ynoise_fracs)):
        nnoise += 1
        model_labels.append('Ynoise' + str(stset.Ynoise_fracs[i]))
        # cur_model = copy.deepcopy(dflt_model)
        cur_model = load_model(all_model_save_files[0])
        cur_int_mols = copy.deepcopy(data_int_base.mols)
        curdata_int = copy.deepcopy(data_int_base)
        if cur_model.pred_type == 'classification':
            # In classification modeling, only a fraction of class labels get scrambled
            Y_int_noise, scrambleidx = ml.scramble_class_labels(copy.deepcopy(data_int_base.Y), models.n_class, scramble_frac = stset.Ynoise_fracs[i])
            for t in range(models.n_tasks):
                for j in range(0, np.size(scrambleidx)):
                    cur_int_mols[scrambleidx[j]].acts[t] = copy.deepcopy(Y_int_noise[scrambleidx[j],t])

        elif cur_model.pred_type == 'regression':
            # In regression modeling, all activities get some noise added
            Y_int_noise = ml.noise_addition_regression(copy.deepcopy(data_int_base.Y), noise_frac=stset.Ynoise_fracs[i])
            for j in range(0, len(cur_int_mols)):
                cur_int_mols[j].acts = copy.deepcopy(Y_int_noise[j])
        curdata_int.mols = copy.deepcopy(cur_int_mols)
        curdata_int.Y = copy.deepcopy(Y_int_noise)

        cur_model = perform_modeling_st(cur_model, curdata_int, data_test_base, model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    # Write model stress test summary report
    if report_ext is not None:
        model_st_report_name = os.path.join(save_root, 'ModelStressTestSummaryReport_' + report_ext + '.csv')
    else:
        model_st_report_name = os.path.join(save_root,'ModelStressTestSummaryReport.csv')
    rep.model_stress_test_report(all_model_save_files, model_labels, model_st_report_name)

    return( model_st_report_name )




########################################################################################################################
# Performs model stress test sensitivity while varying PLS hyperparameters
def pls_sensitivity_mst(all_model_save_files, base_model, model_labels, stset, data_int_base, data_test_base, config_file):
    nhp = 1
    for i in range(0, len(stset.n_comp)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.n_comp = copy.deepcopy(stset.n_comp[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    for i in range(0, len(stset.n_iters)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.n_iters = copy.deepcopy(stset.n_iters[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    return(all_model_save_files, model_labels)


########################################################################################################################
# Performs model stress test while varying ANN hyperparameters
def ann_sensitivity_mst(all_model_save_files, base_model, model_labels, stset, data_int_base, data_test_base, config_file):

    nhp = 1

    for i in range(0, len(stset.n_epochs)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.n_epochs = copy.deepcopy(stset.n_epochs[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    for i in range(0, len(stset.n_hidden)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.n_hidden = copy.deepcopy(stset.n_hidden[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.n_neur)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.n_neur = copy.deepcopy(stset.n_neur[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    for i in range(0, len(stset.learning_rate)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.learning_rate = copy.deepcopy(stset.learning_rate[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.learning_rule)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.learning_rule = copy.deepcopy(stset.learning_rule[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.learning_momentum)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.learning_momentum = copy.deepcopy(stset.learning_momentum[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.n_iters)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.n_iters = copy.deepcopy(stset.n_iters[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.dropout)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.dropout = copy.deepcopy(stset.dropout[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.batch_size)):
        # cur_model = copy.deepcopy(base_model)
        cur_model = load_model(all_model_save_files[0])
        cur_model.hp.batch_size = copy.deepcopy(stset.batch_size[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    return(all_model_save_files, model_labels)


########################################################################################################################
# Performs model stress test sensitivity while varying XGBoost hyperparameters
def knn_sensitivity_mst(all_model_save_files, base_model, model_labels, stset, data_int_base, data_test_base, config_file):

    nhp = 1
    for i in range(0, len(stset.k)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.k = copy.deepcopy(stset.k[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.distance_type)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.distance_type = copy.deepcopy(stset.distance_type[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.weight_type)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.weight_type = copy.deepcopy(stset.weight_type[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    return(all_model_save_files, model_labels)


########################################################################################################################
# Performs model stress test sensitivity while varying XGBoost hyperparameters
def xgboost_sensitivity_mst(all_model_save_files, base_model, model_labels, stset, data_int_base, data_test_base, config_file):

    nhp = 1
    for i in range(0, len(stset.max_depth)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.max_depth = copy.deepcopy(stset.max_depth[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    for i in range(0, len(stset.learning_rate)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.learning_rate = copy.deepcopy(stset.learning_rate[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    for i in range(0, len(stset.n_estimators)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.n_estimators = copy.deepcopy(stset.n_estimators[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))


    for i in range(0, len(stset.objective)):
        cur_model = copy.deepcopy(base_model)
        cur_model.hp.objective = copy.deepcopy(stset.objective[i])
        nhp += 1
        model_labels.append('HP' + str(nhp))

        cur_model = perform_modeling_st(cur_model, copy.deepcopy(data_int_base), copy.deepcopy(data_test_base),
                                             model_labels[-1], config_file)
        all_model_save_files.append(copy.deepcopy(cur_model.io_paths.model_save_file))

    return(all_model_save_files, model_labels)



########################################################################################################################
# Trains a model
def perform_modeling_st(models, data_int, data_test, annotation_string, config_file):

    # Train the model
    models = train_bagging(models, data_int, process_type='serial')

    # Compute the model performance statistics and generate the summary report
    models, wrote_report, save_folder = modeling_wrapup(models, data_int, data_test, report_prefix=annotation_string)

    print('\n\nCreating ModelArchive:')
    archive_folder = os.path.join(save_folder, 'ModelArchive')
    os.mkdir(archive_folder)
    archived_files = rep.modeling_file_archive(archive_folder, models, config_file, int_mols=data_int.mols, test_mols=data_test.mols)
    print('ModelArchive finished')

    return( models )



########################################################################################################################
# Eliminates entries from mols0 that also appear in mols1
def deduplicate_mols(mols0, mols1, eliminate_conflicting_acts = True, eliminate_duplicates = False):

    n0 = len(mols0)
    n1 = len(mols1)
    n_dup = 0 # counter of number of duplicate molecules
    dedupmols0 = []
    dedupmols1 = []
    keepidx1 = np.ones(n1, dtype=np.bool)

    for i in range(0, n0):
        duplicated = False
        activity_conflict = False
        eliminate_duplicate = False
        for j in range(0, n1):
            if mols0[i].mol_name.lower() == mols1[j].mol_name.lower():
                duplicated = True
                n_dup += 1
                if eliminate_duplicates:
                    eliminate_duplicate = True
                    print(' !!! Eliminating duplicate molecule: %s !!!' % mols0[i].mol_name)
                else:
                    print('\n\n!!! Duplicate molecule %s detected in training and test sets !!!' % (mols0[i].mol_name))
                if eliminate_conflicting_acts:
                    if np.array_equal(mols0[i].acts, mols1[j].acts) == False:
                        activity_conflict = True
                        keepidx1[j] = False
                        print('Activity conflict identified in molecule %s.  Molecule eliminated from training, test sets!!!!' % (mols0[i].mol_name))
                break
        if activity_conflict is False and eliminate_duplicate is False:
            dedupmols0.append(copy.deepcopy(mols0[i]))

    # If so desired, keep only the desired molecules from list 1
    for i in range(n1):
        if keepidx1[i] == True:
            dedupmols1.append(copy.deepcopy(mols1[i]))

    if n_dup > 0:
        print('\n\n')

    return( dedupmols0, dedupmols1)


########################################################################################################################
# Loads molecules and activities associated with those molecules
def load_data_multiacts(models, activity_file, exclude_nans = True):

    io = models.io_paths

    # Read in "activities" from the activities file
    multiacts, mol_names, task_names = read_multiacts_file(activity_file, models.pred_type, excludes_file=io.excludes_file)
    n_tasks = len(task_names)
    n_mols = len(mol_names)

    # Determine if the machine learning method can tolerate missing activities for some tasks
    allow_some_missing_tasks = True
    if models.ml_type == 'pls' or models.ml_type == 'knn':
        allow_some_missing_tasks = False

    # Need to exclude molecules where activities are missing for all tasks, IF we are providing activities
    mol_names_keep = copy.deepcopy(mol_names)
    if n_tasks > 0:
        mol_names_keep = []
        keepidx  =[]
        for i in range(n_mols):
            curmissingidx = np.where(np.isnan(multiacts[i,:])==True)[0]
            curn_missing = int(np.size(curmissingidx))
            if curn_missing == n_tasks or (allow_some_missing_tasks==False and curn_missing > 0):
                print('\tMolecule %s is missing too many activities (%d)!' % (mol_names[i], curn_missing))
                continue
            mol_names_keep.append(copy.deepcopy(mol_names[i]))
            keepidx.append(copy.deepcopy(i))
        keepidx = np.array(keepidx)
        n_mols = int(np.size(keepidx))
        mol_names = copy.deepcopy(mol_names_keep)
        multiacts = multiacts[keepidx,:].reshape((n_mols,n_tasks))


        # If desired, eliminate molecules with NaNs for some activities (i.e. missing values).  Can't handle missing vales for now
        if exclude_nans:
            excludeidx = np.array([])
            for i in range(n_tasks):
                curexcludeidx = np.where(np.isnan(multiacts[:,i]) == True)[0]
                excludeidx = np.concatenate((excludeidx, curexcludeidx))

            excludeidx = np.int32(np.unique(excludeidx))
            keepidx = np.setdiff1d(np.arange(n_mols), excludeidx)
            multiacts = multiacts[keepidx,:]
            mol_names_keep = []
            for i in range(np.size(keepidx)):
                mol_names_keep.append(mol_names[keepidx[i]])


    ################################################################
    # Featurize the molecular information

    if models.feats_type == 'ecfp':
        # all_mols = feats.featurize_ecfp_molfiles(models.feats,mol_names_keep, io.mol_root)
        all_mols = feats.load_rdmols_from_molfiles(mol_names_keep, io.mol_root)


    # Find all molecules for which we have predictor and response data (and where we don't have both)
    match_mols, noact_mols, nopred_acts, nopred_acts_names = predictor_response_match(multiacts, mol_names_keep, all_mols)

    return( match_mols, task_names, n_tasks )


########################################################################################################################
def exclude_nans_from_acts(acts, mol_names = None):

    nact_types = int(np.size(acts,1))
    nmols = int(np.size(acts,0))

    excludeidx = np.array([])
    for i in range(nact_types):
        curexcludeidx = np.where(np.isnan(acts[:,i]) == True)[0]
        excludeidx = np.concatenate((excludeidx, curexcludeidx))

    excludeidx = np.int32(np.unique(excludeidx))
    keepidx = np.setdiff1d(np.arange(nmols), excludeidx)
    acts_keep = acts[keepidx,:]

    if mol_names is None:
        return(acts_keep)

    mol_names_keep = []
    for i in range(np.size(keepidx)):
        mol_names_keep.append(copy.deepcopy(mol_names[keepidx[i]]))

    return( acts_keep, mol_names_keep)


########################################################################################################################
# Returns the number of unique classes (per task) contained in a list of molecules
def nclass_from_mols(mols):
    n_mols = len(mols)
    if n_mols == 0:
        return(None, None)

    n_tasks = int(np.size(mols[0].acts))
    all_acts = np.zeros((n_mols,n_tasks))
    for i in range(n_mols):
        all_acts[i,:] = np.squeeze(mols[i].acts)

    n_class = np.zeros(n_tasks, dtype=np.int32)
    n_missing = np.zeros(n_tasks, dtype=np.int32)
    ns_class = []
    for t in range(0, n_tasks):
        cur_acts = all_acts[:, t]
        notnanidx = np.where(np.isnan(cur_acts) == False)[0]
        nanidx = np.where(np.isnan(cur_acts) == True)[0]
        n_missing[t] = int(np.size(nanidx))
        cur_acts = cur_acts[notnanidx]

        uclasses, ucts = np.unique(cur_acts, return_counts=True)

        n_class[t] = np.size(uclasses)
        ns_class.append(ucts)

    return( n_class, ns_class, n_missing)



########################################################################################################################
# Checks the geometry of atoms in a molecule for anomalously big / small distances, and for unoptimized molecules
# pos3d is a N x 3 array of atom coordinates for each atom in a molecule
def molecule_geometry_check(pos3d, min_dist = 1.0, max_dist = 40.0, mol_name = ''):
    warnings = []
    n_atoms = np.size(pos3d,0)

    if n_atoms == 0:
        warnings.append('!!! molecule %s has no atoms !!!' % (mol_name))
        print(warnings[-1])

    if np.all(pos3d[:,2] == 0.0):
        warnings.append('molecule %s appears to be unoptimized' % (mol_name))
        print(warnings[-1])

    for i in range(n_atoms):
        for j in range(i+1,n_atoms):
            curdist = np.linalg.norm(pos3d[i,:] - pos3d[j,:])

            if curdist < min_dist:
                warnings.append('!!! in molecule %s, very small distance (%.3f Angstroms) between atoms %d and %d !!!' % (mol_name,curdist,i,j))
                print(warnings[-1])

            if curdist > max_dist:
                warnings.append('!!! in molecule %s, very large distance (%.3f Angstroms) between atoms %d and %d !!!' % (mol_name,curdist,i,j))
                print(warnings[-1])

    if len(warnings) > 0:
        print('\n')

    return( warnings )


########################################################################################################################
########################################################################################################################
########################################################################################################################
# Base class for storing model stress test settings
class MODEL_STRESS_TEST_SETTINGS():
    def __init__(self, models):

        self.ml_type = copy.deepcopy(models.ml_type)
        self.feats_type = copy.deepcopy(models.feats_type)
        self.pred_type = copy.deepcopy(models.pred_type)

        self.test_set_files = None
        self.activity_files = None

        self.n_test_sets = None
        self.Xnoise_fracs = None
        self.Ynoise_fracs = None

        if models.feats_type == 'ecfp':
            self.n_bits = None
            self.fp_radius = None
            self.use_fcfp = None

        if models.ml_type == 'pls':
            self.n_comp = None
            self.n_iters = None
        elif models.ml_type == 'ann':
            self.n_neur = None
            self.n_hidden = None
            self.n_epochs = None
            self.act_fn = None
            self.learning_rate = None
            self.learning_rule = None
            self.learning_momentum = None
            self.n_iters = None
            self.dropout = None
            self.batch_size = None
        elif models.ml_type == 'knn':
            self.k = None
            self.distance_type = None
            self.weight_type = None
        elif models.ml_type == 'xgboost':
            self.max_depth = None
            self.learning_rate = None
            self.n_estimators = None
            self.objective = None


########################################################################################################################
#  Base class for storing molecular information
class MOL():
    def __init__(self):
        self.atom_seq = []
        self.atom_types_occs = [] # atom types used in occupied bins
        self.elem = []
        self.x3d = []
        self.n_atoms = 0
        self.n_atom_types = 0 # num different types of atoms in molecule
        self.mol_name = None
        self.acts = None  # activities
        self.feats_vec = None # vector of molecule converted into a feature.  Depends on type of features being used
        self.smiles = None # SMILES string representation of molecule.  Not used for all feature types
        self.rdmol = None # representation of a molecule in RDKit compatible format


########################################################################################################################
#  Class for storing prediction settings and information
class PREDICTION_SETTINGS():
    def __init__(self,pred_type='classification'):
        if pred_type == 'classification':
            self.bin_class_thresh = 0.5 # whether or not to perform upsample balancing in a classification model
            self.downsample_balancing = False  # whether or not to perform downsample balancing in a classification model
            self.upsample_balancing = False  # whether or not to perform upsample balancing in a classification model
            self.training_weight_type = 'proportional'  # equal, proportional, exponential, cubic; balancing weighting for smaller classes
            self.exclude_extra_test = False # if performing downsample balancing, whether or not to excldue excess samples from the test set
            self.n_class = None # number of classes, per task.  TBD at training time
            self.n_tasks = None # number of separate tasks to be modeled.  TBD at training time

        elif pred_type == 'regression':
            self.scaling = ml.SCALE_PARAMS()  # number of classes to generate predictions, for each task



# ######################################################################################################################
#  Class for storing data
class RAW_DATA( ):
    def __init__(self, n_class = None):
        self.n_tasks = None
        self.mols = None
        self.X = None # feature information
        self.Y = None # activity / class label information
        self.exclude_mols = None # molecules from the "excludes" file
        self.extra_mols = None # molecules excluded for any other reason (i.e. for class balancing reasons)


# ######################################################################################################################
#  Base class for modeling using bagging or various other ensemble methods
class MODELS( ):
    def __init__(self, settings):
        self.version = '3.0.0' # major, minor, bug fix rev to this class
        self.model_name = copy.deepcopy(settings.model_name)
        self.random_seed = copy.deepcopy(settings.random_seed)
        self.feats_type = copy.deepcopy(settings.feats_type.lower()) # ECFP, graph convolutional, ... descriptors
        self.pred_type  = copy.deepcopy(settings.pred_type.lower()) # what type of prediction- classification or regression?
        self.ml_type    = copy.deepcopy(settings.ml_type.lower()) # Which type of machine learning method- PLS, ANN, XGBoost, etc.
        self.hp = copy.deepcopy(settings.hp)
        self.ensemble_type = copy.deepcopy(settings.ensemble_type)
        self.model_filenames = None
        self.custom_objects = None # Custom layers / objective functions / etc. for Keras model training, saving, and reloading

        self.exclude_sparse_mols = copy.deepcopy(settings.exclude_sparse_mols)
        self.sparse_frac = copy.deepcopy(settings.sparse_frac)

        self.siglevel = settings.siglevel

        self.io_paths = copy.deepcopy(settings.io_paths)

        self.preds = copy.deepcopy(settings.preds)

        self.task_names = None # name of the "tasks" (i.e. activities) to model
        self.n_tasks = 0

        self.feats = copy.deepcopy(settings.feats)
        self.graph = None

        self.all_models = [] # all models built as part of the training process
        self.n_models = copy.deepcopy(settings.n_models)

        self.test_stats = ml.PERF_STATS(settings.pred_type)
        self.valid_stats = ml.PERF_STATS(settings.pred_type)
        self.train_stats = ml.PERF_STATS(settings.pred_type)

        # number of molecules in the training, validation, internal, test set(s) for each task
        self.n_grandtotal = None # total number of molecules for which we have at least 1 activity (in at least one task)
        self.n_totalintask = None # total number of molecules per task for which we have data in training, validation, test sets
        self.n_train = None
        self.n_valid = None
        self.n_int = None
        self.n_test = None
        self.ns_class = None
        self.n_missing = None

        self.valid_ratio = copy.deepcopy(settings.valid_ratio)
        self.test_ratio = copy.deepcopy(settings.test_ratio)

        # Number of features (predictors) in each task
        self.n_feats = None

        # Number of classes to model per task
        self.n_class = None

        self.int_set = None
        self.int_names = None
        self.valid_sets = None
        self.valid_names = None
        self.test_set = None
        self.test_names = None
        self.train_sets = None
        self.train_names = None
