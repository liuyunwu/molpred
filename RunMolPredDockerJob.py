# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

########################################################################################################################
# This is a script that runs INSIDE a Docker container.  It runs a MolPred job that is defined by the config .xml file
#   that gets passed in with the zipped payload folder
print('\033[1;45m\nRunning MolPred job in Docker container\n\n\033[1;m')

import os, glob
import MolPred as mp
import argparse
import shutil
import boto3
import yagmail

# Kick off the job from command line arguments
parser = argparse.ArgumentParser(description='Runs a MolPed "job" (train single model, model stress test, or model '
                                             'prediction), given the name of a zipped job folder to run and path to move results to')
parser.add_argument('job_folder')
parser.add_argument('--local_dst_file', action='store', dest='local_dst_file', default=None)
parser.add_argument('--cloud_src_dir', action='store', dest='cloud_src_dir', default=None)
parser.add_argument('--cloud_dst_dir', action='store', dest='cloud_dst_dir', default=None)
parser.add_argument('--recipient', action='store', dest='recipient', default=None)

args = parser.parse_args()
zipped_job_folder = args.job_folder

molpred_root = '/~/MolPred'  # This hard-coded root is set in the Docker image when it is built

# Determine if this is a cloud or local job
cloud_job = False
if args.cloud_src_dir is not None and args.cloud_dst_dir is not None:
    cloud_job = True
    print('Running MolPred job in the cloud')
elif args.cloud_src_dir is None and args.cloud_dst_dir is not None:
    exit('Specified cloud_dst_dir, but not cloud_src_dir.  Can''t run cloud job!!!')
elif args.cloud_src_dir is not None and args.cloud_dst_dir is None:
    exit('Specified cloud_src_dir, but not cloud_dst_dir.  Can''t run cloud job!!!')


# If this is a cloud job, need to download job folder from S3 bucket
if cloud_job is True:
    s3 = boto3.resource('s3')

    print('Downloading file %s from S3 bucket: %s' % (zipped_job_folder, args.cloud_src_dir))
    s3.meta.client.download_file('molpredjobs','incoming_jobs/'+zipped_job_folder,os.path.join(molpred_root,zipped_job_folder))
    zipped_job_folder = os.path.join(molpred_root, zipped_job_folder)


print('Loading job files from: %s' % (zipped_job_folder))

if args.cloud_dst_dir is not None:
    print('After Docker job completes, results will be moved to: %s' % (args.cloud_dst_dir))


file_root, basefile = os.path.split(zipped_job_folder)

# debug messages...
# cwd = os.getcwd()
# print('Current working directory and contents: %s' % (cwd))
# print(os.listdir())

job_folder, file_ext = os.path.splitext(zipped_job_folder)
if os.path.exists(zipped_job_folder) is False:
    exit('\n\n\nCannot find .zip job folder: %s !!!\n\n\n' % (zipped_job_folder))
try:
    shutil.unpack_archive(zipped_job_folder)
except:
    exit('Cannot extract Docker job payload: %s' % (zipped_job_folder))

# The config file is the xml file stored in the job folder.  Need to find this to run the Docker job
tokens = job_folder.split('docker_payload_')
match_str = tokens[1]
config_file = glob.glob('*.xml')
print(os.listdir())
# print('config_file:  %s' % (config_file))  # debug message
if len(config_file) > 1:
    matchidx = -1
    for i in range(len(config_file)):
        if config_file[i].__contains__(match_str):
            matchidx = i
            break
    if matchidx < 0:
        exit('Cannot find any .xml config file in Docker payload container %s!!!' % (zipped_job_folder))
    config_file = config_file[matchidx]
else:
    config_file = config_file[0]


########################################################################################################################
# Run the job
print('\nRunning Docker job from config file:  %s' % (config_file))
results_root = mp.run_job(config_file)


########################################################################################################################
# Compress results folder and push it to the destination directory
_, unzipped_results_root = os.path.split(results_root)
results_root = os.path.join(molpred_root, unzipped_results_root)
os.chdir(results_root)
zipped_results_folder = shutil.make_archive(results_root, 'zip', root_dir=results_root)

if os.path.exists(results_root) is False:
    print('Results folder to be zipped %s DOES NOT exist!' % (results_root))

if os.path.exists(zipped_results_folder) is False:
    print('.zip results folder %s DOES NOT exist!' % (zipped_results_folder))

if args.local_dst_file is not None:
    new_results_name = os.path.join(molpred_root, args.local_dst_file)
    print('Renaming results folder from: %s  to: %s' % (zipped_results_folder, new_results_name))
    os.rename(zipped_results_folder, new_results_name)

# If this is a cloud job, need to upload the completed job to the S3 bucket
if cloud_job is True:
    s3 = boto3.resource('s3')

    print('Uploading job results file %s to S3 bucket: %s' % (zipped_results_folder, args.cloud_dst_dir))
    zipped_root, base_zipped_file = os.path.split(zipped_results_folder)
    s3.meta.client.upload_file(zipped_results_folder,'molpredjobs','completed_jobs/'+base_zipped_file)
    zipped_job_folder = os.path.join(molpred_root, zipped_results_folder)

    # Email job complete message to recipient, if one is specified
    if args.recipient is not None:
        subject = "AWS Batch Job Complete!"
        body = "MolPred job complete.  You can download it from:\n" + os.path.join(args.cloud_dst_dir, base_zipped_file)

        try:
            yag = yagmail.SMTP("temp@gmail.com", "")
            yag.send(
                to=args.recipient,
                subject=subject,
                contents=body,
                # attachments=filename,
            )
        except:
            print('Error sending email to: %s !!!\n\n' % (args.recipient))

print('\033[1;45m\n\tDocker Run COMPLETE\n\n\033[1;m')
