# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# ######################################################################################################################
# This module consists of data reduction "helper" functions for inputs to other modules

__author__ = 'bob dagostino'

import numpy as np
import os, glob, copy, datetime, time
import shlex

########################################################################################################################
# Parse a directory of .mol files from a root, and stores data in standard Python data types
def parse_mol_files_dir(mol_root):

    mol_files = glob.glob(mol_root + '/*.mol')
    mol_files_upper = glob.glob(mol_root + '/*.MOL')
    [mol_files.append(copy.deepcopy(mol_file_upper)) for mol_file_upper in mol_files_upper]
    mol_files = sorted(mol_files) # alphabetize the filenames
    Nmols = len(mol_files)
    all_mol_names = []
    all_atom_types = []
    all_atom_pos = []
    for n in range(0, Nmols):
        try:
            molecule_name, atom_types, atom_pos = parse_mol_file(mol_files[n])
        except:
            print('!!!!! Error parsing file %s !!!!!' % (mol_files[n]))
            continue
        all_mol_names.append(copy.deepcopy(molecule_name))
        all_atom_types.append(copy.deepcopy(atom_types))
        all_atom_pos.append(copy.deepcopy(atom_pos))

    return ( all_mol_names, all_atom_types, all_atom_pos)


########################################################################################################################
# Parse a single .mol file.  Advantageous to use fancy lexical analyzers to parse data out of space delimited mol files
def parse_mol_file(mol_file, return_conns = False):

    # This code ASSUMES that the mol filename is the name of the molecule
    filetokens = mol_file.split('/')

    head, file_with_ext = os.path.split(mol_file)
    molname_tokens = file_with_ext.split('.')
    molecule_name = molname_tokens[0]


    # print mol_file
    f = open(mol_file)
    lines = f.readlines()
    f.close()
    Nlines = len(lines)
    if Nlines < 4:
        if return_conns is False:
            return( [], [], [] )
        else:
            return ([], [], [], [])

    atom_types = []
    atom_pos = []
    atom_conns = []
    for n in range(2, Nlines):
        newline = lines[n].rstrip('\r\n')

        if newline.__contains__('CHG') or newline.__contains__('END'):
            break

        # Stop parsing if we see CHG or END characters in line
        newline = ','.join(shlex.split(lines[n].rstrip('\r\n')))
        tokens = newline.split(',')
        if len(tokens) < 4:
            continue

        # 4th token must be a character to be an atom position
        if tokens[3].isalpha() == False or is_number(tokens[0]) == False:
            # Check to see if current line is a connectivity line
            if len(tokens) != 7 or return_conns == False:
                continue

            cur_atom_conns = []
            for p in range(0, len(tokens)):
                cur_atom_conns.append(float(tokens[p]))
            cur_atom_conns = np.array(cur_atom_conns)
            atom_conns.append(copy.deepcopy(cur_atom_conns))

            continue
        # print tokens
        atom_types.append(tokens[3])
        curpos = [float(tokens[0]), float(tokens[1]), float(tokens[2])]
        atom_pos.append(curpos)

    if return_conns is True:
        Nconns = len(atom_conns)
        if Nconns > 0:
            conn_mat = np.zeros((Nconns, np.size(atom_conns[0])))
            for n in range(0, Nconns):
                conn_mat[n,:] = np.squeeze(atom_conns[n])
            atom_conns = np.int32(conn_mat)
        else:
            atom_conns = np.array([])

        return (molecule_name, atom_types, atom_pos, atom_conns)

    return( molecule_name, atom_types, atom_pos)


########################################################################################################################
# Renames all mol files in a directory
def rename_molfiles(root_dir):
    os.chdir(root_dir)
    mol_files = glob.glob('*.mol')
    Nmols = len(mol_files)

    for n in range(0, Nmols):
        tokens = mol_files[n].split('_')
        if len(tokens) == 0:
            tokens = mol_files[n].split('.')
        basefile = tokens[0]

        newfile = basefile + '.mol'
        os.rename(mol_files[n], newfile)

    return( True )


########################################################################################################################
# Determines if a string is a number or not.  Python's isdigit() method fails for - and . characters!!!
def is_number(str):
    try:
        float(str)
        return True
    except ValueError:
        return False


########################################################################################################################
# Parses the excluded molecules file and returns a list of molecule names to excluded
def read_excludes_file(excludes_file):
    excluded_mol_names = []

    try:
        f = open(excludes_file)
        lines = f.readlines()
        f.close()
        nlines = len(lines)

        for i in range(1, nlines):
            tokens = lines[i].split(',')
            if len(tokens[0]) > 0:
                excluded_mol_names.append(tokens[0].lower())

    except:
        print('\n\nError encountered parsing the excludes file!!!  No molecules will be excluded...\n\n')
        excluded_mol_names = []

    return( excluded_mol_names )


########################################################################################################################
# Parses activity data from a .csv file for each molecule.  Activity assumed to be in second column.  All other columns ignored
def read_acts_file(acts_file, pred_type = 'classification', excludes_file = None):
    print('Reading activity data from: %s' % (acts_file))
    acts = []
    mol_names = []

    f = open(acts_file)
    lines = f.readlines()
    f.close()

    ct = 0
    for line in lines:
        if( ct == 0):
            ct += 1 # assumes top row is a header row
            continue
        newline = line.rstrip('\r\n')
        tokens = newline.split(',')

        if len(tokens) < 2:
            continue
        if len(tokens[0]) < 1 or len(tokens[1]) < 1:
            continue

        mol_names.append(tokens[0].lower())
        acts.append(float(tokens[1]))

    # Check for duplicate molecule names in the activity file.  Show duplicates (if any) to user- the user must handle
    #   this situation
    core_names = []
    for mol_name in mol_names:
        curbasename = mol_name.split('-')
        core_names.append(copy.deepcopy(curbasename[0]))

    nacts = len(core_names)
    unique_names, unique_counts = np.unique(core_names, return_counts=True)
    nunique = len(unique_names)

    if nunique < nacts:

        dupidx = np.where( unique_counts > 1)[0]
        ndupes = np.size(dupidx)
        print('\n\n\n\033[1;41m!!! %d duplicate core structures detected in activity file: %s !!!' % (ndupes, acts_file))
        for i in range(0, ndupes):
            print('\t%s,  listed %d times in activity file' % (unique_names[dupidx[i]], unique_counts[dupidx[i]]))

        print('\033[1;m\n\n')

    # Get the list of the excluded molecules from the excluded file, if one is supplied
    if excludes_file is not None:
        got_excludes = False
        try:
            exclude_mol_names = read_excludes_file(excludes_file)

            excludeidx = []
            for i in range(0, nacts):
                if mol_names[i] in exclude_mol_names:
                    excludeidx.append(copy.deepcopy(i))

            excludeidx = np.array(excludeidx)
            keepidx = np.setdiff1d(np.arange(0,nacts), excludeidx)

            keep_mol_names = []
            keep_acts = []
            for i in range(0,np.size(keepidx)):
                keep_mol_names.append(copy.deepcopy(mol_names[keepidx[i]]))
                keep_acts.append(copy.deepcopy(acts[keepidx[i]]))

            if np.size(excludeidx) > 0:
                print('\n\n%d molecule(s) from activity file on excludes list:' % (np.size(excludeidx)))
                for i in range(0, np.size(excludeidx)):
                    print('\tExcluding molecule: %s' % (mol_names[excludeidx[i]]))
                print('\n\n')
            mol_names = copy.deepcopy(keep_mol_names)
            acts = copy.deepcopy(keep_acts)

        except:
            print('Could not read molecules from excludes file: %s' % (excludes_file))

    # convert activities to a numpy array
    if len(acts) > 0:
        acts = np.array(acts).reshape((len(acts),1))

    return( acts, mol_names)


########################################################################################################################
# Merges 2+ activity files into a "multi" activity file
def merge_activity_files(act_files, multi_acts_file, allow_missing_acts = True, task_names = None, pred_type = 'classification'):
    merged_files = False

    n_files = len(act_files)
    if n_files < 2:
        return(merged_files)

    all_acts = []
    all_mol_names = []
    assoc_names = []

    # If task names are not passed in as an argument then take task names from the activity file names
    if task_names is None:
        task_names = []
        task_names_provided = False
    else:
        task_names_provided = True

    for i in range(n_files):
        if task_names_provided == False:
            root_path, basefile = os.path.split(act_files[i])
            tokens = basefile.split('.')
            task_names.append(copy.deepcopy(tokens[0]))

        acts, mol_names = read_acts_file(act_files[i])
        all_acts.append(copy.deepcopy(acts))
        assoc_names.append(copy.deepcopy(mol_names))
        if i == 0:
            all_mol_names = copy.deepcopy(mol_names)
        else:
            for j in range(0, len(mol_names)):
                all_mol_names.append(copy.deepcopy(mol_names[j]))

    # Get unique molecule names from all activity files
    all_mol_names = list(set(tuple(all_mol_names)))
    n_mols = len(all_mol_names)

    # Write out the combined, deduplicated list to file
    actvals = np.zeros(n_files)
    f = open(multi_acts_file,'w')
    f.write('molecule_name,')
    for i in range(0, n_files):
        f.write('%s,' % (task_names[i]))
    f.write('\n')
    for i in range(0, n_mols):

        for j in range(0, n_files):
            # Get the activity for the ith molecule in the jth activity file
            if all_mol_names[i] in assoc_names[j]:
                matchidx = assoc_names[j].index(all_mol_names[i])
                actvals[j] = all_acts[j][matchidx]
            else:
                # Missing value
                actvals[j] = np.nan
        # If the molecule lacks all activities, don't write it to the merged file- nothing to train / evaluate on!
        if np.all(np.isnan(actvals)):
            continue

        # If we don't allow ANY missing values and we have at least one missing value, skip this molecule
        if allow_missing_acts == False and np.any(np.isnan(actvals)):
            continue

        f.write('%s,' % (all_mol_names[i]))
        for j in range(n_files):
            if np.isnan(actvals[j]):
                f.write(',')
            else:
                f.write('%f,' % (actvals[j]))
        f.write('\n')

    f.close()
    merged_files = True

    return( merged_files )


########################################################################################################################
# Generates a vector of 3 channel color vectors to use for plotting
def generate_plot_color_vecs(n):
    cvecs = np.zeros((n,3))

    cvecs[0,:] = np.squeeze(np.array([1.0,0.0,0.0]))
    if n > 1:
        cvecs[1, :] = np.squeeze(np.array([0.0, 1.0, 0.0]))
    if n > 2:
        cvecs[2, :] = np.squeeze(np.array([0.0, 0.0, 1.0]))

    if n > 3:
        cvecs[3, :] = np.squeeze(np.array([1.0, 1.0, 0.0]))
    if n > 4:
        cvecs[4, :] = np.squeeze(np.array([1.0, 0.0, 1.0]))
    if n > 5:
        cvecs[5, :] = np.squeeze(np.array([0.0, 1.0, 1.0]))

    for i in range(6, n):
        cvecs[i,:] = np.squeeze(np.random.uniform(0.0,1.0,(1,3)) )

    return(cvecs)
