# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

########################################################################################################################
# This is a module for generating reports

import MolPred as mp
import MachineLearningUtils as ml
import os, glob, copy, time, datetime, sys, shutil
import numpy as np
from sys import platform
import matplotlib
if platform == "darwin":
    # Need to set the graphics back-end to something suitable for MacOS X
    matplotlib.use('agg')
import matplotlib.pyplot as plt


########################################################################################################################
# Moves all inputs used to train / test a model to a root directory, for replay and archival purposes
def modeling_file_archive(archive_root, models, config_file, int_mols=None, test_mols=None, archive_config = False):
    did_backup = False

    if not os.path.isdir(archive_root):
        os.mkdir(archive_root)
        print('Created archival root directory: %s' % (archive_root))

    if int_mols is None and test_mols is None:
        print('No training, validation, or test set molecules to archive!!!')
        return( did_backup )

    # If it exists, back up the activity file
    if models.io_paths.activity_file is not None:
        shutil.copy(models.io_paths.activity_file, archive_root)

    # Back up the model configuration file, renaming it if naming conventions have been adhered to
    if archive_config:
        cfg_root, cfg_stem = os.path.split(config_file)
        tokens = cfg_stem.split('+')
        if len(tokens) == 3:
            new_stem = tokens[0] + tokens[2]
            renamed_config_file = os.path.join(archive_root, new_stem)
            shutil.copy(config_file, renamed_config_file)
        else:
            shutil.copy(config_file, archive_root)
        os.remove(config_file)

    # Back up "internal" set (training, validation) molecules to int archive directory
    if int_mols is not None:
        int_archive_root = os.path.join(archive_root,'IntMols')
        os.mkdir(int_archive_root)

        # Back up mol files to archive directory
        for mol in int_mols:
            if models.io_paths.mol_root is not None:
                cur_mol_file = os.path.join(models.io_paths.mol_root, mol.mol_name.upper() + '.mol')
                if os.path.exists( cur_mol_file ):
                    shutil.copy(cur_mol_file, int_archive_root)
                else:
                    print('Couldn''t find mol file %s to move to archive directory!!!' % (cur_mol_file))


    # Back up "external" test set molecules to int archive directory
    if test_mols is not None:
        test_archive_root = os.path.join(archive_root,'TestMols')
        os.mkdir(test_archive_root)

        # Back up mol files to archive directory
        for mol in test_mols:
            if models.io_paths.mol_root is not None:
                cur_mol_file = os.path.join(models.io_paths.mol_root, mol.mol_name.upper() + '.mol')
                if os.path.exists( cur_mol_file ):
                    shutil.copy(cur_mol_file, test_archive_root)
                else:
                    print('Couldn''t find mol file %s to move to archive directory!!!' % (cur_mol_file))

        # Copy the test set molecules to their own external test set file
        test_set_file = os.path.join(archive_root,'TestSetUsed.csv')
        f = open(test_set_file,'w')
        f.write('Molecule')
        for i in range(models.n_tasks):
            f.write(',%s' % (models.task_names[i]))
        f.write('\n')
        for mol in test_mols:
            f.write('%s,' % mol.mol_name.upper())
            for i in range(models.n_tasks):
                f.write('%f,' % (mol.acts[i]))
            f.write('\n')
        f.close()


    # Compress the archive, if possible.
    try:
        os.chdir(archive_root)
        results_root, tail = os.path.split(archive_root)

        # Move the features .csv files to the archive directory, if they exist.  They can be big but compress nicely
        feats_files = glob.glob(results_root + '/*Features*.csv*')
        for feats_file in feats_files:
            head, tail = os.path.split(feats_file)
            newfeats_file = os.path.join(archive_root,tail)
            shutil.move(feats_file, newfeats_file)

        compressed_name = shutil.make_archive(os.path.join(archive_root), 'zip', root_dir=archive_root)

        # Eliminate the original archive folder, keeping only the zipped version
        shutil.rmtree(archive_root)
    except:
        # Some computers fail when trying to compress the archive and remove the original folder.  Not sure why, but it
        #   doesn't need to cause the program to halt as compression is a "nice to have" feature
        print('\n\nCould not .zip the archive folder!!!\n\n')

    did_backup = True

    return( did_backup )


########################################################################################################################
# Generates a performance summary report for molecules in an "external" test set
def classification_test_report(model, report_name, X_test, test_mols, Y_true = None, use_custom_fns = False):
    Ntest = np.size(X_test,0)
    unique_classes = np.array(range(0, model.Nclass))
    Y_test_med, Y_test_std, test_class_preds = ml.ensemble_pred_class(model, X_test, unique_classes, use_custom_fns=use_custom_fns)
    Nfeatsused_permol = np.sum(X_test,1)

    adstats_test = [ml.APPLICABILITY_DOMAIN() for n in range(0, len(test_mols))]
    for n in range(0, np.size(X_test,0)):
        adstats_test[n] = ml.applicability_metrics(np.ones((model.Npred)), X_test[n,:])

    if np.all(Y_true != None):
        test_stats = ml.perf_stats_class(test_class_preds, Y_true, model.n_class[0], pred_probs=Y_test_med)
        test_mocc = ml.margin_correct_class(test_class_preds, Y_true, Y_test_med, Y_test_std)
        print('\n***** Test Set Statistics *****')
        print('Accy (test) = %.3f' % (test_stats.accuracy))
        print('Confusion Matrix:')
        print(test_stats.CM)
        print('Conditional probability matrix:')
        print(test_stats.Pmat)
        print('# samples per class:')
        print(test_stats.Ns)
        print('total # samples = %d' % (np.sum(test_stats.Ns)))


    ###################################################
    # Write summary results to file

    f = open(report_name,'w')
    f.write('Classification Model Report for External Test Set\n\n')
    f.write('\nTotal # features in trained model:  %d\n\n' % (np.size(model.trainbinsidx[0])) )
    if model.exclude_sparse_mols == True:
        f.write('Exclude sparse mols?,True\n')
        f.write('Sparse bin occs frac,%f\n' % (model.sparse_frac) )
    else:
        f.write('Exclude sparse mols?,False\n')
    if model.Nclass == 2 and model.bin_class_thresh is not None:
        f.write('Class 1 Decision Thresh,%.3f\n' % (model.bin_class_thresh))
    else:
        f.write('Class Decision,Highest Prob Class\n')

    if np.all(Y_true != None):
        f.write('\n********** Test set statistics **********\n')
        f.write('Pcc (test),%.3f\n' % (test_stats.accuracy))
        f.write('Confusion matrix:\n')
        for r in range(0, model.n_class[0]):
            for c in range(0, model.n_class[0]):
                f.write('%d,' % (test_stats.CM[r, c]))
            f.write('\n')
        f.write('Conditional probability matrix:\n')
        for r in range(0, model.n_class[0]):
            for c in range(0, model.n_class[0]):
                f.write('%.3f,' % (test_stats.Pmat[r, c]))
            f.write('\n')
        f.write('# samples per class:,')
        for n in range(0, model.n_class[0]):
            f.write('%d,' % (test_stats.ns_class[n]))
        f.write('\n')
        f.write('Total # samples,%d\n' % (np.sum(test_stats.ns_class)))
        f.write('Test Summary Stats:\n')
        f.write(',accuracy,%.3f\n' % (test_stats.accuracy))
        f.write(',specificity,%.3f\n' % (test_stats.specificity))
        f.write(',sensitivity,%.3f\n' % (test_stats.sensitivity))
        f.write(',PPV,%.3f\n' % (test_stats.PPV))
        f.write(',NPV,%.3f\n' % (test_stats.NPV))
        if test_stats.auc is not None:
            f.write(',AUC,%.3f\n' % (test_stats.auc))

    f.write('\n\n************************************************\nPredictions for individual molecules:\n')

    f.write('\n>>>>> Test set molecules and ensemble predictions:\n')
    f.write('\nMolecule Name,# features used,True Class,Predicted Class,')
    for n in range(0, model.Nclass):
        f.write('Pr(class %d),' % (n))
    f.write('StdDev of Probability,MOCC,overlapfrac,Tanimoto,Hamming')
    f.write('\n')
    for m in range(0, Ntest):
        if np.all(Y_true) == None:
            f.write('%s,%d,,%d,' % (test_mols[m].mol_name.upper(), Nfeatsused_permol[m],test_class_preds[m]))
        else:
            f.write('%s,%d,%d,%d,' % (test_mols[m].mol_name.upper(), Nfeatsused_permol[m], Y_true[m], test_class_preds[m]))
        for n in range(0, model.Nclass):
            f.write('%.3f,' % (Y_test_med[m, n]))
        if np.all(Y_true) == None:
            f.write('%.3f,,%.3f,%.3f,%.3f,\n' % (Y_test_std[m, 0],adstats_test[m].binoccs_overlap_frac, adstats_test[m].tanimoto_coef, adstats_test[m].hamming_coef))
        else:
            f.write('%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d\n' % (Y_test_std[m, 0],test_mocc[m], adstats_test[m].binoccs_overlap_frac, adstats_test[m].tanimoto_coef, adstats_test[m].hamming_coef))

    f.close()

    return( )



########################################################################################################################
# Generates a regression modeling report based on model building results for "bagging" randomized method
def regression_test_report(model, report_name, test_mols, X_test, Y_true=None, use_custom_fns=True):

    Ntest = np.size(X_test,0)
    acts_are_available = False
    if np.all(Y_true) != None:
        acts_are_available = True

    # Compute per-molecule predictions for all molecules in internal, test sets
    Y_test_med, Y_test_mean, Y_test_std = ml.ensemble_prediction(model, X_test, use_custom_fns=use_custom_fns)
    Nfeatsused_pertestmol = np.sum(X_test, 1)

    # If a model was trained with activity scaling on, unscale the data before writing the report
    if model.scaling.scaling_set:
        Y_test_med = ml.invert_scaling(model.scaling, Y_test_med)
        Y_test_std = ml.invert_scaling(model.scaling, Y_test_std)


    # Compute applicability domain stats for internal and test set molecules
    adstats_test = [ml.APPLICABILITY_DOMAIN() for n in range(0, len(test_mols))]
    for n in range(0, np.size(X_test,0)):
        adstats_test[n] = ml.applicability_metrics(np.ones((model.Npred)), X_test[n,:])

    # Create the file
    f = open(report_name,'w')

    # Print out hyperparameters
    f.write('*********************************************************\n')
    f.write('Regression Model Test Prediction Report\n')
    f.write('Machine Learning type:,%s\n' % (model.ml_type.upper()))
    f.write('%d,test molecules\n' % (Ntest))
    f.write('Model class version,%s\n' % (model.class_version))
    f.write('\nEndpoint activity type,%s\n' % (model.act_type))
    activity_scaling = model.scaling.method
    if activity_scaling == None:
        activity_scaling = 'None'
    f.write('Activity scaling method,%s\n' % (activity_scaling))
    if model.exclude_sparse_mols == True:
        f.write('Exclude sparse mols?,True\n')
        f.write('Sparse bin occs frac,%f\n' % (model.sparse_frac) )
    else:
        f.write('Exclude sparse mols?,False\n')

    f.write('\n\n********** Machine Learning Hyperparameters for Trained Model **********\n\n')
    f.write('Machine learning type,%s\n' % (model.ml_type))
    if model.ml_type.lower() == 'ann':
        # ANN specific parameters
        f.write('ANN specific settings\n')
        f.write(',Activation function,%s\n' % (model.hp.act_fn))
        f.write(',# iterations,%d\n' % (model.hp.Niters))
        f.write(',Learning rate,%.4f\n' % (model.hp.learning_rate))
        f.write(',Learning rule,%s\n' % (model.hp.learning_rule))
        f.write(',Learning momentum,%s\n' % (model.hp.learning_momentum))
        f.write(',Dropout,%.3f\n' % (model.hp.dropout))
        f.write(',Nneur,%d\n' % (model.hp.Nneur))
    elif model.ml_type.lower() == 'pls':
        f.write('# components,%d\n' % (model.hp.Ncomp))
        f.write('Max iters,%d\n' % (model.hp.Niters))
        f.write('Scaling used,%d\n' % (model.hp.scale_data))

    f.write('\n\n********** Ensemble Predictions for Molecules in Test Set **********\n')
    if acts_are_available:
        f.write('Molecule,# feats used,Y true,Y pred (test),Y std (test),overlapfrac,Tanimoto,Hamming\n')
    else:
        f.write('Molecule,# feats used,Y pred (test),Y std (test),overlapfrac,Tanimoto,Hamming\n')
    for n in range(0, Ntest):
        if acts_are_available:
            # We do know the acivities
            f.write('%s,%d,%f,%f,%f,%f,%f,%f\n' % (test_mols[n].mol_name.upper(),Nfeatsused_pertestmol[n],test_mols[n].acts,Y_test_med[n],Y_test_std[n],
                                                   adstats_test[n].binoccs_overlap_frac, adstats_test[n].tanimoto_coef, adstats_test[n].hamming_coef) )
        else:
            # Don't know the "true" activity for the molecule
            f.write('%s,%d,%f,%f,%f,%f,%f\n' % (test_mols[n].mol_name.upper(), Nfeatsused_pertestmol[n], Y_test_med[n], Y_test_std[n],
            adstats_test[n].binoccs_overlap_frac, adstats_test[n].tanimoto_coef, adstats_test[n].hamming_coef))

    f.write('\n\n')

    f.close()

    return( )



########################################################################################################################
# Generates a performance summary report based on training statistics for all classes
# Includes test set stats if a test set was used
def classification_training_report(models, report_name, int_mols, test_mols, X_int, Y_int, X_test = None, Y_test = None):

    valid_probs, valid_prob_std, valid_class_preds, train_probs, train_prob_std, train_class_preds = \
        ml.models_predict_class(models, X_int, is_int_set=True)

    models.train_stats = ml.multitask_perf_stats_class(train_class_preds, Y_int, models.n_class,
                                                       pred_probs=train_probs)
    models.valid_stats = ml.multitask_perf_stats_class(valid_class_preds, Y_int, models.n_class,
                                                       pred_probs=valid_probs)

    if models.n_test > 0:
        test_probs, test_prob_std, test_class_preds = ml.models_predict_class(models, X_test)
        models.test_stats = ml.multitask_perf_stats_class(test_class_preds, Y_test, models.n_class,
                                                          pred_probs=test_probs)



    for t in range(models.n_tasks):
        print('\n\nPerformance stats for task #%d (%s)' % (t+1, models.task_names[t]))
        print('***** Training Set Statistics *****')
        print('Accuracy (train),%.3f' % (models.train_stats[t].accuracy))
        print('Confusion Matrix:')
        print(models.train_stats[t].CM)
        print('Conditional probability matrix:')
        print(models.train_stats[t].Pmat)
        print('# samples per class:')
        print(models.train_stats[t].ns_class)
        print('total # samples = %d' % (np.sum(models.train_stats[t].ns_class)))
        print('\n***** Validation Set Statistics *****')
        print('Accuracy (valid),%.3f' % (models.valid_stats[t].accuracy))
        print('Confusion Matrix:')
        print(models.valid_stats[t].CM)
        print('Conditional probability matrix:')
        print(models.valid_stats[t].Pmat)
        print('# samples per class:')
        print(models.valid_stats[t].ns_class)
        print('total # samples %d\n' % (np.sum(models.valid_stats[t].ns_class)))


        if models.n_test > 0:
            print('\n\n***** Test Set Statistics *****')
            print('Accuracy (test),%.3f' % (models.test_stats[t].accuracy))
            print('Confusion Matrix:')
            print(models.test_stats[t].CM)
            print('Conditional probability matrix:')
            print(models.test_stats[t].Pmat)
            print('# samples per class:')
            print(models.test_stats[t].ns_class)
            print('total # samples,%d' % (np.sum(models.test_stats[t].ns_class)))


    ###################################################
    # Write summary results to file for all tasks
    Nfeatsused_permol = np.sum(X_int, 1)


    f = open(report_name,'w')
    f.write('Classification Model Report\n\n')
    f.write('\n********** Basic Model Building Information **********\n\n')
    f.write('# bagging models built,%d\n' % (models.n_models))
    f.write('Model name,%s\n' % (models.model_name))
    f.write('Total # molecules,%d\n' % (models.n_grandtotal))
    f.write('MODEL class version,%s\n' % (models.version))
    f.write('\r\n')
    f.write('Expected # of train, validation, test samples with data for each task\n')
    f.write('NOTE: actual sample counts may vary for individual models due to presence of missing labels\n')
    for t in range(models.n_tasks):

        notnan_int = np.where(np.isnan(Y_int[:,t]) == False)[0]
        n_int = int(np.size(notnan_int))
        if models.n_test > 0:
            notnan_test= np.where(np.isnan(Y_test[:,t]) == False)[0]
            n_test = int(np.size(notnan_test))
        else:
            n_test = 0

        test_ratio = float(n_test) / float(n_int + n_test)

        # n_test = int(np.round(models.test_ratio * models.n_totalintask[t]))
        int_ratio = 1.0 - test_ratio
        # n_int = int( np.round(int_ratio * models.n_totalintask[t]) )
        n_valid = int(np.round(models.valid_ratio * n_int))
        n_train = n_int - n_valid
        train_ratio = float(n_train)/float(n_int)
        valid_ratio = float(n_valid) / float(n_int)
        # test_ratio = float(n_test) / float(models.n_totalintask[t])
        f.write('Sample Counts for task:  %s (%d of %d)\n' % (models.task_names[t], t+1, models.n_tasks))
        f.write(',# internal molecules,%d,,internal ratio:,%.3f\n' % (n_int, int_ratio))
        f.write(',# training molecules per model,%d,,training ratio:,%.3f\n' % (n_train, train_ratio))
        f.write(',# validation molecules per model,%d,,valid ratio:,%.3f\n' % (n_valid, valid_ratio))
        f.write(',# holdout test set molecules,%d,,test ratio:,%.3f\n' % (n_test, test_ratio))
        f.write(',# missing labels,%d,\n' % (models.n_missing[t]))
    f.write('\r\n')

    if models.exclude_sparse_mols:
        f.write('Exclude sparse mols?,True\n')
        f.write('Sparse bin occs frac,%f\n' % (models.sparse_frac) )
    else:
        f.write('Exclude sparse mols?,False\n')

    for t in range(models.n_tasks):
        f.write('Task #%d (%s) class decision criteria:\n' % (t+1, models.task_names[t]))
        if models.n_class[t] == 2 and models.preds.bin_class_thresh[t] is not None:
            f.write('Class 1 Decision Thresh,%.3f\n' % (models.preds.bin_class_thresh[t]))
        else:
            f.write('Class Decision,Highest Prob Class\n')

    if models.random_seed is not None:
        f.write('Random seed,%d\n' % (models.random_seed))
    else:
        f.write('Random seed,None\n')


    # Write feature settings / parameters out to file
    f = write_feature_params(f, models)

    # Write ML hyperparameters to file
    f = write_hyperparameters(f, models)

    for t in range(models.n_tasks):
        f.write('\n\n>>>>>>>>>>>>>>>>>>>>>>>>>> Statistics for Task #%d (%s) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n' % (t+1, models.task_names[t]))

        f.write('\n********** Training set statistics **********\n')
        f.write('Accuracy (train),%.3f\n' % (models.train_stats[t].accuracy))
        f.write('Confusion matrix:\n')
        for r in range(0,models.n_class[t]):
            for c in range(0, models.n_class[t]):
                f.write('%d,' % (models.train_stats[t].CM[r,c]))
            f.write('\n')
        f.write('Conditional probability matrix:\n')
        for r in range(0,models.n_class[t]):
            for c in range(0, models.n_class[t]):
                f.write('%.3f,' % (models.train_stats[t].Pmat[r,c]))
            f.write('\n')
        f.write('# samples per class,')
        for n in range(0, models.n_class[t]):
            f.write('%d,' % (models.train_stats[t].ns_class[n]))
        f.write('\n')
        f.write('Total # samples,%d\n' % (np.sum(models.train_stats[t].ns_class)))
        if models.n_class[t] == 2:
            f.write('Training Summary Stats:\n')
            f.write(',accuracy,%.3f\n' % (models.train_stats[t].accuracy))
            f.write(',specificity,%.3f\n' % (models.train_stats[t].specificity))
            f.write(',sensitivity,%.3f\n' % (models.train_stats[t].sensitivity))
            f.write(',PPV,%.3f\n' % (models.train_stats[t].PPV))
            f.write(',NPV,%.3f\n' % (models.train_stats[t].NPV))
            if models.train_stats[t].auc is not None:
                f.write(',AUC,%.3f\n' % (models.train_stats[t].auc))


        f.write('\n\n********** Validation set statistics **********\n')
        f.write('Accuracy (validation),%.3f\n' % (models.valid_stats[t].accuracy))
        f.write('Confusion matrix:\n')
        for r in range(0,models.n_class[t]):
            for c in range(0, models.n_class[t]):
                f.write('%d,' % (models.valid_stats[t].CM[r,c]))
            f.write('\n')
        f.write('Conditional probability matrix:\n')
        for r in range(0,models.n_class[t]):
            for c in range(0, models.n_class[t]):
                f.write('%.3f,' % (models.valid_stats[t].Pmat[r,c]))
            f.write('\n')
        f.write('# samples per class,')
        for n in range(0, models.n_class[t]):
            f.write('%d,' % (models.valid_stats[t].ns_class[n]))
        f.write('\n')
        f.write('Total # samples,%d\n' % (np.sum(models.valid_stats[t].ns_class)))
        if models.n_class[t] == 2:
            f.write('Validation Summary Stats:\n')
            f.write(',accuracy,%.3f\n' % (models.valid_stats[t].accuracy))
            f.write(',specificity,%.3f\n' % (models.valid_stats[t].specificity))
            f.write(',sensitivity,%.3f\n' % (models.valid_stats[t].sensitivity))
            f.write(',PPV,%.3f\n' % (models.valid_stats[t].PPV))
            f.write(',NPV,%.3f\n' % (models.valid_stats[t].NPV))
            if models.valid_stats[t].auc is not None:
                f.write(',AUC,%.3f\n' % (models.valid_stats[t].auc))

        if models.n_test > 0:
            f.write('\n\n********** Test set statistics **********\n')
            f.write('Accuracy (test),%.3f\n' % (models.test_stats[t].accuracy))
            f.write('Confusion matrix:\n')
            for r in range(0, models.n_class[t]):
                for c in range(0, models.n_class[t]):
                    f.write('%d,' % (models.test_stats[t].CM[r, c]))
                f.write('\n')
            f.write('Conditional probability matrix:\n')
            for r in range(0, models.n_class[t]):
                for c in range(0, models.n_class[t]):
                    f.write('%.3f,' % (models.test_stats[t].Pmat[r, c]))
                f.write('\n')
            f.write('# samples per class:,')
            for n in range(0, models.n_class[t]):
                f.write('%d,' % (models.test_stats[t].ns_class[n]))
            f.write('\n')
            f.write('Total # samples,%d\n' % (np.sum(models.test_stats[t].ns_class)))
            if models.n_class[t] == 2:
                f.write('Test Summary Stats:\n')
                f.write(',accuracy,%.3f\n' % (models.test_stats[t].accuracy))
                f.write(',specificity,%.3f\n' % (models.test_stats[t].specificity))
                f.write(',sensitivity,%.3f\n' % (models.test_stats[t].sensitivity))
                f.write(',PPV,%.3f\n' % (models.test_stats[t].PPV))
                f.write(',NPV,%.3f\n' % (models.test_stats[t].NPV))
                if models.test_stats[t].auc is not None:
                    f.write(',AUC,%.3f\n' % (models.test_stats[t].auc))


        f.write('\n\n************************************************\nPredictions for individual molecules:\n')
        f.write('\n\n>>>>> Training set molecules and ensemble predictions:\n')
        f.write('Molecule Name,# Feats Used,True Class,Predicted Class,')
        for n in range(0, models.n_class[t]):
            f.write('Pr(class %d),' % (n))
        f.write('StdDev of Probability,MOCC,overlapfrac,Tanimoto,Hamming')
        f.write('\n')
        for m in range(0, models.n_int):
            if np.isnan(Y_int[m,t]) or Y_int[m,t] is None:
                Ycur_str = ''
            else:
                Ycur_str = str(int(Y_int[m,t]))

            if np.isnan(train_class_preds[t][m]):
                train_class_str = ''
            else:
                train_class_str = str(int(train_class_preds[t][m]))

            f.write('%s,%d,%s,%s,' % (int_mols[m].mol_name.upper(),Nfeatsused_permol[m],Ycur_str,train_class_str))
            for n in range(0, models.n_class[t]):
                f.write('%.3f,' % (train_probs[t][m,n]))
            f.write('\n')

        f.write('\n\n>>>>> Validation set molecules and ensemble predictions:\n')
        f.write('Molecule Name,# Feats Used,True Class,Predicted Class,')
        for n in range(0, models.n_class[t]):
            f.write('Pr(class %d),' % (n))
        f.write('StdDev of Probability,MOCC,overlapfrac,Tanimoto,Hamming')
        f.write('\n')
        for m in range(0, models.n_int):
            if np.isnan(Y_int[m,t]) or Y_int[m,t] is None:
                Ycur_str = ''
            else:
                Ycur_str = str(int(Y_int[m,t]))

            if np.isnan(valid_class_preds[t][m]):
                valid_class_str = ''
            else:
                valid_class_str = str(int(valid_class_preds[t][m]))

            f.write('%s,%d,%s,%s,' % (int_mols[m].mol_name.upper(),Nfeatsused_permol[m],Ycur_str,valid_class_str))
            for n in range(0, models.n_class[t]):
                f.write('%.3f,' % (valid_probs[t][m,n]))
            f.write('\n')

        if models.n_test > 0:
            Nfeatsused_permol_test = np.sum(X_test, 1)

            f.write('\n\n>>>>> Test set molecules and ensemble predictions:\n')
            f.write('Molecule Name,# Feats Used,True Class,Predicted Class,')
            for n in range(0, models.n_class[t]):
                f.write('Pr(class %d),' % (n))
            f.write('StdDev of Probability,MOCC,overlapfrac,Tanimoto,Hamming')
            f.write('\n')
            for m in range(0, models.n_test):
                if np.isnan(Y_test[m, t]) or Y_test[m, t] is None:
                    Ycur_str = ''
                else:
                    Ycur_str = str(int(Y_test[m, t]))

                f.write('%s,%d,%s,%d,' % (test_mols[m].mol_name.upper(), Nfeatsused_permol_test[m], Ycur_str, test_class_preds[t][m]))
                for n in range(0, models.n_class[t]):
                    f.write('%.3f,' % (test_probs[t][m, n]))
                f.write('\n')
        for i in range(5):
            f.write('\n')
    f.close()
    return( )


########################################################################################################################
# Writes feature settings to an open file handle.  The specifics will depend on the featurization type
def write_hyperparameters(f, models):
    f.write('\n\n********** Machine Learning Hyperparameters **********\n\n')
    f.write('Machine learning type,%s\n' % (models.ml_type))
    if models.ml_type.lower() == 'ann':
        # ANN specific parameters
        f.write('# hidden layers,%d\n' % (models.hp.n_hidden))
        f.write('Activation function,%s\n' % (models.hp.act_fn))
        f.write('# epochs,%d\n' % (models.hp.n_epochs))
        f.write('# iterations,%d\n' % (models.hp.n_iters))
        f.write('# neurons,%d\n' % (models.hp.n_neur))
        f.write('Learning rate,%.4f\n' % (models.hp.learning_rate))
        f.write('Learning rule,%s\n' % (models.hp.learning_rule))
        f.write('Learning momentum,%s\n' % (models.hp.learning_momentum))
        f.write('Dropout,%.3f\n' % (models.hp.dropout))
    elif models.ml_type.lower() == 'pls':
        f.write('# components,%d\n' % (models.hp.n_comp))
        f.write('Max iters,%d\n' % (models.hp.n_iters))
        f.write('PLS Scaling used,%d\n' % (models.hp.scale_data))
        f.write('Algorithm used,%s\n' % (models.hp.algo))
    elif models.ml_type.lower() == 'knn':
        f.write('# nearest neighbors,%d\n' % (models.hp.k))
        f.write('distance type,%s\n' % (models.hp.distance_type))
        f.write('weight type,%s\n' % (models.hp.weight_type))
    elif models.ml_type.lower() == 'xgboost':
        f.write('XGBoost specific settings:\n')
        f.write('max depth,%d\n' % (models.hp.max_depth))
        f.write('learning rate,%f\n' % (models.hp.learning_rate))
        f.write('n_estimators,%d\n' % (models.hp.n_estimators))
        f.write('objective fn,%s\n' % (models.hp.objective))

    f.write('\n\n')
    return(f)


########################################################################################################################
# Writes feature settings to an open file handle.  The specifics will depend on the featurization type
def write_feature_params(f, models):
    f.write('\n\n********** Input Data Featurization Settings **********\n\n')
    f.write('Feature type,%s\n' % (models.feats_type))
    f.write('Total # features used in trained model,%d\n' % (models.n_feats))
    f.write('Sample training weight type,%s\n' % (models.preds.training_weight_type))

    if models.feats_type == 'ecfp':
        f.write('# ECFP bits,%d\n' % (models.feats.n_bits))
        f.write('Fingerprint radius,%d,(max # bond connections)\n' % (models.feats.fp_radius))
        if models.feats.use_fcfp:
            f.write('Use FCFP feats?,True\n')
        else:
            f.write('Use FCFP feats?,False\n')

    f.write('\n')

    return(f)



########################################################################################################################
# Generates a regression modeling report based on model building results for "bagging" randomized method
def regression_training_report(models, report_name, int_mols, test_mols, X_int, Y_int, X_test, Y_test):

    # Compute per-molecule predictions for al molecules in internal, test sets
    has_test_set = False
    if Y_test is not None:
        if np.size(Y_test) > 0:
            has_test_set = True


    preds_valid, preds_valid_std, all_preds_valid, preds_train, preds_train_std, all_preds_train = ml.models_predict(models, X_int, is_int_set=True)
    n_featsused_perintmol = np.sum(X_int, 1)

    if has_test_set:
        preds_test, preds_test_std, all_preds_test = ml.models_predict(models, X_test)


    # For reporting / data analysis purposes, put activities back to original scaling
    if models.scaling.scaling_set:
        unscaled_preds_valid = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_valid))
        unscaled_preds_valid_std = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_valid_std), mean_center=False)
        unscaled_preds_train = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_train))
        unscaled_preds_train_std = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_train_std), mean_center=False)
        unscaled_int = ml.invert_scaling(models.scaling, Y_int)

        if has_test_set:
            unscaled_preds_test = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_test))
            unscaled_preds_test_std = ml.invert_scaling(models.scaling, ml.list_to_mat(preds_test_std), mean_center=False)
            unscaled_test = ml.invert_scaling(models.scaling, Y_test)

        unscaled_confint_per_pred_valid = []
        unscaled_confint_per_pred_train = []
        unscaled_confint_per_pred_test = []
        for i in range(models.n_tasks):
            unscaled_confint_per_pred_valid.append(ml.invert_scaling(models.scaling, models.valid_stats[i].confint_per_pred))
            unscaled_confint_per_pred_train.append(ml.invert_scaling(models.scaling, models.train_stats[i].confint_per_pred))
            if has_test_set:
                unscaled_confint_per_pred_test.append(ml.invert_scaling(models.scaling, models.test_stats[i].confint_per_pred))

    # Create the file
    f = open(report_name,'w')

    # Print out hyperparameters
    f.write('*********************************************************\n')
    f.write('%s Regression Model Building Performance Report\n' % (models.feats_type))
    f.write('Machine Learning type:,%s\n' % (models.ml_type.upper()))
    f.write('MODEL class version,%s\n' % (models.version))
    f.write(',# internal molecules,%d,,internal ratio:,%.3f\n' % (models.n_int, float(models.n_int)/float(models.n_grandtotal)))
    f.write(',# training molecules per model,%d,,training ratio:,%.3f\n' % (models.n_train, float(models.n_train)/float(models.n_int)))
    f.write(',# validation molecules per model,%d,,valid ratio:,%.3f\n' % (models.n_valid, float(models.n_valid)/float(models.n_int)))
    f.write(',# holdout test set molecules,%d,,test ratio:,%.3f\n' % (models.n_test, float(models.n_test)/float(models.n_grandtotal)))

    f.write('\nModel Name,%s\n' % (models.model_name))
    activity_scaling = models.scaling.method
    if activity_scaling is None:
        activity_scaling = 'None'
    f.write('Activity scaling method,%s\n' % (activity_scaling))
    if models.exclude_sparse_mols == True:
        f.write('Exclude sparse mols?,True\n')
        f.write('Sparse bin occs frac,%f\n' % (models.sparse_frac) )
    else:
        f.write('Exclude sparse mols?,False\n')


    f.write('\n\nSummary statistics across %d tasks in all %d models:\n' % (models.n_tasks, models.n_models))

    ci_string = str(int(np.round(models.siglevel * 100))) + '% CI'
    f.write('\n********** Training Set Statistics **********\n')
    f.write('Task Name,Rsq,Rsq %s LowerLimit,Rsq %s UpperLimit,RMS Error, MaxAbs Error\n' % (ci_string,ci_string))
    for i in range(models.n_tasks):
        f.write('%s,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (models.task_names[i],models.train_stats[i].rsq,models.train_stats[i].confint_rsq[0,0],
                                              models.train_stats[i].confint_rsq[0,1], models.train_stats[i].e_rms,
                                              models.train_stats[i].e_maxabs))

    f.write('\n********** Validation Set Statistics **********\n')
    f.write('Task Name,Rsq,Rsq %s LowerLimit,Rsq %s UpperLimit,RMS Error, MaxAbs Error\n' % (ci_string,ci_string))
    for i in range(models.n_tasks):
        f.write('%s,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (models.task_names[i],models.valid_stats[i].rsq,models.valid_stats[i].confint_rsq[0,0],
                                              models.valid_stats[i].confint_rsq[0,1], models.valid_stats[i].e_rms,
                                              models.valid_stats[i].e_maxabs))

    if has_test_set:
        f.write('\n********** Test Set Statistics **********\n')
        f.write('Task Name,Rsq,Rsq %s LowerLimit,Rsq %s UpperLimit,RMS Error, MaxAbs Error\n' % (ci_string,ci_string))
        for i in range(models.n_tasks):
            f.write('%s,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (models.task_names[i],models.test_stats[i].rsq,models.test_stats[i].confint_rsq[0,0],
                                                  models.test_stats[i].confint_rsq[0,1], models.test_stats[i].e_rms,
                                                  models.test_stats[i].e_maxabs))
    f.write('\n')

    f = write_feature_params(f, models)
    f = write_hyperparameters(f, models)

    for i in range(models.n_tasks):
        f.write('\n')
        for j in range(3):
            f.write('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n')
        f.write('Predictions for task: %s (%d of %d tasks)\n' % (models.task_names[i], i+1, models.n_tasks))

        f.write('\n\n********** Training Set Model Predictions **********\n')
        f.write('Molecule,# features used,Y true (scaled),Prediction (scaled),Pred StdDev (scaled),Pred %s LowerLimit (scaled),Pred %s UpperLimit (scaled),' % (ci_string,ci_string))
        if models.scaling.scaling_set:
            f.write(',Y true (unscaled),Prediction (unscaled),Pred StdDev (unscaled),Pred %s LowerLimit (unscaled),Pred %s UpperLimit (unscaled)' % (ci_string,ci_string))
        f.write('\n')
        for j in range(0, models.n_int):
            f.write('%s,%d,%f,%f,%f,%f,%f,' % (int_mols[j].mol_name.upper(),n_featsused_perintmol[j],Y_int[j,i],
                                         preds_train[i][j],preds_train_std[i][j],models.train_stats[i].confint_per_pred[j,0],
                                               models.train_stats[i].confint_per_pred[j,1]))
            if models.scaling.scaling_set:
                f.write(',%f,%f,%f,%f,%f' % (unscaled_int[j,i],unscaled_preds_train[j,i], unscaled_preds_train_std[j,i],
                                             unscaled_confint_per_pred_train[i][j,0], unscaled_confint_per_pred_train[i][j,1]))
            f.write('\n')
        f.write('\n\n')

        f.write('\n\n********** Validation Set Model Predictions **********\n')
        f.write('Molecule,# features used,Y true (scaled),Prediction (scaled),Pred StdDev (scaled),Pred %s LowerLimit (scaled),Pred %s UpperLimit (scaled),' % (ci_string,ci_string))
        if models.scaling.scaling_set:
            f.write(',Y true (unscaled),Prediction (unscaled),Pred StdDev (unscaled),Pred %s LowerLimit (unscaled),Pred %s UpperLimit (unscaled)' % (ci_string,ci_string))
        f.write('\n')
        for j in range(0, models.n_int):
            f.write('%s,%d,%f,%f,%f,%f,%f,' % (int_mols[j].mol_name.upper(),n_featsused_perintmol[j],Y_int[j,i],
                                         preds_valid[i][j],preds_valid_std[i][j],models.valid_stats[i].confint_per_pred[j,0],
                                               models.valid_stats[i].confint_per_pred[j, 1]))
            if models.scaling.scaling_set:
                f.write(',%f,%f,%f,%f,%f' % (unscaled_int[j,i],unscaled_preds_valid[j,i], unscaled_preds_valid_std[j,i],
                                             unscaled_confint_per_pred_valid[i][j,0],unscaled_confint_per_pred_valid[i][j,1]))
            f.write('\n')
        f.write('\n\n')

        if has_test_set:
            f.write('\n\n********** Test Set Model Predictions **********\n')
            f.write('Molecule,# features used,Y true (scaled),Prediction (scaled),Pred StdDev (scaled),Pred %s LowerLimit (scaled),Pred %s UpperLimit (scaled),' % (ci_string, ci_string))
            if models.scaling.scaling_set:
                f.write(',Y true (unscaled),Prediction (unscaled),Pred StdDev (unscaled),Pred %s LowerLimit (unscaled),Pred %s UpperLimit (unscaled)' % (ci_string,ci_string))
            f.write('\n')
            for j in range(0, models.n_test):
                f.write('%s,%d,%f,%f,%f,%f,%f,' % (test_mols[j].mol_name.upper(), n_featsused_perintmol[j], Y_test[j, i],
                                                   preds_test[i][j], preds_test_std[i][j],
                                                   models.test_stats[i].confint_per_pred[j, 0],
                                                   models.test_stats[i].confint_per_pred[j, 1]))
                if models.scaling.scaling_set:
                    f.write(',%f,%f,%f,%f,%f' % (unscaled_test[j,i],unscaled_preds_test[j,i], unscaled_preds_test_std[j,i],
                                                 unscaled_confint_per_pred_test[i][j,0],unscaled_confint_per_pred_test[i][j,1]))
                f.write('\n')
            f.write('\n\n')

    f.close()

    return( )


########################################################################################################################
# Generates a tanimoto similarity report of feature descriptor vectors
def tanimoto_similarity_report(report_name, X, Y, pred_type='regression', labels=None, small_tc_thresh=0.01):
    npred = np.size(X, 1)
    ns = np.size(X, 0)
    report_root, tail = os.path.split(report_name)
    tokens = tail.split('.')
    plot_base = tokens[0]

    if labels is None:
        labels = []
        for i in range(0, ns):
            labels.append('mol_' + str(i))

    usefeatidx = np.arange(0, npred)  # use all features
    nfeatscomp = np.size(usefeatidx)

    # Comparing all samples to all samples
    nclass = 1
    Xmean_all = np.mean(X[:, usefeatidx], 0).reshape((1, nfeatscomp))
    tanimat_all = np.zeros((ns, ns))
    tc_sumsq_all = 0.0
    coeffs_all = []
    ntc_all = 0
    for i in range(0, ns):
        for j in range(i + 1, ns):
            tanimat_all[i, j] = ml.tanimoto_coefficient(X[i, usefeatidx], X[j, usefeatidx])
            tc_sumsq_all += (tanimat_all[i, j] ** 2.0)
            coeffs_all.append(copy.deepcopy(tanimat_all[i, j]))
            if np.isnan(tc_sumsq_all):
                print('debug here:')
            ntc_all += 1
    tc_rms_all = np.sqrt(tc_sumsq_all / float(ntc_all))
    coeffs_all = np.array(coeffs_all)

    if pred_type == 'classification':
        uclasses = np.unique(Y)
        nclass = np.size(uclasses)
        Xmean_class = np.zeros((nclass, npred))
        ns_class = np.zeros(nclass, dtype=np.int32)
        tc_sumsq = np.zeros(nclass)
        ntc_class = np.zeros(nclass)
        tc_rms_class = np.zeros(nclass)
        class_coeffs_all = []

        labels_per_class = []

        tanimat = []
        for i in range(0, nclass):
            curclassidx = np.where(Y == uclasses[i])[0]
            ns_class[i] = np.size(curclassidx)
            Xmean_class[i, :] = np.mean(X[curclassidx, :], 0)
            cur_labels = []
            cur_class_coeffs = []

            curtanimat = np.zeros((ns_class[i], ns_class[i]))
            for j in range(0, ns_class[i]):
                cur_labels.append(copy.deepcopy(labels[curclassidx[j]]))
                for k in range(j + 1, ns_class[i]):
                    curtanimat[j, k] = ml.tanimoto_coefficient(X[curclassidx[j], usefeatidx],
                                                            X[curclassidx[k], usefeatidx])
                    tc_sumsq[i] += (curtanimat[j, k] ** 2.0)
                    cur_class_coeffs.append(copy.deepcopy(curtanimat[j, k]))
                    ntc_class[i] += 1
            tc_rms_class[i] = np.sqrt(tc_sumsq[i] / float(ntc_class[i]))
            cur_class_coeffs = np.array(cur_class_coeffs)
            class_coeffs_all.append(copy.deepcopy(cur_class_coeffs))
            tanimat.append(copy.deepcopy(curtanimat))
            labels_per_class.append(copy.deepcopy(cur_labels))

    # Some statistics and indicators based on Tanimoto coeffs
    small_frac_all = np.size(np.where(coeffs_all < small_tc_thresh)[0]) / float(np.size(coeffs_all))
    med_coeffs_all = np.median(coeffs_all)

    if pred_type == 'classification':
        small_frac_class = np.zeros(nclass)
        med_coeffs_class = np.zeros(nclass)
        for i in range(0, nclass):
            small_frac_class[i] = np.size(np.where(class_coeffs_all[i] < small_tc_thresh)[0]) / float(
                np.size(class_coeffs_all[i]))
            med_coeffs_class[i] = np.median(class_coeffs_all[i])

    # Trying to find outliers.  Which molecules have very low similarity compared to all others, both ALL and per class
    rms_tc_ind = np.zeros(ns)
    med_tc_ind = np.zeros(ns)
    small_frac_ind = np.zeros(ns)
    for i in range(0, ns):
        tc_rows = tanimat_all[i,i+1:]
        tc_cols = tanimat_all[:i,i]
        tc_ind = np.concatenate((tc_rows,tc_cols))
        rms_tc_ind[i] = np.sqrt( np.mean( tc_ind**2.0))
        med_tc_ind[i] = np.median( tc_ind)
        small_frac_ind[i] = np.size(np.where(tc_ind < small_tc_thresh)[0]) / float(np.size(tc_ind))

    if pred_type == 'classification':
        rms_tc_class_ind = []
        med_tc_class_ind = []
        small_frac_class_ind = []
        for i in range(0, nclass):
            rms_tc_class_ind.append(np.zeros(ns_class[i]))
            med_tc_class_ind.append(np.zeros(ns_class[i]))
            small_frac_class_ind.append(np.zeros(ns_class[i]))

        for i in range(0, nclass):
            for j in range(0, ns_class[i]):
                tc_rows = tanimat[i][j, j + 1:]
                tc_cols = tanimat[i][:j, j]
                tc_ind = np.concatenate((tc_rows, tc_cols))
                rms_tc_class_ind[i][j] = np.sqrt(np.mean(tc_ind ** 2.0))
                med_tc_class_ind[i][j] = np.median(tc_ind)
                small_frac_class_ind[i][j] = np.size(np.where(tc_ind < small_tc_thresh)[0]) / float(np.size(tc_ind))


    ####################################################################
    plt.figure(1)
    plt.imshow(tanimat_all)
    plt.title('Tanimoto Coefficient Matrix for All %d Samples (combined %d Classes)\nRMS coeff = %.3f;  Median coeff = %.3f; Fraction < %.3f = %.3f' % (ns, nclass, tc_rms_all, med_coeffs_all, small_tc_thresh, small_frac_all))
    plt.colorbar()
    plt.xticks(np.arange(0, ns), labels, rotation=-90)
    plt.yticks(np.arange(0, ns), labels)
    plt.show(block=False)
    save_name = os.path.join(report_root,'TanimotoMatrix_All' + plot_base + '.pdf')
    plt.savefig(save_name, bbox_inches='tight', dpi=480)

    if pred_type == 'classification':
        for i in range(0, nclass):
            plt.figure(i + 2)
            plt.imshow(tanimat[i])
            plt.title(
                'Tanimoto Coefficient Matrix for Class %d (%d Samples)\nRMS coeff = %.3f;  Median coeff = %.3f; Fraction < %.3f = %.3f' % (
                i, ns_class[i], tc_rms_class[i], med_coeffs_class[i], small_tc_thresh, small_frac_class[i]))
            plt.colorbar()
            plt.xticks(np.arange(0, ns_class[i]), labels_per_class[i], rotation=-90)
            plt.yticks(np.arange(0, ns_class[i]), labels_per_class[i])
            do_block = False
            if i == (nclass - 1):
                do_block = True
            save_name = os.path.join(report_root, 'TanimotoMatrix_Class' + str(i) + '_' + plot_base + '.pdf')
            plt.savefig(save_name, bbox_inches='tight', dpi=480)

            plt.show(block=do_block)


    ###########################################################################
    # Write out the matrices and stats to file
    f = open(report_name,'w')
    f.write('Tanimoto Similarity Report for Features in %s\n\n' % (plot_base))
    f.write('# molecules (total),%d\n' % (ns))
    if pred_type == 'classification':
        for i in range(0, nclass):
            f.write('Class %d molecules,%d\n' % (i,ns_class[i]))
    f.write('\n\n')
    f.write('Tanimoto coefficient statistics for all molecules compared to all molecules:\n')
    f.write('RMS TC,%.3f\n' % (tc_rms_all))
    f.write('Median TC,%.3f\n' % (med_coeffs_all))
    f.write('Fraction of TCs < %.3f,%.3f\n' % (small_tc_thresh,small_frac_all))
    if pred_type == 'classification':
        for i in range(0, nclass):
            f.write('TC stats for Class %d molecules only:\n' % (i))
            f.write('RMS TC,%.3f\n' % (tc_rms_class[i]))
            f.write('Median TC,%.3f\n' % (med_coeffs_class[i]))
            f.write('Fraction of TCs < %.3f,%.3f\n' % (small_tc_thresh, small_frac_class[i]))
    f.write('\n\n\n')

    f.write('Tanimoto similarity of Individual Molecules vs. All other molecules:\n')
    f.write('Molecule,RMS TC,Median TC,Frac TC < %.2f\n' % (small_tc_thresh))
    for i in range(0, ns):
        f.write('%s,%.3f,%.3f,%.3f,\n' % (labels[i], rms_tc_ind[i], med_tc_ind[i], small_frac_ind[i]))
    f.write('\n\n')

    if pred_type == 'classification':
        for i in range(0, nclass):
            f.write('Tanimoto similarity of Individual Molecules vs. All other Class %d molecules:\n' % (i) )
            f.write('Molecule,RMS TC,Median TC,Frac TC < %.2f\n' % (small_tc_thresh))
            for j in range(0, ns_class[i]):
                f.write('%s,%.3f,%.3f,%.3f,\n' % (labels_per_class[i][j], rms_tc_class_ind[i][j], med_tc_class_ind[i][j], small_frac_class_ind[i][j]))
            f.write('\n\n')
    f.write('\n\n\n')

    f.write('Tanimoto similarity matrix (all molecules vs. all molecules)\n')
    for i in range(0, ns):
        f.write(',%s' % (labels[i]))
    f.write('\n')
    for i in range(0,ns):
        f.write('%s' % (labels[i]))
        for j in range(0,ns):
            if j <= i:
                f.write(',')
            else:
                f.write(',%.3f' % (tanimat_all[i,j]))
        f.write('\n')
    f.write('\n\n\n')

    if pred_type == 'classification':
        for i in range(0, nclass):
            f.write('Tanimoto similarity matrix for Class %d\n' % (i))
            for j in range(0, ns_class[i]):
                f.write(',%s' % (labels_per_class[i][j]))
            f.write('\n')
            for j in range(0, ns_class[i]):
                f.write('%s' % (labels_per_class[i][j]))
                for k in range(0, ns_class[i]):
                    if k <= j:
                        f.write(',')
                    else:
                        f.write(',%.3f' % (tanimat[i][j,k]))
                f.write('\n')
            f.write('\n\n\n')

    f.close()
    return ( )


########################################################################################################################
# Writes out the comprehensive report for the model stress test process.  This includes results from all permutations
#   of features, hyperparameters, test sets, and noise additions
def model_stress_test_report(all_model_save_files, model_labels, report_name):

    n_models = len(all_model_save_files)

    base_models = mp.load_model(all_model_save_files[0])

    if n_models == 0:
        wrote_report = False
    else:
        # Determine which models go with which stress test steps
        ststepidx = []
        for i in range(0, n_models):
            if 'basecase' in model_labels[i].lower():
                ststepidx.append(0)
            elif 'features' in model_labels[i].lower():
                ststepidx.append(1)
            elif 'hp' in model_labels[i].lower():
                ststepidx.append(2)
            elif 'testset' in model_labels[i].lower():
                ststepidx.append(3)
            elif 'xnoise' in model_labels[i].lower() or 'ynoise' in model_labels[i].lower():
                ststepidx.append(4)
        ststepidx = np.array(ststepidx)

        if base_models.pred_type == 'classification':
            wrote_report = classification_model_stress_test_report(base_models, all_model_save_files, model_labels, report_name, ststepidx)
        elif base_models.pred_type == 'regression':
            wrote_report = regression_model_stress_test_report(base_models, all_model_save_files, model_labels, report_name, ststepidx)

    # Delete all but the base cse model.  This is to save hard-drive space as the models can get huge
    for i in range(1, len(all_model_save_files)):
        # If doing MST for an ANN, delete the .hdf5 files for each unnecessary model
        if base_models.ml_type == 'ann':
            cur_models = mp.load_model(all_model_save_files[i])
            file_root, base_file = os.path.split(all_model_save_files[i])
            for j in range(len(cur_models.model_filenames)):
                cur_file = os.path.join(file_root, cur_models.model_filenames[j])
                os.remove(cur_file)

        # Delete the .pkl file
        os.remove(all_model_save_files[i])

    return( wrote_report)


########################################################################################################################
def classification_model_stress_test_report(base_models, all_model_save_files, model_labels, report_name, ststepidx):
    wrote_report = False
    n_models = len(all_model_save_files)
    if n_models == 0:
        return(wrote_report)

    head, tail = os.path.split(report_name)

    # We don't load all models into RAM- not enough RAM for some of the bg model types (deep ANNs)
    # Instead, load a model one at a time and store the needed information in separate containers
    all_test_stats = []
    all_valid_stats = []
    all_hp = []
    all_feats = []
    all_nfeats = []
    all_preds = []
    for i in range(n_models):
        cur_models = mp.load_model(all_model_save_files[i])
        if i == 0:
            base_models = cur_models
        all_test_stats.append(copy.deepcopy(cur_models.test_stats))
        all_valid_stats.append(copy.deepcopy(cur_models.valid_stats))
        all_hp.append(copy.deepcopy(cur_models.hp))
        all_nfeats.append(copy.deepcopy(cur_models.n_feats))
        all_feats.append(copy.deepcopy(cur_models.feats))
        all_preds.append(copy.deepcopy(cur_models.preds))


    f = open(report_name,'w')
    f.write('Model Stress Test Report for %s\n\n' % (tail))
    f.write('Unless otherwise noted the random number generator seed was set so that the same random samples (and test sets) were used for all comparisons\n\n\n')
    f.write('\nBasic Sample Count Information\n')
    f.write('NOTE: the sample counts are "expected" sample counts.  Actual counts for any individual model may vary\n')
    f.write('\n')
    for t in range(base_models.n_tasks):
        f.write('Sample size information for task %s (%d of %d):\n' % (base_models.task_names[t], t+1, base_models.n_tasks))
        f.write('Data Set Sizes,#Samples,')
        for i in range(0, base_models.n_class[t]):
            f.write('# Class %d,' % (i))
        f.write('\n')

        train_ratio = 1.0 - base_models.valid_ratio
        n_train_task = int(np.round(train_ratio * np.sum(base_models.ns_class[t])))
        f.write('# Training,%d,' % (n_train_task))
        n_train_task_perclass = np.int32(np.round(train_ratio * base_models.ns_class[t]))
        for i in range(0, base_models.n_class[t]):
            f.write('%d,' % (n_train_task_perclass[i]))
        f.write('\n')

        n_valid_task = int(np.round(base_models.valid_ratio * np.sum(base_models.ns_class[t])))
        f.write('# Validation,%d,' % (n_valid_task))
        n_valid_task_perclass = np.int32(np.round(base_models.valid_ratio * base_models.ns_class[t]))
        for i in range(0, base_models.n_class[t]):
            f.write('%d,' % (n_valid_task_perclass[i]))
        f.write('\n')

        if base_models.n_test > 0:
            n_test_task = int(np.round(base_models.test_ratio * np.sum(base_models.ns_class[t])))
            f.write('# Test,%d,' % (n_test_task))
            n_test_task_perclass = np.int32(np.round(base_models.test_ratio * base_models.ns_class[t]))
            for i in range(0, base_models.n_class[t]):
                f.write('%d,' % (n_test_task_perclass[i]))
            f.write('\n')
        f.write('# Missing Acts,%d\n\n\n' % (base_models.n_missing[t]))


    f.write('>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Part 0: Base Case Model Performance <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('Base Case Model Settings\n')
    f.write('********** Feature Settings **********\n')
    f.write('Feature type,%s\n' % (base_models.feats_type))
    f.write('Total # of features used,%d\n' % (base_models.n_feats))

    if base_models.feats_type == 'ecfp':
        f.write('FP radius,%d\n' % (base_models.feats.fp_radius))
        f.write('n_bits,%d\n' % (base_models.feats.n_bits))
        if base_models.feats.use_fcfp:
            f.write('Use FCFP?,True\n')
        else:
            f.write('Use FCFP?,False\n')
    f.write('\n\n')

    f.write('********** Machine Learning Hyperparameters **********\n')
    f.write('Machine learning type,%s\n' % (base_models.ml_type))

    if base_models.ml_type == 'pls':
        f.write('# components,%d\n' % (base_models.hp.n_comp))
        f.write('# iterations,%d\n' % (base_models.hp.n_iters))
    elif base_models.ml_type == 'ann':
        f.write('Activation function,%s\n' % (base_models.hp.act_fn))
        f.write('# Iterations,%d\n' % (base_models.hp.n_iters))
        f.write('Learning rate,%f\n' % (base_models.hp.learning_rate))
        f.write('Learning rule,%s\n' % (base_models.hp.learning_rule))
        f.write('Learning momentum,%f\n' % (base_models.hp.learning_momentum))
        f.write('Dropout,%f\n' % (base_models.hp.dropout))
        f.write('Nneur,%d\n' % (base_models.hp.n_neur))
    elif base_models.ml_type == 'knn':
        f.write('k,%d\n' % (base_models.hp.k))
        f.write('Distance type,%s\n' % (base_models.hp.distance_type))
        f.write('Weight type,%s\n' % (base_models.hp.weight_type))
    elif base_models.ml_type == 'xgboost':
        f.write('Max depth,%d\n' % (base_models.hp.max_depth))
        f.write('Learning rate,%f\n' % (base_models.hp.learning_rate))
        f.write('n_estimators,%d\n' % (base_models.hp.n_estimators))
        f.write('objective fn,%s\n' % (base_models.hp.objective))
    f.write('\n\n')

    f.write('Performance Statistics for Base Case\n')
    for t in range(base_models.n_tasks):
        f.write('\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MST Results for Task %s (%d of %d) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n' % (
        base_models.task_names[t], t + 1, base_models.n_tasks))
        if base_models.n_test > 0:
            f.write('***** Test Set Statistics *****,,,,***** Validation Set Statistics *****\n')
            f.write('Accy(test),%.3f,,,Accy(validation),%.3f\n' % (base_models.test_stats[t].accuracy, base_models.valid_stats[t].accuracy))
            f.write('Confusion Matrix:,,,,Confusion Matrix:\n')
            f.write('Accy(test),%.3f,,,Accy(validation),%.3f\n' % (base_models.test_stats[t].accuracy, base_models.valid_stats[t].accuracy))
            f.write('Confusion Matrix:,,,,Confusion Matrix:\n')
        else:
            # No test set- validation only
            f.write('***** Validation Set Statistics *****\n')
            f.write('Pcc(validation),%.3f\n' % (base_models.valid_stats[t].accuracy))
            f.write('Confusion Matrix:\n')

        for i in range(0,base_models.n_class[t]):
            if base_models.n_test > 0:
                for j in range(0,base_models.n_class[t]):
                    f.write('%d,' % (base_models.test_stats[t].CM[i,j]))
                f.write(',,')
            for j in range(0,base_models.n_class[t]):
                f.write('%d,' % (base_models.valid_stats[t].CM[i,j]))
            f.write('\n')
        if base_models.n_test > 0:
            f.write('Conditional Probability Matrix:,,,,Conditional Probability Matrix:\n')
        else:
            f.write('Conditional Probability Matrix:\n')
        for i in range(0,base_models.n_class[t]):
            if base_models.n_test:
                for j in range(0,base_models.n_class[t]):
                    f.write('%.3f,' % (base_models.test_stats[t].Pmat[i,j]))
                f.write(',,')
            for j in range(0,base_models.n_class[t]):
                f.write('%.3f,' % (base_models.valid_stats[t].Pmat[i,j]))
            f.write('\n')
        f.write('# samples per class,')

        if base_models.n_test > 0:
            for i in range(0,base_models.n_class[t]):
                f.write('%d,' % (base_models.test_stats[t].ns_class[i]))
            f.write(',')
        f.write('# samples per class,')
        for i in range(0,base_models.n_class[t]):
            f.write('%d,' % (base_models.valid_stats[t].ns_class[i]))
        if base_models.n_test > 0:
            f.write('\nTotal # Samples,%d,,,Total # Samples,%d\n' % (np.sum(base_models.test_stats[t].ns_class),np.sum(base_models.valid_stats[t].ns_class)))
        else:
            f.write('\nTotal # Samples,%d\n' % (np.sum(base_models.valid_stats[t].ns_class)))

        if base_models.n_test > 0:
            f.write('Test Summary Stats:,,,,Validation Summary Stats:,\n')
            f.write(',accuracy,%.3f,,,accuracy,%.3f\n' % (base_models.test_stats[t].accuracy, base_models.valid_stats[t].accuracy))
            if base_models.n_class[t] == 2:
                f.write(',specificity,%.3f,,,specificity,%.3f\n' % (base_models.test_stats[t].specificity, base_models.valid_stats[t].specificity))
                f.write(',sensitivity,%.3f,,,sensitivity,%.3f\n' % (base_models.test_stats[t].sensitivity, base_models.valid_stats[t].sensitivity))
                f.write(',PPV,%.3f,,,PPV,%.3f\n' % (base_models.test_stats[t].PPV, base_models.valid_stats[t].PPV))
                f.write(',NPV,%.3f,,,NPV,%.3f\n' % (base_models.test_stats[t].NPV, base_models.valid_stats[t].NPV))
                if base_models.test_stats[t].auc is not None:
                    f.write(',AUC,%.3f,,,AUC,%.3f\n' % (base_models.test_stats[t].auc, base_models.valid_stats[t].auc))
            else:
                for i in range(0, base_models.n_class[t]):
                    f.write(',Pr[%d|%d],' % (i,i))
                    for j in range(0, base_models.n_class[t]):
                        f.write('%.3f,,,%.3f\n' % (base_models.test_stats[t].Pmat[j,j], base_models.valid_stats[t].Pmat[j,j]))

    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 1: Variability with Features Used <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    curidx = np.where(ststepidx == 1)[0]
    ncur = np.size(curidx)
    f.write('Feature Settings,Base Case,')
    for i in range(0, ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')
    f.write('Total # features,%d,' % (base_models.n_feats))
    for i in range(0, ncur):
        f.write('%d,' % (all_nfeats[curidx[i]]))

    if base_models.feats_type == 'ecfp':
        f.write('\nFP radius,%d,' % (base_models.feats.fp_radius))
        for i in range(0, ncur):
            f.write('%d,' % (all_feats[curidx[i]].fp_radius))
        f.write('\nn_bits,%d,' % (base_models.feats.n_bits))
        for i in range(0, ncur):
            f.write('%d,' % (all_feats[curidx[i]].n_bits))
        use_fcfp_str = 'False'
        if base_models.feats.use_fcfp:
            use_fcfp_str = 'True'
        f.write('\nUse FCFP?,%s,' % (use_fcfp_str))
        for i in range(0, ncur):
            use_fcfp_str = 'False'
            if all_feats[curidx[i]].use_fcfp:
                use_fcfp_str = 'True'
            f.write('%s,' % (use_fcfp_str))

    f.write('\nSample training weights,%s,' % (base_models.preds.training_weight_type))
    for i in range(0, ncur):
        f.write('%s,' % (all_preds[curidx[i]].training_weight_type))
    f.write('\n\n\n')

    f = write_stats_block(f, base_models, curidx, test_stats=all_test_stats, valid_stats=all_valid_stats)


    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 2: Variability with Machine Learning Hyperparameters Used <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('To determine the sensitivity in the machine learning hyperparameters (# of neurons- activation function- ANN dropout- etc.) we vary the hyperparameters to determine if our base case results are highly sensitive to the hyperparameters used\n\n')
    curidx = np.where(ststepidx == 2)[0]
    ncur = np.size(curidx)

    f.write('\n,Base Case,')
    for i in range(0,ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')
    if base_models.ml_type == 'pls':
        f.write('n_comp,%d,' % (all_hp[0].n_comp))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_comp))
        f.write('\n')
        f.write('n_iters,%d,' % (all_hp[0].n_iters))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_iters))
        f.write('\n')
    elif base_models.ml_type == 'ann':
        f.write('Activation fn,%s,' % (all_hp[0].act_fn))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].act_fn))
        f.write('\n')
        f.write('# iterations,%d,' % (all_hp[0].n_iters))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_iters))
        f.write('\n')

        f.write('# hidden layers,%d,' % (all_hp[0].n_hidden))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_hidden))
        f.write('\n')
        f.write('# epochs,%d,' % (all_hp[0].n_epochs))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_epochs))
        f.write('\n')

        f.write('Learning rate,%.4f,' % (all_hp[0].learning_rate))
        for i in range(0,ncur):
            f.write('%.4f,' % (all_hp[curidx[i]].learning_rate))
        f.write('\n')
        f.write('Learning rule,%s,' % (all_hp[0].learning_rule))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].learning_rule))
        f.write('\n')
        f.write('Learning momentum,%.4f,' % (all_hp[0].learning_momentum))
        for i in range(0,ncur):
            f.write('%.4f,' % (all_hp[curidx[i]].learning_momentum))
        f.write('\n')
        f.write('Dropout,%.4f,' % (all_hp[0].dropout))
        for i in range(0,ncur):
            f.write('%.4f,' % (all_hp[curidx[i]].dropout))
        f.write('\n')
        f.write('n_neur,%d,' % (all_hp[0].n_neur))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_neur))
        f.write('\n')
    elif base_models.ml_type == 'knn':
        f.write('k,%d,' % (all_hp[0].k))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].k))
        f.write('\n')
        f.write('Distance type,%s,' % (all_hp[0].distance_type))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].distance_type))
        f.write('\n')
        f.write('Weight type,%s,' % (all_hp[0].weight_type))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].weight_type))
        f.write('\n')
    elif base_models.ml_type == 'xgboost':
        f.write('Max depth,%d,' % (all_hp[0].max_depth))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].max_depth))
        f.write('\n')
        f.write('Learning rate,%f,' % (all_hp[0].learning_rate))
        for i in range(0,ncur):
            f.write('%f,' % (all_hp[curidx[i]].learning_rate))
        f.write('\n')
        f.write('n_estimators,%d,' % (all_hp[0].n_estimators))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_estimators))
        f.write('\n')
        f.write('Objective fn,%s,' % (all_hp[0].objective))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].objective))
        f.write('\n')

    f = write_stats_block(f, base_models, curidx, test_stats=all_test_stats, valid_stats=all_valid_stats)


    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 3: Variability with Test Set Used <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('To determine how sensitive results are to the test set used, statistics are computed for several different randomly selected test sets (training and validation sets are likewise randomly selected)\n')
    f.write('In between random test set shuffles, input data features and machine learning hyperparameters are held constant\n\n')
    curidx = np.where(ststepidx == 3)[0]
    ncur = np.size(curidx)
    f.write(',Base Case,')
    for i in range(0, ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')

    f = write_stats_block(f, base_models, curidx, test_stats=all_test_stats, valid_stats=all_valid_stats)


    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 4: Variability with Noise Injected to Features / Activities  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('To determine how sensitive results are to the input data, errors are deliberately introduced into features (X) and in the activities (Y)\n')
    f.write('Errors are NOT introduced into the test set (only in the "internal" test / validation sets)\n\n\n')

    curidx = np.where(ststepidx == 4)[0]
    ncur = np.size(curidx)
    f.write(',Base Case,')
    for i in range(0, ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')
    f = write_stats_block(f, base_models, curidx, test_stats=all_test_stats, valid_stats=None)

    f.close()

    wrote_report = True
    return( wrote_report)


########################################################################################################################
def write_stats_block(f, base_models, useidx, test_stats = None, valid_stats = None):
    ncur = np.size(useidx)

    if base_models.n_test > 0:
    # if test_stats[0].accuracy is not None:

        f.write('\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
        f.write('Test Set Statistics:\n\n')

        for t in range(base_models.n_tasks):
            f.write('\n\nTest Set Statistics for task %s (%d of %d):\n' % (base_models.task_names[t],t+1,base_models.n_tasks))
            f.write('accuracy,%.3f,' % (test_stats[0][t].accuracy))
            for i in range(0,ncur):
                f.write('%.3f,' % (test_stats[useidx[i]][t].accuracy))
            f.write('\n')
            if base_models.n_class[t] == 2:
                f.write('specificity,%.3f,' % (test_stats[0][t].specificity))
                for i in range(0,ncur):
                    f.write('%.3f,' % (test_stats[useidx[i]][t].specificity))
                f.write('\n')
                f.write('sensitivity,%.3f,' % (test_stats[0][t].sensitivity))
                for i in range(0,ncur):
                    f.write('%.3f,' % (test_stats[useidx[i]][t].sensitivity))
                f.write('\n')
                f.write('PPV,%.3f,' % (test_stats[0][t].PPV))
                for i in range(0,ncur):
                    f.write('%.3f,' % (test_stats[useidx[i]][t].PPV))
                f.write('\n')
                f.write('NPV,%.3f,' % (test_stats[0][t].NPV))
                for i in range(0,ncur):
                    f.write('%.3f,' % (test_stats[useidx[i]][t].NPV))
                f.write('\n')
                if base_models.test_stats[t].auc is not None:
                    f.write('AUC,%.3f,' % (test_stats[0][t].auc))
                    for i in range(0,ncur):
                        f.write('%.3f,' % (test_stats[useidx[i]][t].auc))
                    f.write('\n')

            else:
                # non-binary classifier; just print out main diagonal of conditional probability matrix
                for i in range(base_models.n_class[t]):
                    f.write('Pr[%d|%d],%.3f,' % (i,i,test_stats[0][t].Pmat[i,i]))
                    for j in range(0,ncur):
                        f.write('%.3f,' % (test_stats[useidx[j]][t].Pmat[i,i]))
                    f.write('\n')

            f.write('\n')

    if valid_stats is None:
        return(f)

    f.write('\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
    f.write('Validation Set Statistics:\n\n')

    for t in range(base_models.n_tasks):
        f.write('\n\nValidation Set Statistics for task %s (%d of %d):\n' % (base_models.task_names[t], t + 1, base_models.n_tasks))
        f.write('accuracy,%.3f,' % (valid_stats[0][t].accuracy))
        for i in range(0,ncur):
            f.write('%.3f,' % (valid_stats[useidx[i]][t].accuracy))
        f.write('\n')
        if base_models.n_class[t] == 2:
            f.write('specificity,%.3f,' % (valid_stats[0][t].specificity))
            for i in range(0,ncur):
                f.write('%.3f,' % (valid_stats[useidx[i]][t].specificity))
            f.write('\n')
            f.write('sensitivity,%.3f,' % (valid_stats[0][t].sensitivity))
            for i in range(0,ncur):
                f.write('%.3f,' % (valid_stats[useidx[i]][t].sensitivity))
            f.write('\n')
            f.write('PPV,%.3f,' % (valid_stats[0][t].PPV))
            for i in range(0,ncur):
                f.write('%.3f,' % (valid_stats[useidx[i]][t].PPV))
            f.write('\n')
            f.write('NPV,%.3f,' % (valid_stats[0][t].NPV))
            for i in range(0,ncur):
                f.write('%.3f,' % (valid_stats[useidx[i]][t].NPV))
            f.write('\n')
            if valid_stats[0][t].auc is not None:
                f.write('AUC,%.3f,' % (valid_stats[0][t].auc))
                for i in range(0,ncur):
                    f.write('%.3f,' % (valid_stats[useidx[i]][t].auc))
                f.write('\n')
        else:
            # non-binary classifier; just print out main diagonal of conditional probability matrix
            for i in range(base_models.n_class[t]):
                f.write('Pr[%d|%d],%.3f,' % (i,i,valid_stats[0][t].Pmat[i,i]))
                for j in range(0,ncur):
                    f.write('%.3f,' % (valid_stats[useidx[i]][t].Pmat[i,i]))
                f.write('\n')

    return( f )


########################################################################################################################
def write_stats_block_regr(f, base_models, useidx, test_stats = None, valid_stats = None):
    ncur = np.size(useidx)

    ci_string = str(int(np.round(base_models.siglevel * 100))) + '% CI'
    if base_models.n_test > 0:
        f.write('\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
        f.write('Test Set Statistics:\n\n')

        for t in range(base_models.n_tasks):
            f.write('\n\nTest Set Statistics for task %s (%d of %d):\n' % (base_models.task_names[t],t+1,base_models.n_tasks))
            f.write('Rsq(test),%.4f,' % (test_stats[0][t].rsq))
            for i in range(ncur):
                f.write('%.4f,' % (test_stats[useidx[i]][t].rsq))
            f.write('\n')

            f.write('Rsq %s LowerLimit(test),%.4f,' % (ci_string, test_stats[0][t].confint_rsq[0,0]))
            for i in range(ncur):
                f.write('%.4f,' % (test_stats[useidx[i]][t].confint_rsq[0,0]))
            f.write('\n')

            f.write('Rsq %s UpperLimit(test),%.4f,' % (ci_string, test_stats[0][t].confint_rsq[0,1]))
            for i in range(ncur):
                f.write('%.4f,' % (test_stats[useidx[i]][t].confint_rsq[0,1]))
            f.write('\n')

            f.write('RMS Error (test),%.4f,' % (test_stats[0][t].e_rms))
            for i in range(ncur):
                f.write('%.4f,' % (test_stats[useidx[i]][t].e_rms))
            f.write('\n')

            f.write('MaxAbs Error (test),%.4f,' % (test_stats[0][t].e_maxabs))
            for i in range(ncur):
                f.write('%.4f,' % (test_stats[useidx[i]][t].e_maxabs))
            f.write('\n')


    if valid_stats is None:
        return(f)

    f.write('\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
    f.write('Validation Set Statistics:\n\n')

    for t in range(base_models.n_tasks):
        f.write('\n\nValidation Set Statistics for task %s (%d of %d):\n' % (base_models.task_names[t], t + 1, base_models.n_tasks))

        f.write('Rsq(valid),%.4f,' % (valid_stats[0][t].rsq))
        for i in range(ncur):
            f.write('%.4f,' % (valid_stats[useidx[i]][t].rsq))
        f.write('\n')

        f.write('Rsq %s LowerLimit(validation),%.4f,' % (ci_string, valid_stats[0][t].confint_rsq[0, 0]))
        for i in range(ncur):
            f.write('%.4f,' % (valid_stats[useidx[i]][t].confint_rsq[0, 0]))
        f.write('\n')

        f.write('Rsq %s UpperLimit(validation),%.4f,' % (ci_string, valid_stats[0][t].confint_rsq[0, 1]))
        for i in range(ncur):
            f.write('%.4f,' % (valid_stats[useidx[i]][t].confint_rsq[0, 1]))
        f.write('\n')

        f.write('RMS Error (validation),%.4f,' % (valid_stats[0][t].e_rms))
        for i in range(ncur):
            f.write('%.4f,' % (valid_stats[useidx[i]][t].e_rms))
        f.write('\n')

        f.write('MaxAbs Error (validation),%.4f,' % (valid_stats[0][t].e_maxabs))
        for i in range(ncur):
            f.write('%.4f,' % (valid_stats[useidx[i]][t].e_maxabs))
        f.write('\n')

    return( f )



########################################################################################################################
def regression_model_stress_test_report(base_models, all_model_save_files, model_labels, report_name, ststepidx):

    wrote_report = False
    n_models = len(all_model_save_files)
    if n_models == 0:
        return(wrote_report)

    head, tail = os.path.split(report_name)

    # We don't load all models into RAM- not enough RAM for some of the bg model types (deep ANNs)
    # Instead, load a model one at a time and store the needed information in separate containers
    all_test_stats = []
    all_valid_stats = []
    all_hp = []
    all_feats = []
    all_nfeats = []
    all_preds = []
    for i in range(n_models):
        cur_models = mp.load_model(all_model_save_files[i])
        if i == 0:
            base_models = copy.deepcopy(cur_models)
        all_test_stats.append(copy.deepcopy(cur_models.test_stats))
        all_valid_stats.append(copy.deepcopy(cur_models.valid_stats))
        all_hp.append(copy.deepcopy(cur_models.hp))
        all_nfeats.append(copy.deepcopy(cur_models.n_feats))
        all_feats.append(copy.deepcopy(cur_models.feats))
        all_preds.append(copy.deepcopy(cur_models.preds))


    f = open(report_name,'w')
    f.write('Model Stress Test Report for %s\n\n' % (tail))
    f.write('Unless otherwise noted the random number generator seed was set so that the same random samples (and test sets) were used for all comparisons\n\n\n')
    f.write('\nBasic Sample Count Information for Each Task\n')
    f.write('NOTE: the sample counts are "expected" sample counts.  Actual counts for any individual model may vary slightly due to missing values in some tasks\n')
    f.write('\n')

    f.write('Data Set Partitions,')
    for i in range(base_models.n_tasks):
        f.write('%s,' % (base_models.task_names[i]))
    f.write('\n')

    f.write('# Training,')
    for i in range(base_models.n_tasks):
        int_ratio = 1.0 - base_models.test_ratio
        train_ratio = (1.0 - base_models.valid_ratio) * int_ratio
        n_train_task = int(np.round(train_ratio * base_models.n_totalintask[i]))
        f.write('%d,' % (n_train_task))
    f.write('\n')

    f.write('# Validation,')
    for i in range(base_models.n_tasks):
        int_ratio = 1.0 - base_models.test_ratio
        n_valid_task = int(np.round(base_models.valid_ratio * int_ratio * base_models.n_totalintask[i]))
        f.write('%d,' % (n_valid_task))
    f.write('\n')

    if base_models.n_test > 0:
        f.write('# Test,')
        for i in range(base_models.n_tasks):
            n_test_task = int(np.round(base_models.test_ratio * base_models.n_totalintask[i]))
            f.write('%d,' % (n_test_task))
        f.write('\n')

    f.write('# Missing,')
    for i in range(base_models.n_tasks):
        f.write('%d,' % (base_models.n_missing[i]))
    f.write('\n')

    f.write('>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Part 0: Base Case Model Performance <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('Base Case Model Settings\n')
    f.write('********** Feature Settings **********\n')
    f.write('Feature type,%s\n' % (base_models.feats_type))
    f.write('Total # of features used,%d\n' % (base_models.n_feats))
    if base_models.feats_type == 'ecfp':
        f.write('FP radius,%d\n' % (base_models.feats.fp_radius))
        f.write('n_bits,%d\n' % (base_models.feats.n_bits))
        if base_models.feats.use_fcfp:
            f.write('Use FCFP?,True\n')
        else:
            f.write('Use FCFP?,False\n')
    f.write('\n\n')

    f.write('********** Machine Learning Hyperparameters **********\n')
    f.write('Machine learning type,%s\n' % (base_models.ml_type))

    if base_models.ml_type == 'pls':
        f.write('# components,%d\n' % (base_models.hp.n_comp))
        f.write('# iterations,%d\n' % (base_models.hp.n_iters))
    elif base_models.ml_type == 'ann':
        f.write('Activation function,%s\n' % (base_models.hp.act_fn))
        f.write('# Iterations,%d\n' % (base_models.hp.n_iters))
        f.write('Learning rate,%f\n' % (base_models.hp.learning_rate))
        f.write('Learning rule,%s\n' % (base_models.hp.learning_rule))
        f.write('Learning momentum,%f\n' % (base_models.hp.learning_momentum))
        f.write('Dropout,%f\n' % (base_models.hp.dropout))
        f.write('Nneur,%d\n' % (base_models.hp.n_neur))
    elif base_models.ml_type == 'knn':
        f.write('k,%d\n' % (base_models.hp.k))
        f.write('Distance type,%s\n' % (base_models.hp.distance_type))
        f.write('Weight type,%s\n' % (base_models.hp.weight_type))
    elif base_models.ml_type == 'xgboost':
        f.write('Max depth,%d\n' % (base_models.hp.max_depth))
        f.write('Learning rate,%f\n' % (base_models.hp.learning_rate))
        f.write('n_estimators,%d\n' % (base_models.hp.n_estimators))
        f.write('objective fn,%s\n' % (base_models.hp.objective))
    f.write('\n\n')

    f.write('Performance Statistics for Base Case\n')
    ci_string = str(int(np.round(base_models.siglevel * 100))) + '% CI'
    for t in range(base_models.n_tasks):
        f.write('\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MST Results for Task %s (%d of %d) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n' % (
        base_models.task_names[t], t + 1, base_models.n_tasks))
        if base_models.n_test > 0:
            f.write('***** Test Set Statistics *****,,,,***** Validation Set Statistics *****\n')
            f.write('Rsq(test),%.4f,,,Rsq(validation),%.4f\n' % (base_models.test_stats[t].rsq,base_models.valid_stats[t].rsq))
            f.write('Rsq %s LowerLimit (test),%.4f,,,Rsq %s LowerLimit (validation),%.4f\n' % (ci_string,base_models.test_stats[t].confint_rsq[0,0],
                                                                                       ci_string,base_models.valid_stats[t].confint_rsq[0,0]))
            f.write('Rsq %s UpperLimit (test),%.4f,,,Rsq %s UpperLimit (validation),%.4f\n' % (ci_string,base_models.test_stats[t].confint_rsq[0,1],
                                                                                       ci_string,base_models.valid_stats[t].confint_rsq[0,1]))
            f.write('RMS Error (test),%.4f,,,RMS Error (validation),%.4f\n' % (base_models.test_stats[t].e_rms, base_models.valid_stats[t].e_rms))
            f.write('MaxAbs Error (test),%.4f,,,MaxAbs (validation),%.4f\n' % (base_models.test_stats[t].e_maxabs, base_models.valid_stats[t].e_maxabs))


        else:
            # No test set- validation only
            f.write('***** Validation Set Statistics *****\n')
            f.write('Rsq(validation),%.3f\n' % (base_models.valid_stats[t].rsq))
            f.write('Rsq %s LowerLimit (validation),%.4f\n' % (ci_string, base_models.valid_stats[t].confinf_rsq[0, 0]))
            f.write('Rsq %s UpperLimit (validation),%.4f\n' % (ci_string, base_models.valid_stats[t].confinf_rsq[0, 1]))
            f.write('RMS Error (validation),%.3f\n' % (base_models.valid_stats[t].e_rms))
            f.write('MaxAbs Error (validation),%.3f\n' % (base_models.valid_stats[t].e_maxabs))

        f.write('\n')

    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 1: Variability with Features Used <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    curidx = np.where(ststepidx == 1)[0]
    ncur = np.size(curidx)
    f.write('Feature Settings,Base Case,')
    for i in range(0, ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')
    f.write('Total # features,%d,' % (base_models.n_feats))
    for i in range(0, ncur):
        f.write('%d,' % (all_nfeats[curidx[i]]))

    if base_models.feats_type == 'ecfp':
        f.write('\nFP radius,%d,' % (base_models.feats.fp_radius))
        for i in range(0, ncur):
            f.write('%d,' % (all_feats[curidx[i]].fp_radius))
        f.write('\nn_bits,%d,' % (base_models.feats.n_bits))
        for i in range(0, ncur):
            f.write('%d,' % (all_feats[curidx[i]].n_bits))
        use_fcfp_str = 'False'
        if base_models.feats.use_fcfp:
            use_fcfp_str = 'True'
        f.write('\nUse FCFP?,%s,' % (use_fcfp_str))
        for i in range(0, ncur):
            use_fcfp_str = 'False'
            if all_feats[curidx[i]].use_fcfp:
                use_fcfp_str = 'True'
            f.write('%s,' % (use_fcfp_str))

    f.write('\nSample training weights,%s,' % (base_models.preds.training_weight_type))
    for i in range(0, ncur):
        f.write('%s,' % (all_preds[curidx[i]].training_weight_type))
    f.write('\n\n\n')

    f = write_stats_block_regr(f, base_models, curidx, test_stats=all_test_stats, valid_stats=all_valid_stats)


    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 2: Variability with Machine Learning Hyperparameters Used <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('To determine the sensitivity in the machine learning hyperparameters (# of neurons- activation function- ANN dropout- etc.) we vary the hyperparameters to determine if our base case results are highly sensitive to the hyperparameters used\n\n')
    curidx = np.where(ststepidx == 2)[0]
    ncur = np.size(curidx)

    f.write('\n,Base Case,')
    for i in range(0,ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')
    if base_models.ml_type == 'pls':
        f.write('n_comp,%d,' % (all_hp[0].n_comp))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_comp))
        f.write('\n')
        f.write('n_iters,%d,' % (all_hp[0].n_iters))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_iters))
        f.write('\n')
    elif base_models.ml_type == 'ann':
        f.write('Activation fn,%s,' % (all_hp[0].act_fn))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].act_fn))
        f.write('\n')
        f.write('# iterations,%d,' % (all_hp[0].n_iters))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_iters))
        f.write('\n')

        f.write('# hidden layers,%d,' % (all_hp[0].n_hidden))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_hidden))
        f.write('\n')
        f.write('# epochs,%d,' % (all_hp[0].n_epochs))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_epochs))
        f.write('\n')

        f.write('Learning rate,%.4f,' % (all_hp[0].learning_rate))
        for i in range(0,ncur):
            f.write('%.4f,' % (all_hp[curidx[i]].learning_rate))
        f.write('\n')
        f.write('Learning rule,%s,' % (all_hp[0].learning_rule))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].learning_rule))
        f.write('\n')
        f.write('Learning momentum,%.4f,' % (all_hp[0].learning_momentum))
        for i in range(0,ncur):
            f.write('%.4f,' % (all_hp[curidx[i]].learning_momentum))
        f.write('\n')
        f.write('Dropout,%.4f,' % (all_hp[0].dropout))
        for i in range(0,ncur):
            f.write('%.4f,' % (all_hp[curidx[i]].dropout))
        f.write('\n')
        f.write('n_neur,%d,' % (all_hp[0].n_neur))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_neur))
        f.write('\n')
    elif base_models.ml_type == 'knn':
        f.write('k,%d,' % (all_hp[0].k))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].k))
        f.write('\n')
        f.write('Distance type,%s,' % (all_hp[0].distance_type))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].distance_type))
        f.write('\n')
        f.write('Weight type,%s,' % (all_hp[0].weight_type))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].weight_type))
        f.write('\n')
    elif base_models.ml_type == 'xgboost':
        f.write('Max depth,%d,' % (all_hp[0].max_depth))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].max_depth))
        f.write('\n')
        f.write('Learning rate,%f,' % (all_hp[0].learning_rate))
        for i in range(0,ncur):
            f.write('%f,' % (all_hp[curidx[i]].learning_rate))
        f.write('\n')
        f.write('n_estimators,%d,' % (all_hp[0].n_estimators))
        for i in range(0,ncur):
            f.write('%d,' % (all_hp[curidx[i]].n_estimators))
        f.write('\n')
        f.write('Objective fn,%s,' % (all_hp[0].objective))
        for i in range(0,ncur):
            f.write('%s,' % (all_hp[curidx[i]].objective))
        f.write('\n')

    f = write_stats_block_regr(f, base_models, curidx, test_stats=all_test_stats, valid_stats=all_valid_stats)

    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 3: Variability with Test Set Used <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('To determine how sensitive results are to the test set used, statistics are computed for several different randomly selected test sets (training and validation sets are likewise randomly selected)\n')
    f.write('In between random test set shuffles, input data features and machine learning hyperparameters are held constant\n\n')
    curidx = np.where(ststepidx == 3)[0]
    ncur = np.size(curidx)
    f.write(',Base Case,')
    for i in range(0, ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')

    f = write_stats_block_regr(f, base_models, curidx, test_stats=all_test_stats, valid_stats=all_valid_stats)

    f.write('\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Stress Test Step 4: Variability with Noise Injected to Features / Activities  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n')
    f.write('To determine how sensitive results are to the input data, errors are deliberately introduced into features (X) and in the activities (Y)\n')
    f.write('Errors are NOT introduced into the test set (only in the "internal" test / validation sets)\n\n\n')

    curidx = np.where(ststepidx == 4)[0]
    ncur = np.size(curidx)
    f.write(',Base Case,')
    for i in range(0, ncur):
        f.write('%s,' % (model_labels[curidx[i]]))
    f.write('\n')
    f = write_stats_block_regr(f, base_models, curidx, test_stats=all_test_stats, valid_stats=None)

    f.close()

    wrote_report = True
    return( wrote_report)


########################################################################################################################
def write_feature_matrix_to_file(X, filename, row_names=None, col_names=None):
    nrows = np.size(X, 0)
    ncols = np.size(X, 1)

    if row_names is not None:
        if len(row_names) != nrows:
            exit('Mismatch in # of feature rows and # of feature row names!!!')
    if col_names is not None:
        if len(col_names) != nrows:
            exit('Mismatch in # of feature rows and # of feature row names!!!')

    try:

        nbin_occs_feature = np.sum(X, 0)
        nbin_occs_mol = np.sum(X,1)

        Xbin = copy.deepcopy(X)
        Xbin[Xbin>0] = 1
        nmolbin_occs_feature = np.sum(Xbin,0)
        nuniquebin_occs_mol = np.sum(Xbin, 1)

        f = open(filename, 'w')
        # Header row
        f.write(',# Used Features Per Mol,# Unique Occupied Features Per Mol')
        for i in range(ncols):
            if col_names is None:
                curcolname = 'Feature ' + str(i)
            else:
                curcolname = col_names[i]
            f.write(',%s' % (curcolname))

        f.write('\n')

        for i in range(nrows):
            if row_names is None:
                currowname  ='Molecule ' + str(i)
            else:
                currowname = row_names[i]
            f.write('%s,%d,%d,' % (currowname, nbin_occs_mol[i], nuniquebin_occs_mol[i]))
            for j in range(ncols):
                f.write('%f,' % (X[i,j]))
            f.write('\n')

        # Write out some summary stats to file
        f.write('\n\n,,# Mols w Occupied Bin')
        for i in range(ncols):
            f.write(',%d' % (nmolbin_occs_feature[i]))
        f.write('\n,,Total # Bin Occupancies')
        for i in range(ncols):
            f.write(',%d' % (nbin_occs_feature[i]))

        f.close()
    except:
        print('Cannot write feature matrix to file: %s!!!' % (filename))

    return (True)


########################################################################################################################
# Generate output files of the most significant features
def write_sig_features_files(report_name, models, X_int, Y_int, X_test, Y_test):
    n_files_written = 0

    path_head, path_tail = os.path.split(report_name)
    base_file = path_tail.split('.')

    # Using scikit-learn's built in feature_importances_ algorithm, which uses "Gini importance" or "mean decrease impurity"
    #   algorithms
    if models.ml_type == 'xgboost':
        filename = os.path.join(path_head, 'GiniImportantFeatures_' + models.ml_type + '_' + models.feats_type + '_' + base_file[0] + '.csv')
        wrote_gini_file = write_gini_feature_importances_file(models, filename)
        n_files_written += 1


    return( n_files_written)



########################################################################################################################
# Write out the most important features, as calculated by Scikit-learn's Gini importance algorithm.  Feature importances
#   must be calculated and stores in a MODELS structure before calling this function
def write_gini_feature_importances_file(models, feature_importance_file, n_top_feats = 50):

    if models.n_feats < n_top_feats:
        n_top_feats = models.n_feats

    f = open(feature_importance_file,'w')

    f.write('Most Important %d Features from Gini Importance Algorithm\n,Importances Averaged Across All %d Models and %d Tasks,\n\n' %
            (n_top_feats,models.n_models, models.n_tasks))

    all_mostimpidx = []
    all_meanimps = []
    all_stdimps = []
    for i in range(models.n_tasks):
        f.write('\n%d Most Important Features for Task %s (%d of %d),\n' % (n_top_feats, models.task_names[i], i+1,models.n_tasks))
        f.write('Feats Idx,Mean Import,StdDev Import,\n')
        imps_mat_task = np.zeros((models.n_models, models.n_feats))
        for j in range(models.n_models):
            imps_mat_task[j,:] = np.squeeze(models.all_models[j][i].feature_importances_)

        mean_imps = np.mean(imps_mat_task, 0)
        std_imps = np.std(imps_mat_task,0)

        sortidx = np.argsort(mean_imps)[::-1]
        all_mostimpidx.append(sortidx[0:n_top_feats])
        all_meanimps.append(mean_imps[sortidx[0:n_top_feats]])
        all_stdimps.append(std_imps[sortidx[0:n_top_feats]])

        for j in range(n_top_feats):
            f.write('%d,%.6f,%.6f,\n' % (sortidx[j], mean_imps[sortidx[j]], std_imps[sortidx[j]]))

    f.close()

    return(True)


########################################################################################################################
def tanimoto_similarity_report_train_test(report_name, X_train, Y_train, X_test, Y_test, names_train, names_test,
                                          pred_type = 'classification', n_class = 2):

    n_train = np.size(X_train,0)
    n_feats = np.size(X_train,1)
    n_test = np.size(X_test,0)

    if pred_type == 'classification':
        trainclassidx = []
        testclassidx = []
        n_inclass_train = np.zeros(n_class, dtype=np.int32)
        n_inclass_test = np.zeros(n_class, dtype=np.int32)
        for i in range(n_class):
            curidx = np.where(Y_train == i)[0]
            trainclassidx.append(copy.deepcopy(curidx))
            n_inclass_train[i] = np.size(curidx)

            curidx = np.where(Y_test == i)[0]
            testclassidx.append(copy.deepcopy(curidx))
            n_inclass_test[i] = np.size(curidx)


    # Compare Tanimoto similarity of each test set molecule to training set (entire set and individually for each class)
    test_mean_all = np.zeros(n_test)
    test_std_all = np.zeros(n_test)
    idxused = []
    test_sorted_tc = []
    test_sorted_names = []
    if pred_type == 'classification':
        test_mean_class = np.zeros((n_test, n_class))
        test_std_class = np.zeros((n_test, n_class))

    for i in range(n_test):
        if pred_type == 'classification':
            tc_class = []
            tc_all = []
            for j in range(n_class):
                curtc_class = np.zeros(n_inclass_train[j])
                for k in range(n_inclass_train[j]):
                    curtc_class[k] = ml.tanimoto_coefficient(X_test[i,:], X_train[trainclassidx[j][k],:])
                tc_class.append(copy.deepcopy(curtc_class))
                if j == 0:
                    tc_all = copy.deepcopy(curtc_class)
                    idxused = copy.deepcopy(trainclassidx[j])

                else:
                    tc_all = np.concatenate((tc_all, curtc_class))
                    idxused = np.concatenate((idxused, trainclassidx[j]))

                test_mean_class[i,j] = np.mean(curtc_class)
                test_std_class[i,j] = np.std(curtc_class)


        else:
            tc_all = np.zeros(n_train)
            for j in range(n_train):
                tc_all[j] = ml.tanimoto_coefficient(X_test[i, :], X_train[j, :])
            idxused = np.arange(0, n_train, dtype=np.int32)

        test_mean_all[i] = np.mean(tc_all)
        test_std_all[i] = np.std(tc_all)
        curclosestidx = np.argsort(tc_all)[::-1]
        test_sorted_tc.append(tc_all[curclosestidx])
        cur_names = []
        [cur_names.append(copy.deepcopy(names_train[idxused[curclosestidx[j]]])) for j in range(n_train)]
        test_sorted_names.append(copy.deepcopy(cur_names))

    # Some bulk statistics for the entire test set
    test_mean_of_means = np.mean(test_mean_all)
    if pred_type == 'classification':
        test_mean_of_classes = np.zeros((n_class,n_class))
        test_std_of_classes = np.zeros((n_class,n_class))
        for i in range(n_class):
            for j in range(n_class):
                test_mean_of_classes[i,j] = np.mean(test_mean_class[testclassidx[i],j])
                test_std_of_classes[i, j] = np.std(test_mean_class[testclassidx[i], j])


    # Compare Tanimoto similarity of each training set molecule to test set (entire set and individually for each class)
    train_mean_all = np.zeros(n_train)
    train_std_all = np.zeros(n_train)
    if pred_type == 'classification':
        train_mean_class = np.zeros((n_train, n_class))
        train_std_class = np.zeros((n_train, n_class))

    for i in range(n_train):
        if pred_type == 'classification':
            tc_class = []
            tc_all = []
            for j in range(n_class):
                curtc_class = np.zeros(n_inclass_test[j])
                for k in range(n_inclass_test[j]):
                    curtc_class[k] = ml.tanimoto_coefficient(X_train[i,:], X_test[testclassidx[j][k],:])
                tc_class.append(copy.deepcopy(curtc_class))
                if j == 0:
                    tc_all = copy.deepcopy(curtc_class)
                else:
                    tc_all = np.concatenate((tc_all, curtc_class))

                train_mean_class[i,j] = np.mean(curtc_class)
                train_std_class[i,j] = np.std(curtc_class)

        else:
            tc_all = np.zeros(n_test)
            for j in range(n_test):
                tc_all[j] = ml.tanimoto_coefficient(X_train[i, :], X_test[j, :])

        train_mean_all[i] = np.mean(tc_all)
        train_std_all[i] = np.std(tc_all)

    # Some bulk statistics for the entire training set
    train_mean_of_means = np.mean(train_mean_all)
    if pred_type == 'classification':
        train_mean_of_classes = np.zeros((n_class,n_class))
        train_std_of_classes = np.zeros((n_class,n_class))
        for i in range(n_class):
            for j in range(n_class):
                train_mean_of_classes[i,j] = np.mean(train_mean_class[trainclassidx[i], j])
                train_std_of_classes[i, j] = np.std(train_mean_class[trainclassidx[i], j])


    path, basefile = os.path.split(report_name)
    f = open(report_name,'w')
    f.write('Tanimoto Similarity Coefficient Statistics for %s\n\n' % (basefile))
    f.write('Prediction Type:,%s\n' % (pred_type))
    f.write('# Training Samples,%d\n' % (n_train))
    if pred_type == 'classification':
        for i in range(n_class):
            f.write(',# Class %d Train Samples,%d\n' % (i,n_inclass_train[i]))
    f.write('# Test Samples,%d\n' % (n_test))
    if pred_type == 'classification':
        for i in range(n_class):
            f.write(',# Class %d Test Samples,%d\n' % (i,n_inclass_test[i]))
    f.write('\n\n\n')

    f.write('*******************************************************************************************************\n')
    f.write('Tanimoto Similarity statistics: comparing each TEST set molecule to all molecules in training set\n')

    f.write('Population Tanimoto Similarity Statistics:\n')
    f.write('Mean TC,%.4f,(all test samples vs. all training samples)\n' % (test_mean_of_means))
    if pred_type == 'classification':
        f.write('\nMean Tanimoto Similarities (per class)\n')
        for i in range(n_class):
            f.write(',Train Class %d' % (i))
        f.write('\n')
        for i in range(n_class):
            f.write('Test Class %d' % (i))
            for j in range(n_class):
                f.write(',%.4f' % (test_mean_of_classes[i,j]))
            f.write('\n')
        f.write('\n')

        f.write('\nStd Dev of Tanimoto Similarities (per class)\n')
        for i in range(n_class):
            f.write(',Train Class %d' % (i))
        f.write('\n')
        for i in range(n_class):
            f.write('Test Class %d' % (i))
            for j in range(n_class):
                f.write(',%.4f' % (test_std_of_classes[i,j]))
            f.write('\n')
        f.write('\n')


    f.write('\nTanimoto Similarities for Each Test Set Molecule Compared to Training Set Molecules:\n')
    f.write('Test Mol Name,Class Label,Mean (all train),StdDev (all train),')
    if pred_type == 'classification':
        for i in range(n_class):
            f.write('Mean (Train Class %d),StdDev (Train Class %d),' % (i,i))
    f.write('\n')
    for i in range(n_test):
        f.write('%s,%d,%.4f,%.4f,' % (names_test[i],Y_test[i],test_mean_all[i],test_std_all[i]))
        if pred_type == 'classification':
            for j in range(n_class):
                f.write('%.4f,%.4f,' % (test_mean_class[i,j], test_std_class[i,j]))
        f.write('\n')

    f.write('\n\n\nThe most similar TRAINING set molecules to each TEST set molecule (and Tanimoto similarities):\n')
    f.write('Test Mol Name')
    for i in range(n_train):
        f.write(',#%d Closest Train Mol,#%d Closest TC' % (i+1,i+1))
    f.write('\n')
    for i in range(n_test):
        f.write('%s' % (names_test[i]))
        for j in range(n_train):
            f.write(',%s,%.4f' % (test_sorted_names[i][j],test_sorted_tc[i][j]))
        f.write('\n')


    f.write('\n\n\n\n')
    for i in range(0,2):
        f.write('*******************************************************************************************************\n')
    f.write('Tanimoto Similarity statistics: comparing each TRAINING set molecule to all molecules in training set\n')

    f.write('Population Tanimoto Similarity Statistics:\n')
    f.write('Mean TC,%.4f,(all training samples vs. all test samples)\n' % (train_mean_of_means))
    if pred_type == 'classification':
        f.write('\nMean Tanimoto Similarities (per class)\n')
        for i in range(n_class):
            f.write(',Test Class %d' % (i))
        f.write('\n')
        for i in range(n_class):
            f.write('Train Class %d' % (i))
            for j in range(n_class):
                f.write(',%.4f' % (train_mean_of_classes[i,j]))
            f.write('\n')
        f.write('\n')

        f.write('\nStd Dev of Tanimoto Similarities (per class)\n')
        for i in range(n_class):
            f.write(',Test Class %d' % (i))
        f.write('\n')
        for i in range(n_class):
            f.write('Train Class %d' % (i))
            for j in range(n_class):
                f.write(',%.4f' % (train_std_of_classes[i,j]))
            f.write('\n')
        f.write('\n')


    f.write('\nTanimoto Similarities for Each Training Set Molecule Compared to Test Set Molecules:\n')
    f.write('Training Mol Name,Class Label,Mean (all test),StdDev (all test),')
    if pred_type == 'classification':
        for i in range(n_class):
            f.write('Mean (Test Class %d),StdDev (Test Class %d),' % (i,i))
    f.write('\n')
    for i in range(n_train):
        f.write('%s,%d,%.4f,%.4f,' % (names_train[i],Y_train[i],train_mean_all[i],train_std_all[i]))
        if pred_type == 'classification':
            for j in range(n_class):
                f.write('%.4f,%.4f,' % (train_mean_class[i,j], train_std_class[i,j]))
        f.write('\n')


    f.close()


    print('\n\nWrote Tanimoto similarity statistics to file: %s' % (report_name))
    return(True)


########################################################################################################################
# Generates an inference report
def classification_inference_report(infer_settings, all_models, all_data, all_class_preds, all_pred_probs):
    success = False
    n_models = len(all_models)
    n_tasks = all_models[0].n_tasks #TODO: replace this with a more general soln. that allows for different nums of tasks per model

    has_activities = False
    if all_data[0].Y is not None:
        if np.size(all_data[0].Y) > 0:
            has_activities = True

    # Performance statistics, if we have class labels
    if has_activities:
        all_stats = []
        for i in range(n_models):
            stats = ml.multitask_perf_stats_class(all_class_preds[i], all_data[i].Y, all_models[i].n_class, pred_probs=all_pred_probs[i])
            all_stats.append(copy.deepcopy(stats))


    # Get all molecules in common across all models
    # Need to get a consolidated list of all molecules to be evaluated by all models
    all_names_sequential = []
    all_names_separate = []
    for i in range(n_models):
        cur_mol_names = []
        for j in range(len(all_data[i].mols)):
            cur_mol_names.append(copy.deepcopy(all_data[i].mols[j].mol_name))
            all_names_sequential.append(copy.deepcopy(all_data[i].mols[j].mol_name))
        all_names_separate.append(copy.deepcopy(cur_mol_names))

    unique_mol_names = list(set(all_names_sequential))

    # For each unique molecule name, get the index of each data set (if any) where the molecule data is stored
    n_unique = len(unique_mol_names)
    matchmat = np.zeros((n_unique,n_models), dtype=np.int32)
    Y_all = np.zeros((n_unique,n_tasks)) * np.nan # create a matrix of Nans
    for t in range(n_tasks):
        for i in range(n_unique):
            for j in range(n_models):
                if unique_mol_names[i] in all_names_separate[j]:
                    # Get the index of the current unique name
                    matchmat[i,j] = all_names_separate[j].index(unique_mol_names[i])
                    if has_activities:
                        useidx = matchmat[i,j]
                        Y_all[i,t] = all_data[j].Y[useidx,t]
                else:
                    matchmat[i,j] = -1

    # Calculate how correlated the predictions are- i.e. "prediction complementarity".  Need to have the same tasks
    pc = None
    if infer_settings.ensemble_type is not None:
        pc = ml.prediction_complementarity(all_class_preds, matchmat, probs=all_pred_probs)


    ###########################################################
    # Ensemble predictions
    used_ensemble = False
    if infer_settings.ensemble_type is not None:
        used_ensemble = True
        ens_stats = []
        print('Generating ensemble predictions...')
        ens_class_preds, ens_pred_probs = ml.ensemble_predictions_class(all_class_preds, all_pred_probs, matchmat,
                                                                 ensemble_type=infer_settings.ensemble_type)
        if has_activities:
            ens_stats = ml.multitask_perf_stats_class(ens_class_preds, Y_all, all_models[0].n_class, pred_probs=ens_pred_probs)


    ###########################################################
    # Write the report
    model_names = []
    task_names = all_models[0].task_names # TODO: replace this for the more general scenario with non-overlapping numbers of tasks per model
    if infer_settings.report_name is None:
        infer_settings.report_name = all_models[0].model_name[0] + "_InferenceReport_" + str(n_models) + "models"
    cur_time = time.strftime('%H-%M-%S')
    cur_date = time.strftime('%m%d%Y')
    report_file = os.path.join(infer_settings.save_root, infer_settings.report_name + '_' + cur_date + '_' + cur_time + '.csv')

    f = open(report_file,'w')
    f.write('CLASSIFICATION MODEL INFERENCE REPORT\n')
    f.write('\n')
    f.write('Predictions from molecules listed in the file: %s\n' % (infer_settings.molecules_file))
    f.write('\n')
    f.write('Models used to generate predictions:\n')
    for i in range(n_models):
        f.write('Model %d,%s\n' % (i, infer_settings.model_files[i]))
        model_path, model_stem = os.path.split(infer_settings.model_files[i])
        model_names.append(copy.deepcopy(model_stem))
    f.write('\n')
    f.write('\n')
    if used_ensemble:
        f.write('Ensemble method used:\n')
        f.write(',%s\n\n' % (infer_settings.ensemble_type))

    if n_models > 1 and pc is not None:
        f.write('Complementarity of individual model predictions\n')
        for t in range(n_tasks):
            f.write('Comparison of model predictions for task #%d:\n' % (t+1))
            for i in range(0,pc[t].n_comps):
                f.write(',Comparison of Model %d vs. Model %d Predictions:\n' % (pc[t].modelscompidx[i, 0],pc[t].modelscompidx[i, 1]))
                f.write(',,Corr Coef of Class Preds,,%.3f\n' % (pc[t].corrcoef_preds[i]))
                for m in range(all_models[0].n_class[t]):
                    for n in range(m,all_models[0].n_class[t]):
                        f.write(',,Corr Coef of (Model %d Class %d) vs. (Model %d Class %d) Probs,,%.3f\n' %
                                (pc[t].modelscompidx[i, 0], m, pc[t].modelscompidx[i, 1], n,pc[t].corrcoef_probs[i][m,n]))

            f.write('\n')
        f.write('\n')

    # Write performance stats to file, if we have activities
    for t in range(n_tasks):
        # f.write('\n\n\n>>>>>>>>>>>>>>>>>>>> Predictions (and statistics) for Task %d of %d (%s) <<<<<<<<<<<<<<<<<<<<\n' % (t+1,n_tasks, task_names[t]))
        f.write('\n\n\n>>>>>>>>>>>>>>>>>>>> Predictions (and statistics) for %d Models <<<<<<<<<<<<<<<<<<<<\n' % (n_models))
        if has_activities:
            f.write('Performance Statistics for each model\n')
            f.write(',,')
            if used_ensemble:
                f.write('Ensemble Stats,')
            for i in range(n_models):
                f.write('%s,' % (model_names[i]))
            f.write('\n')

            f.write(',accuracy,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].accuracy))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].accuracy))
            f.write('\n')

            f.write(',balanced accy,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].balanced_accuracy))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].balanced_accuracy))
            f.write('\n')

            f.write(',specificity,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].specificity))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].specificity))
            f.write('\n')

            f.write(',sensitivity,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].sensitivity))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].sensitivity))
            f.write('\n')

            f.write(',NPV,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].NPV))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].NPV))
            f.write('\n')

            f.write(',PPV,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].PPV))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].PPV))
            f.write('\n')

            f.write(',AUC,')
            if used_ensemble:
                f.write('%f,' % (ens_stats[t].auc))
            for i in range(n_models):
                f.write('%f,' % (all_stats[i][t].auc))
            f.write('\n')

            f.write('\n\n')

        # Write out predictions per molecule, per model
        n_class = all_models[0].n_class[t]


        # Write out the header rows

        # Top level header row
        f.write(',') # skip column for molecule name
        if has_activities:
            f.write(',') # skip column for true activity
        if used_ensemble:
            f.write('Ensemble Prediction,')
            for i in range(n_class):
                f.write(',')
        for i in range(n_models):
            f.write(',%s,' % (all_models[i].model_name))
            for j in range(n_class):
                f.write(',')
        f.write('\n')

        # Write out second header row
        f.write('Molecule Name,')
        if has_activities:
            f.write('True Class,')
        if used_ensemble:
            f.write('Class Pred,')
            for i in range(n_class):
                f.write('Pr. Class %d,' % (i))
        for i in range(n_models):
            f.write(',Class Pred,')
            for j in range(n_class):
                f.write('Pr. Class %d,' % (j))
        f.write('\n')

        for i in range(n_unique):
            # Find if any of the models didn't have a prediction for the current molecule
            #   This can happen for various reasons, i.e. if featurization fails for the current molecule
            missingpredidx = np.where(matchmat[i,:] < 0)[0]
            n_missingpred = np.size(missingpredidx)
            if n_missingpred > 0:
                continue

            f.write('%s,' % (unique_mol_names[i]))
            if has_activities:
                usemodelidx = np.where(matchmat[i,:] > -1)[0]
                useidx = matchmat[i,usemodelidx[0]]
                f.write('%f,' % (all_data[usemodelidx[0]].Y[useidx,0]))

            if used_ensemble:
                f.write('%f,' % (ens_class_preds[t][i,0]))
                for j in range(n_class):
                    f.write('%f,' % (ens_pred_probs[t][i, j]))

            for j in range(n_models):
                matchidx = int(matchmat[i,j])
                if matchidx > -1:
                    f.write(',%f,' % (all_class_preds[j][t][matchidx,0]))
                    for k in range(n_class):
                        f.write('%f,' % (all_pred_probs[j][t][matchidx,k]))
                else:
                    f.write(',')
            f.write('\n')

    f.close()


    return(success)




########################################################################################################################
# Generates an inference report for regression models
def regression_inference_report(infer_settings, all_models, all_data, all_preds, all_mol_preds):
    success = False
    n_models = len(all_models)
    n_tasks = all_models[0].n_tasks #TODO: replace this with a more general soln. that allows for different nums of tasks per model

    has_activities = False
    if all_data[0].Y is not None:
        if np.size(all_data[0].Y) > 0:
            has_activities = True

    if has_activities:
        all_stats = []
        all_pred_errors = []
        for i in range(n_models):
            stats, pred_errors = ml.error_stats(all_preds[i], all_data[i].Y, Y_pred_all=all_mol_preds[i], siglevel=infer_settings.siglevel)
            all_stats.append(stats)
            all_pred_errors.append(pred_errors)


    # Get all molecules in common across all models
    # Need to get a consolidated list of all molecules to be evaluated by all models
    all_names_sequential = []
    all_names_separate = []
    for i in range(n_models):
        cur_mol_names = []
        for j in range(len(all_data[i].mols)):
            cur_mol_names.append(copy.deepcopy(all_data[i].mols[j].mol_name))
            all_names_sequential.append(copy.deepcopy(all_data[i].mols[j].mol_name))
        all_names_separate.append(copy.deepcopy(cur_mol_names))

    unique_mol_names = list(set(all_names_sequential))

    # For each unique molecule name, get the index of each data set (if any) where the molecule data is stored
    n_unique = len(unique_mol_names)
    matchmat = np.zeros((n_unique,n_models), dtype=np.int32)
    Y_all = np.zeros((n_unique,n_tasks)) * np.nan # create a matrix of Nans
    for t in range(n_tasks):
        for i in range(n_unique):
            for j in range(n_models):
                if unique_mol_names[i] in all_names_separate[j]:
                    # Get the index of the current unique name
                    matchmat[i,j] = all_names_separate[j].index(unique_mol_names[i])
                    if has_activities:
                        useidx = matchmat[i,j]
                        Y_all[i,t] = all_data[j].Y[useidx,t]
                else:
                    matchmat[i,j] = -1

    # Calculate how correlated the predictions are- i.e. "prediction complementarity".  Need to have the same tasks
    pc = None
    if infer_settings.ensemble_type is not None:
        pc = ml.prediction_complementarity(all_preds, matchmat)


    ###########################################################
    # Ensemble predictions
    used_ensemble = False
    if infer_settings.ensemble_type is not None:
        used_ensemble = True
        ens_stats = []
        print('Generating ensemble predictions...')
        ens_preds = ml.ensemble_predictions(all_preds, matchmat)
        if has_activities:
            ens_stats, ens_pred_errors = ml.error_stats(ens_preds, Y_all)


    ###########################################################
    # Write the report
    model_names = []
    task_names = all_models[0].task_names # TODO: replace this for the more general scenario with non-overlapping numbers of tasks per model
    if infer_settings.report_name is None:
        infer_settings.report_name = all_models[0].model_name[0] + "_InferenceReport_" + str(n_models) + "models"
    cur_time = time.strftime('%H-%M-%S')
    cur_date = time.strftime('%m%d%Y')
    report_file = os.path.join(infer_settings.save_root, infer_settings.report_name + '_' + cur_date + '_' + cur_time + '.csv')
    ci_string = str(int(np.round(infer_settings.siglevel * 100))) + '% CI'

    f = open(report_file,'w')
    f.write('REGRESSION MODEL INFERENCE REPORT\n')
    f.write('\nNOTE:  Since different models can have different data scaling settings, all reported predictions (and statistics) are shown for UNSCALED predictions\n')
    f.write('\n')
    f.write('Predictions from molecules listed in the file: %s\n' % (infer_settings.molecules_file))
    f.write('\n')
    f.write('Models used to generate predictions:\n')
    for i in range(n_models):
        model_path, model_stem = os.path.split(infer_settings.model_files[i])
        model_names.append(copy.deepcopy(model_stem))
        f.write('Model %d,%s\n' % (i+1, model_stem))
    f.write('\n')
    f.write('\n')

    if used_ensemble:
        f.write('Ensemble method used:\n')
        f.write(',%s\n\n' % (infer_settings.ensemble_type))

    if n_models > 1 and pc is not None:
        f.write('Complementarity of individual model predictions\n')
        for t in range(n_tasks):
            f.write('Comparison of model predictions for task:  #%d:\n' % (t+1))
            for i in range(0,pc[t].n_comps):
                f.write(',Models %d vs. %d Corr Coef:,,%.4f\n' % (pc[t].modelscompidx[i, 0]+1,pc[t].modelscompidx[i, 1]+1,pc[t].corrcoef_preds[i]))

            f.write('\n')
        f.write('\n')

    # Write performance stats to file, if we have activities
    for t in range(n_tasks):
        # f.write('\n\n\n>>>>>>>>>>>>>>>>>>>> Predictions (and statistics) for Task %d of %d (%s) <<<<<<<<<<<<<<<<<<<<\n' % (t+1,n_tasks, task_names[t]))
        f.write('\n\n\n>>>>>>>>>>>>>>>>>>>> Predictions (and statistics) for task %s (%d of %d) <<<<<<<<<<<<<<<<<<<<\n' % (task_names[t],t+1,n_tasks))
        if has_activities:

            f.write('\n********** Prediction Statistics for Each Model **********\n')
            f.write('Model Name,Rsq,Rsq %s LowerLimit,Rsq %s UpperLimit,RMS Error, MaxAbs Error\n' % (ci_string, ci_string))
            if used_ensemble:
                f.write('Ensemble,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (ens_stats[t].rsq, ens_stats[t].confint_rsq[0,0],
                                                                 ens_stats[t].confint_rsq[0,1], ens_stats[t].e_rms,
                                                                 ens_stats[t].e_maxabs))
            for i in range(n_models):
                f.write('%s,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (model_names[i], all_stats[i][t].rsq, all_stats[i][t].confint_rsq[0, 0],
                all_stats[i][t].confint_rsq[0, 1], all_stats[i][t].e_rms, all_stats[i][t].e_maxabs))
            f.write('\n')

            # f = write_feature_params(f, models)
            # f = write_hyperparameters(f, models)
            #
            # f.write('\n')


        # Write out the header rows

        # Top level header row
        f.write('\nMolecule Name,')
        if has_activities:
            f.write('True Activity,')
        if used_ensemble:
            f.write('Ensemble Prediction,,')
        for i in range(n_models):
            f.write('%s Prediction,%s LowerLimit,%s UpperLimit,,' % (model_names[i],ci_string,ci_string))
        f.write('\n')

        for i in range(n_unique):
            # Find if any of the models didn't have a prediction for the current molecule
            #   This can happen for various reasons, i.e. if featurization fails for the current molecule
            missingpredidx = np.where(matchmat[i,:] < 0)[0]
            n_missingpred = np.size(missingpredidx)
            if n_missingpred > 0:
                continue

            f.write('%s,' % (unique_mol_names[i]))
            if has_activities:
                usemodelidx = np.where(matchmat[i,:] > -1)[0]
                useidx = matchmat[i,usemodelidx[0]]
                f.write('%f,' % (all_data[usemodelidx[0]].Y[useidx,t]))

            if used_ensemble:
                f.write('%f,,' % (ens_preds[t][i]))

            for j in range(n_models):
                matchidx = int(matchmat[i,j])
                if matchidx > -1:
                    f.write('%f,%f,%f,,' % (all_preds[j][t][matchidx],all_stats[j][t].confint_per_pred[matchidx,0],
                                          all_stats[j][t].confint_per_pred[matchidx, 1]))
                else:
                    f.write(',')
            f.write('\n')

    f.close()


    return(success)
